# EU-wide Simulation tool
***
# Introduction
This sub-project is part of the ["New IACS Vision in Action” 
--- NIVA](https://www.niva4cap.eu/) project that delivers a  a suite of digital solutions, e-tools and good practices for  e-governance and initiates an innovation ecosystem to  support further development of IACS that will facilitate  data and information flows.
This project has received funding from the European Union’s  Horizon 2020 research and innovation programme under  grant agreement No 842009. 
Please visit the [website](https://www.niva4cap.eu) for  further information. A complete list of the sub-projects  made available under the NIVA project can be found on  [gitlab](https://gitlab.com/nivaeu/)

**Table of Contents**

[General Info](#general-info)
-------------
The EU-wide Simulation tool collects in a single platform the procedures for the quantification of Direct Payments of the CAP based on Member State’s decisions, according to the Reg. (EU) on CAP Strategic Plan [to be published]. It allows a sound quantification of the Basic Income Support for Sustainability (BISS) and the Complementary Redistributive Income Support for Sustainability (CRISS) based on eligible hectares or entitlements. 

The EU-wide Simulation tool is able to generate data relating to 3 procedures (models):
Uniform amount of support per hectare – Basic Income Support for Sustainability (BISS) on eligible hectares.
Payment entitlements and convergence – Member State grants the Basic Income Support for Sustainability (BISS) on entitlements.
Redistributive Payment – related to the Complementary Redistributive Income Support for Sustainability (CRISS) on eligible hectares.

The EU-wide Simulation tool is part of the Seamless Claim developed by NIVA. It is able to support policy makers during the programming phase and in the implementation one, providing: assessment of different scenarios tailored on Member State’s decisions; quantification of payment entitlements (or unit amount per hectare) by farmer. Furthermore, it is useful for Paying Agencies in the management of the CAP.

[Contacts](#contacts)
-------------
Algorithm Project Manager:
- Fabio Pierangeli - fabio.pierangeli@crea.gov.it

IT Project Manager:
- Luca Ruscio - l.ruscio@tecso.biz

[Technologies](#technologies)
-------------
- NIVA-SMT vers 1.1
- Java ver. 1.8
- PostgreSQL 13

[Installation](#installation)
 -------------
The compiled software is in the dist folder.  
1) Set the JAVA_HOME in the environment variables;
2) Install RDBMS PostgreSQL;
3) Database Creation;
4) Start the application from start.bat;
5) In setting go and change the database connection settings.

[Full Documentation](#documentation)
--------------
Full documentation is available through the wiki: [WIKI](https://gitlab.com/nivaeu/uc5b_nivasimulationtool/-/wikis/EU-wide-Simulation-tool)

[Notes](#note)
 -------------
(*) In the start.bat file it is possible to increase, if necessary, the java memory for the execution of the application. It is sufficient to modify the parameters of Xms and Xmx. 

(**) File structure:

- Type: [.csv]
- Delimiter: [;]
- Enclosed: ["]
- Numeric Format: [####,###]
- Heading: [Yes, first row]

1. File PYH-Unit Amount per hectare:
- CUAA: [VARCHAR(255)]
- NUM_SUP_0: [NUMERIC(18;4)]

2. File PYR- Redistributive Payment:
- CUAA: [VARCHAR(255)]
- NUM_SUP_0: [NUMERIC(18;4)]

3. File PYE-Payment entitlements and convergence:
- CUAA: [VARCHAR(255)]
- ENTITLEMENT: [VARCHAR(255)]
- NUM_SUP_22: [NUMERIC(18;4)]
- AMT_BPS_22: [NUMERIC(18;3)]

[License](#license)
--------------
This project uses the following license: EUPL
