package bl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Properties;

import org.pentaho.di.core.KettleClientEnvironment;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;

import gui.Home;

public class UtilKettle {
	public static final String FOLDER_CONF = "conf";
	public static final String FOLDER_KETTLE = ".kettle";
	public static final String FOLDER_JOB = "bin/PDI";
	public static final String NAME_FILE_KETTLE = "kettle.properties";
	public static final String NAME_JDBC_KETTLE = "jdbc.properties";
	public static final String LANGUAGE_FILE_KETTLE = ".languageChoice";
	private static final String STRING_DEFAULT_LOCALE = "LocaleDefault";
	private static final String SMT_JOB_MAIN_PYH = "SMT_JOB_MAIN_PYH.kjb";
	private static final String SMT_JOB_MAIN_PYR = "SMT_JOB_MAIN_PYR.kjb";
	private static final String SMT_JOB_MAIN_PYE = "SMT_JOB_MAIN_PYE.kjb";

	public static final String NAME_JOB_TEST = "testCreat.ktr";

	private static Locale defaultLocale = Locale.ITALY;

	public UtilKettle() {
	}

	public static void initialized() {

		try {
			System.setProperty("KETTLE_HOME", Home.getPathJar() + "/" + FOLDER_CONF + "/");
			System.setProperty("KETTLE_JNDI_ROOT", Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
			if (!KettleClientEnvironment.isInitialized()) {
				System.out.println("Kettle-init-ini");
				KettleEnvironment.init();
				System.out.println("Kettle-init-end");
			}

		} catch (KettleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void configureEnviroment() {
		File dirConf = new File(Home.getPathJar() + "/" + FOLDER_CONF);
		File dirKettle = new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
		File fileKettle = new File(
				Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_FILE_KETTLE);
		File fileJDBCKettle = new File(
				Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_JDBC_KETTLE);

		if (!dirKettle.exists()) {
			if (!dirConf.exists()) {
				dirConf.mkdir();

			}
			dirKettle.mkdir();
			// creazione del file kettle.properties
			try (FileWriter fw = new FileWriter(
					Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_FILE_KETTLE, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {
				// out.println("JNDI="+ConnectionDB.getJNDI());
				out.println("# Questo file � stato generato da Pentaho Data Integration versione 9.0.0.0-423.");
				out.println("#");
				out.println("# Alcuni esempi di variabili da impostare:");
				out.println("#");
				out.println(" PRODUCTION_SERVER = hercules");
				out.println("# TEST_SERVER = zeus");
				out.println("# DEVELOPMENT_SERVER = thor");
				out.println("#");
				out.println("# Nota: le linee con # all'inizio sono commenti");
				out.println("#");
				out.println("#");
				out.println("#Mon Aug 24 18:06:14 CEST 2020");
				out.println("KETTLE_CORE_JOBENTRIES_FILE=");
				out.println("KETTLE_LAZY_REPOSITORY=true");
				out.println("KETTLE_DEFAULT_DATE_FORMAT=");
				out.println("KETTLE_JOB_LOG_SCHEMA=");
				out.println("KETTLE_TRANS_PAN_JVM_EXIT_CODE=");
				out.println("KETTLE_CHANNEL_LOG_TABLE=");
				out.println("KETTLE_SPLIT_FIELDS_REMOVE_ENCLOSURE=false");
				out.println("SHARED_STREAMING_BATCH_POOL_SIZE=0");
				out.println("vfs.sftp.userDirIsRoot=false");
				out.println("KETTLE_COMPATIBILITY_PUR_OLD_NAMING_MODE=N");
				out.println("KETTLE_CARTE_JETTY_ACCEPT_QUEUE_SIZE=");
				out.println("KETTLE_MAX_LOGGING_REGISTRY_SIZE=10000");
				out.println("KETTLE_JNDI_ROOT=");
				out.println("KETTLE_DEFAULT_TIMESTAMP_FORMAT=");
				out.println("KETTLE_MAX_LOG_TIMEOUT_IN_MINUTES=1440");
				out.println("KETTLE_SHARED_OBJECTS=");
				out.println("KETTLE_GLOBAL_LOG_VARIABLES_CLEAR_ON_EXPORT=false");
				out.println("KETTLE_METRICS_LOG_TABLE=");
				out.println("KETTLE_BATCHING_ROWSET=N");
				out.println("KETTLE_TRANS_LOG_TABLE=");
				out.println("KETTLE_JOBENTRY_LOG_SCHEMA=");
				out.println("KETTLE_DEFAULT_INTEGER_FORMAT=");
				out.println("KETTLE_METRICS_LOG_DB=");
				out.println("KETTLE_CARTE_JETTY_RES_MAX_IDLE_TIME=");
				out.println("KETTLE_PLUGIN_CLASSES=");
				out.println("KETTLE_FILE_OUTPUT_MAX_STREAM_LIFE=0");
				out.println("KETTLE_DEFAULT_BIGNUMBER_FORMAT=");
				out.println("KETTLE_REDIRECT_STDERR=N");
				out.println("KETTLE_FAIL_ON_LOGGING_ERROR=N");
				out.println("KETTLE_ROWSET_GET_TIMEOUT=50");
				out.println("KETTLE_CHANNEL_LOG_DB=");
				out.println("KETTLE_METRICS_LOG_SCHEMA=");
				out.println("KETTLE_EMPTY_STRING_DIFFERS_FROM_NULL=Y");
				out.println("KETTLE_ROWSET_PUT_TIMEOUT=50");
				out.println("KETTLE_AGGREGATION_MIN_NULL_IS_VALUED=N");
				out.println("KETTLE_JOB_LOG_TABLE=");
				out.println("KETTLE_DEFAULT_NUMBER_FORMAT=");
				out.println("KETTLE_DEFAULT_SERVLET_ENCODING=");
				out.println("KETTLE_HADOOP_CLUSTER_GATEWAY_CONNECTION=false");
				out.println("KETTLE_REDIRECT_STDOUT=N");
				out.println("KETTLE_TRANS_LOG_DB=");
				out.println("KETTLE_TRANS_PERFORMANCE_LOG_SCHEMA=");
				out.println("KETTLE_MAX_LOG_SIZE_IN_LINES=5000");
				out.println("KETTLE_JOBENTRY_LOG_TABLE=");
				out.println("KETTLE_PLUGIN_PACKAGES=");
				out.println("KETTLE_COMPATIBILITY_TEXT_FILE_OUTPUT_APPEND_NO_HEADER=N");
				out.println("KETTLE_DISABLE_CONSOLE_LOGGING=N");
				out.println("KETTLE_ALLOW_EMPTY_FIELD_NAMES_AND_TYPES=false");
				out.println("KETTLE_TRANS_PERFORMANCE_LOG_DB=");
				out.println("KETTLE_MAX_JOB_TRACKER_SIZE=5000");
				out.println("PENTAHO_METASTORE_FOLDER=");
				out.println("KETTLE_STEP_LOG_TABLE=");
				out.println("KETTLE_STEP_PERFORMANCE_SNAPSHOT_LIMIT=0");
				out.println("KETTLE_CORE_STEPS_FILE=");
				out.println("KETTLE_STEP_LOG_DB=");
				out.println("KETTLE_AGGREGATION_ALL_NULLS_ARE_ZERO=N");
				out.println("KETTLE_LENIENT_STRING_TO_NUMBER_CONVERSION=N");
				out.println("KETTLE_LOG_SIZE_LIMIT=0");
				out.println("KETTLE_PASSWORD_ENCODER_PLUGIN=Kettle");
				out.println("KETTLE_DATA_REFINERY_HTTP_CLIENT_TIMEOUT=2000");
				out.println("KETTLE_JOB_LOG_DB=");
				out.println("KETTLE_TRANS_PERFORMANCE_LOG_TABLE=");
				out.println("KETTLE_MAX_TAB_LENGTH=17");
				out.println("KETTLE_CHANNEL_LOG_SCHEMA=");
				out.println("KETTLE_STEP_LOG_SCHEMA=");
				out.println("KETTLE_JOBENTRY_LOG_DB=");
				out.println("KETTLE_CARTE_JETTY_ACCEPTORS=");
				out.println("KETTLE_COMPATIBILITY_MERGE_ROWS_USE_REFERENCE_STREAM_WHEN_IDENTICAL=N");
				out.println("KETTLE_STREAMING_TIME_LIMIT=10000");
				out.println("KETTLE_HIDE_DEVELOPMENT_VERSION_WARNING=N");
				out.println("KETTLE_LOG_TAB_REFRESH_PERIOD=1000");
				out.println("KETTLE_SYSTEM_HOSTNAME=");
				out.println("SHIM_DRIVER_DEPLOYMENT_LOCATION=DEFAULT");
				out.println("KETTLE_LOG_TAB_REFRESH_DELAY=1000");
				out.println("KETTLE_FILE_OUTPUT_MAX_STREAM_COUNT=1024");
				out.println("KETTLE_CARTE_OBJECT_TIMEOUT_MINUTES=1440");
				out.println("KETTLE_LOG_MARK_MAPPINGS=N");
				out.println("KETTLE_MAX_JOB_ENTRIES_LOGGED=5000");
				out.println("KETTLE_COMPATIBILITY_IMPORT_PATH_ADDITION_ON_VARIABLES=N");
				out.println("KETTLE_COMPATIBILITY_DB_IGNORE_TIMEZONE=N");
				out.println("s3.vfs.useTempFileOnUploadData=N");
				out.println("KETTLE_STREAMING_ROW_LIMIT=5000");
				out.println("KETTLE_TRANS_LOG_SCHEMA=");
				out.println("##CREA-SMT###################");
				out.println("PYH_FILE_CSV=./" + ConfigurationProperties.FOLDER_DATA + "/PYH_FILE_${DATE_STR}.csv");
				out.println("PYR_FILE_CSV=./" + ConfigurationProperties.FOLDER_DATA + "/PYR_FILE_${DATE_STR}.csv");
				out.println("PYE_FILE_CSV=./" + ConfigurationProperties.FOLDER_DATA + "/PYE_FILE_${DATE_STR}.csv");
				out.println("##CREA-SMT###################");

			} catch (IOException e) {
				// exception handling left as an exercise for the reader
			}
			// creazione del file jdbc.properties
			try (FileWriter fw = new FileWriter(
					Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_JDBC_KETTLE, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {

				out.println("#CONNESSIONE ETL NIVA-SMT");
				out.println("NIVA-SMT/type=javax.sql.DataSource");
				out.println("NIVA-SMT/driver=org.postgresql.Driver");
				out.println("NIVA-SMT/url=jdbc:postgresql://" + Home.getDB_IP() + ":" + Home.getDB_PORTA() + "/"
						+ Home.getDB_NAME());
				out.println("NIVA-SMT/user=" + Home.getDB_UTENTE());
				out.println("NIVA-SMT/password=" + Home.getDB_PASSWORD());

			} catch (IOException e) {
				// exception handling left as an exercise for the reader
			}

//			try {
//				// copio il file
//				UtilFile.copiaFile(
//						ConfigurationProperties.class.getClass().getResourceAsStream("/resources/config.properties"),
//						Home.getPathJar() + "/" + FOLDER_DB + "/" + NAME_FILE_CONFIG);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
		} else {
			if (!fileKettle.exists()) {
				// creazione del file kettle.properties
				try (FileWriter fw = new FileWriter(
						Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_FILE_KETTLE, true);
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw)) {
					// out.println("JNDI="+ConnectionDB.getJNDI());

				} catch (IOException e) {
					// exception handling left as an exercise for the reader
				}
			}
			if (!fileJDBCKettle.exists()) {
				// creazione del file jdbc.properties
				try (FileWriter fw = new FileWriter(
						Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_JDBC_KETTLE, true);
						BufferedWriter bw = new BufferedWriter(fw);
						PrintWriter out = new PrintWriter(bw)) {

					out.println("#CONNESSIONE ETL NIVA-SMT");
					out.println("NIVA-SMT/type=javax.sql.DataSource");
					out.println("NIVA-SMT/driver=org.postgresql.Driver");
					out.println("NIVA-SMT/url=jdbc:postgresql://" + Home.getDB_IP() + ":" + Home.getDB_PORTA() + "/"
							+ Home.getDB_NAME());
					out.println("NIVA-SMT/user=" + Home.getDB_UTENTE());
					out.println("NIVA-SMT/password=" + Home.getDB_PASSWORD());

				} catch (IOException e) {
					// exception handling left as an exercise for the reader
				}

			}

		}

		saveSettings(Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
	}

	public static void updateKettleJDBCEnviroment() {
		File fileJDBCKettle = new File(
				Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_JDBC_KETTLE);

		if (fileJDBCKettle.exists()) {
			fileJDBCKettle.delete();
			// creazione del file jdbc.properties
			try (FileWriter fw = new FileWriter(
					Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE + "/" + NAME_JDBC_KETTLE, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {

				out.println("#CONNESSIONE ETL NIVA-SMT");
				out.println("NIVA-SMT/type=javax.sql.DataSource");
				out.println("NIVA-SMT/driver=org.postgresql.Driver");
				out.println("NIVA-SMT/url=jdbc:postgresql://" + Home.getDB_IP() + ":" + Home.getDB_PORTA() + "/"
						+ Home.getDB_NAME());
				out.println("NIVA-SMT/user=" + Home.getDB_UTENTE());
				out.println("NIVA-SMT/password=" + Home.getDB_PASSWORD());

			} catch (IOException e) {
				// exception handling left as an exercise for the reader
			}

		}

	}

	public static void saveSettings(String path) {
		try {

			Properties properties = new Properties();
			properties.setProperty(STRING_DEFAULT_LOCALE, defaultLocale.toString());
			properties.store(new FileOutputStream(path + "/" + LANGUAGE_FILE_KETTLE), "Language Choice");
		} catch (IOException e) {
			// Ignore
		}
	}

	public static void executeJob() {
		try {

			// saveSettings();

			try {
				if (!KettleClientEnvironment.isInitialized()) {
					System.setProperty("KETTLE_HOME", Home.getPathJar() + "/" + FOLDER_CONF + "/");
					System.setProperty("KETTLE_JNDI_ROOT", Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
					KettleEnvironment.init();
				}
			} catch (KettleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				JobMeta jobMeta = new JobMeta("C:\\NIVA-SMT\\Kettle\\job.kjb", null);
				Job job = new Job(null, jobMeta);
				job.start();
				job.waitUntilFinished();
				if (job.getErrors() != 0) {
					System.out.println("Error encountered!");
				}
			} catch (KettleXMLException e) {

				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void executeJobPYH(String AMT_MAX_23, String AMT_MAX_24, String AMT_MAX_25, String AMT_MAX_26,
			String DATE, String NUM_MIN_SUP_LEVEL, String PCT_BISS_23, String PCT_BISS_24, String PCT_BISS_25,
			String PCT_BISS_26) throws Exception {

		if (!KettleClientEnvironment.isInitialized()) {
			System.setProperty("KETTLE_HOME", Home.getPathJar() + "/" + FOLDER_CONF + "/");
			System.setProperty("KETTLE_JNDI_ROOT", Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
			System.out.println("Kettle-init-job");
			KettleEnvironment.init();
		}

		JobMeta jobMeta = new JobMeta(Home.getPathJar() + "/" + FOLDER_JOB + "/" + SMT_JOB_MAIN_PYH, null);
		jobMeta.setParameterValue("AMT_MAX_23", AMT_MAX_23);
		jobMeta.setParameterValue("AMT_MAX_24", AMT_MAX_24);
		jobMeta.setParameterValue("AMT_MAX_25", AMT_MAX_25);
		jobMeta.setParameterValue("AMT_MAX_26", AMT_MAX_26);
		jobMeta.setParameterValue("DATE", DATE);
		jobMeta.setParameterValue("NUM_MIN_SUP_LEVEL", NUM_MIN_SUP_LEVEL);
		jobMeta.setParameterValue("PCT_BISS_23", PCT_BISS_23);
		jobMeta.setParameterValue("PCT_BISS_24", PCT_BISS_24);
		jobMeta.setParameterValue("PCT_BISS_25", PCT_BISS_25);
		jobMeta.setParameterValue("PCT_BISS_26", PCT_BISS_26);
		Job job = new Job(null, jobMeta);
		job.start();
		job.waitUntilFinished();
		if (job.getErrors() != 0) {
			System.out.println("KO Error:" + job.getErrors());
		} else {
			System.out.println("OK");
		}

	}

	public static void executeJobPYR(String AMT_HA_23, String AMT_HA_24, String AMT_HA_25, String AMT_HA_26,
			String PCT_CRISS_23,String PCT_CRISS_24,String PCT_CRISS_25, String PCT_CRISS_26,
			String DATE, String FLG_ONLY_PURE, String NUM_MIN_SUP_LEVEL, String NUM_SUP_RNG_FM_23_X1,
			String NUM_SUP_RNG_FM_23_X2, String NUM_SUP_RNG_FM_23_X3, String NUM_SUP_RNG_FM_24_X1,
			String NUM_SUP_RNG_FM_24_X2, String NUM_SUP_RNG_FM_24_X3, String NUM_SUP_RNG_FM_25_X1,
			String NUM_SUP_RNG_FM_25_X2, String NUM_SUP_RNG_FM_25_X3, String NUM_SUP_RNG_FM_26_X1,
			String NUM_SUP_RNG_FM_26_X2, String NUM_SUP_RNG_FM_26_X3, String NUM_SUP_RNG_TO_23_X1,
			String NUM_SUP_RNG_TO_23_X2, String NUM_SUP_RNG_TO_23_X3, String NUM_SUP_RNG_TO_24_X1,
			String NUM_SUP_RNG_TO_24_X2, String NUM_SUP_RNG_TO_24_X3, String NUM_SUP_RNG_TO_25_X1,
			String NUM_SUP_RNG_TO_25_X2, String NUM_SUP_RNG_TO_25_X3, String NUM_SUP_RNG_TO_26_X1,
			String NUM_SUP_RNG_TO_26_X2, String NUM_SUP_RNG_TO_26_X3, String PCT_BISS_23_X1, String PCT_BISS_23_X2,
			String PCT_BISS_23_X3, String PCT_BISS_24_X1, String PCT_BISS_24_X2, String PCT_BISS_24_X3,
			String PCT_BISS_25_X1, String PCT_BISS_25_X2, String PCT_BISS_25_X3, String PCT_BISS_26_X1,
			String PCT_BISS_26_X2, String PCT_BISS_26_X3) throws Exception {

		if (!KettleClientEnvironment.isInitialized()) {
			System.setProperty("KETTLE_HOME", Home.getPathJar() + "/" + FOLDER_CONF + "/");
			System.setProperty("KETTLE_JNDI_ROOT", Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
			System.out.println("Kettle-init-job");
			KettleEnvironment.init();
		}

		JobMeta jobMeta = new JobMeta(Home.getPathJar() + "/" + FOLDER_JOB + "/" + SMT_JOB_MAIN_PYR, null);
		jobMeta.setParameterValue("AMT_MAX_23", AMT_HA_23);
		jobMeta.setParameterValue("AMT_MAX_24", AMT_HA_24);
		jobMeta.setParameterValue("AMT_MAX_25", AMT_HA_25);
		jobMeta.setParameterValue("AMT_MAX_26", AMT_HA_26);
		jobMeta.setParameterValue("PCT_CRISS_23", PCT_CRISS_23);
		jobMeta.setParameterValue("PCT_CRISS_24", PCT_CRISS_24);
		jobMeta.setParameterValue("PCT_CRISS_25", PCT_CRISS_25);
		jobMeta.setParameterValue("PCT_CRISS_26", PCT_CRISS_26);
		jobMeta.setParameterValue("DATE", DATE);
		jobMeta.setParameterValue("FLG_ONLY_PURE", FLG_ONLY_PURE);
		jobMeta.setParameterValue("NUM_MIN_SUP_LEVEL", NUM_MIN_SUP_LEVEL);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_23_X1", NUM_SUP_RNG_FM_23_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_23_X2", NUM_SUP_RNG_FM_23_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_23_X3", NUM_SUP_RNG_FM_23_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_24_X1", NUM_SUP_RNG_FM_24_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_24_X2", NUM_SUP_RNG_FM_24_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_24_X3", NUM_SUP_RNG_FM_24_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_25_X1", NUM_SUP_RNG_FM_25_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_25_X2", NUM_SUP_RNG_FM_25_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_25_X3", NUM_SUP_RNG_FM_25_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_26_X1", NUM_SUP_RNG_FM_26_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_26_X2", NUM_SUP_RNG_FM_26_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_FM_26_X3", NUM_SUP_RNG_FM_26_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_23_X1", NUM_SUP_RNG_TO_23_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_23_X2", NUM_SUP_RNG_TO_23_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_23_X3", NUM_SUP_RNG_TO_23_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_24_X1", NUM_SUP_RNG_TO_24_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_24_X2", NUM_SUP_RNG_TO_24_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_24_X3", NUM_SUP_RNG_TO_24_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_25_X1", NUM_SUP_RNG_TO_25_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_25_X2", NUM_SUP_RNG_TO_25_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_25_X3", NUM_SUP_RNG_TO_25_X3);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_26_X1", NUM_SUP_RNG_TO_26_X1);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_26_X2", NUM_SUP_RNG_TO_26_X2);
		jobMeta.setParameterValue("NUM_SUP_RNG_TO_26_X3", NUM_SUP_RNG_TO_26_X3);
		jobMeta.setParameterValue("PCT_BISS_23_X1", PCT_BISS_23_X1);
		jobMeta.setParameterValue("PCT_BISS_23_X2", PCT_BISS_23_X2);
		jobMeta.setParameterValue("PCT_BISS_23_X3", PCT_BISS_23_X3);
		jobMeta.setParameterValue("PCT_BISS_24_X1", PCT_BISS_24_X1);
		jobMeta.setParameterValue("PCT_BISS_24_X2", PCT_BISS_24_X2);
		jobMeta.setParameterValue("PCT_BISS_24_X3", PCT_BISS_24_X3);
		jobMeta.setParameterValue("PCT_BISS_25_X1", PCT_BISS_25_X1);
		jobMeta.setParameterValue("PCT_BISS_25_X2", PCT_BISS_25_X2);
		jobMeta.setParameterValue("PCT_BISS_25_X3", PCT_BISS_25_X3);
		jobMeta.setParameterValue("PCT_BISS_26_X1", PCT_BISS_26_X1);
		jobMeta.setParameterValue("PCT_BISS_26_X2", PCT_BISS_26_X2);
		jobMeta.setParameterValue("PCT_BISS_26_X3", PCT_BISS_26_X3);
		Job job = new Job(null, jobMeta);
		job.start();
		job.waitUntilFinished();
		if (job.getErrors() != 0) {
			System.out.println("KO Error:" + job.getErrors());
		} else {
			System.out.println("OK");
		}

	}

	public static void executeJobPYE(String NUM_MAX_SUP_LEVEL, String NUM_YEARS_CONV, String NUM_YEAR_MAX_LEVEL,
			String DATE, String AMT_MAX_LEVEL, String PCT_SL,
			// String AMT_MAX_BPS_22,
			// String AMT_MAX_GRE_22,
			String NUM_CY_SL, String PCT_GRE, String AMT_HA_GRE, String PCT_AUA, String AMT_MAX_23, String PCT_BISS_23,
			String AMT_MAX_24, String PCT_BISS_24, String AMT_MAX_25, String PCT_BISS_25, String AMT_MAX_26,
			String PCT_BISS_26) throws Exception {

		if (!KettleClientEnvironment.isInitialized()) {
			System.setProperty("KETTLE_HOME", Home.getPathJar() + "/" + FOLDER_CONF + "/");
			System.setProperty("KETTLE_JNDI_ROOT", Home.getPathJar() + "/" + FOLDER_CONF + "/" + FOLDER_KETTLE);
			System.out.println("Kettle-init-job");
			KettleEnvironment.init();
		}

		JobMeta jobMeta = new JobMeta(Home.getPathJar() + "/" + FOLDER_JOB + "/" + SMT_JOB_MAIN_PYE, null);
//				jobMeta.setParameterValue("AMT_MAX_23", AMT_MAX_23);
		jobMeta.setParameterValue("NUM_MAX_SUP_LEVEL", NUM_MAX_SUP_LEVEL);
		jobMeta.setParameterValue("NUM_YEARS_CONV", NUM_YEARS_CONV);
		jobMeta.setParameterValue("NUM_YEAR_MAX_LEVEL", NUM_YEAR_MAX_LEVEL);
		jobMeta.setParameterValue("DATE", DATE);
		jobMeta.setParameterValue("AMT_MAX_LEVEL", AMT_MAX_LEVEL);
		jobMeta.setParameterValue("PCT_SL", PCT_SL);
		jobMeta.setParameterValue("NUM_CY_SL", NUM_CY_SL);
		// jobMeta.setParameterValue("AMT_MAX_BPS_22",AMT_MAX_BPS_22);
		// jobMeta.setParameterValue("AMT_MAX_GRE_22",AMT_MAX_GRE_22);
		jobMeta.setParameterValue("PCT_GRE", PCT_GRE);
		jobMeta.setParameterValue("AMT_HA_GRE", AMT_HA_GRE);
		jobMeta.setParameterValue("PCT_AUA", PCT_AUA);
		jobMeta.setParameterValue("AMT_MAX_23", AMT_MAX_23);
		jobMeta.setParameterValue("PCT_BISS_23", PCT_BISS_23);
		jobMeta.setParameterValue("AMT_MAX_24", AMT_MAX_24);
		jobMeta.setParameterValue("PCT_BISS_24", PCT_BISS_24);
		jobMeta.setParameterValue("AMT_MAX_25", AMT_MAX_25);
		jobMeta.setParameterValue("PCT_BISS_25", PCT_BISS_25);
		jobMeta.setParameterValue("AMT_MAX_26", AMT_MAX_26);
		jobMeta.setParameterValue("PCT_BISS_26", PCT_BISS_26);

		Job job = new Job(null, jobMeta);
		job.start();
		job.waitUntilFinished();
		if (job.getErrors() != 0) {
			System.out.println("KO Error:" + job.getErrors());
		} else {
			System.out.println("OK");
		}

	}

}
