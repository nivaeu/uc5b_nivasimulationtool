package bl;

import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.jfree.ui.Align;

import gui.Home;

public class IconRenderer extends JLabel implements TableCellRenderer {
	/**
	* 
	*/
	private static final long serialVersionUID = -1704237026187104327L;

	public IconRenderer(String image) {

		if (image != null) {
			ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/" + image));
			Image imageHome = iconHome.getImage();
			imageHome = imageHome.getScaledInstance(25, 25, java.awt.Image.SCALE_SMOOTH);
			iconHome = new ImageIcon(imageHome);
			setIcon(iconHome);
		}
		setAlignmentX(CENTER_ALIGNMENT);
		setHorizontalAlignment(Align.CENTER);
		this.setBorder(null);
		// button.setFocusable(false);

	}

	/**
	 * 
	 * @param text
	 * @param image
	 */
	public IconRenderer(String text, ImageIcon image) {
		setText(text);
		setIcon(image);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean selected, boolean focused, int row,
			int col) {

		if (col == 5 && table.getSelectedRow() < 0) {
			if (table.getModel().getValueAt(row, 5) != null) {
				switch (Integer.parseInt(obj.toString())) {
				case -1:
					table.getColumnModel().getColumn(5).setCellRenderer(new IconRenderer("error.png"));
					table.getColumnModel().getColumn(5).setMaxWidth(90);
					break;
				case 0:
					table.getColumnModel().getColumn(5).setCellRenderer(new IconRenderer("warning.png"));
					table.getColumnModel().getColumn(5).setMaxWidth(90);
					break;

				case 1:
					table.getColumnModel().getColumn(5).setCellRenderer(new IconRenderer("completed.png"));
					table.getColumnModel().getColumn(5).setMaxWidth(90);
					break;

				default:
					break;
				}

			} else {
				table.getColumnModel().getColumn(5).setCellRenderer(new IconRenderer("wait.png"));
				table.getColumnModel().getColumn(5).setMaxWidth(90);

			}

		}
		return this;
	}

}