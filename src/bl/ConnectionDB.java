package bl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import gui.Home;

public class ConnectionDB {
	Connection connection = null;

	public ConnectionDB() {
		connection = null;
	}

	public static String getJNDI() {
		String connectionString = "jdbc:postgresql://" + Home.getDB_IP() + ":" + Home.getDB_PORTA() + "/"
				+ Home.getDB_NAME() + "?user=" + Home.getDB_UTENTE() + "&password=" + Home.getDB_PASSWORD();
		return connectionString;
	}

	public Connection getConnectionDB(String database, String utente, String password) {

		String connectionString = "jdbc:postgresql://" + Home.getDB_IP() + ":" + Home.getDB_PORTA() + "/" + database;
		try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		try {
			connection = DriverManager.getConnection(connectionString, utente, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return connection;
	}

	protected void finalize() throws Throwable {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.finalize();
	}

	public boolean testConnection(String database, String ip, String porta, String utente, String password) {

		String connectionString = "jdbc:postgresql://" + ip + ":" + porta + "/" + database;
		try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

		try {
			connection = DriverManager.getConnection(connectionString, utente, password);
			if (connection != null)
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}
		return false;

	}

	public boolean creaDatabase() {
		String sql;

		boolean result = false;
		try {
			connection = getConnectionDB("", Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			if (connection != null) {
				sql = "CREATE DATABASE  \"" + Home.getDB_NAME() + "\"  WITH   OWNER = " + Home.getDB_UTENTE()
						+ "  ENCODING = 'UTF8'   CONNECTION LIMIT = -1";
				try {
					connection.prepareStatement(sql).execute();
				} catch (Exception e) {
					System.out.println("Problems in creating the database or database created");
				}

				result = true;
			} else {
				result = false;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}
		return result;
	}

	public boolean verificaDatabaseETabelle() {
		String sql;
		ResultSet rs = null;
		boolean result = false;
		try {
			connection = getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			if (connection != null) {
				sql = " SELECT count(*)  nTable" + " FROM information_schema.tables " + " WHERE   table_catalog= '"
						+ Home.getDB_NAME() + "'"
						+ " AND UPPER(table_name) in ('SYS_MODEL','SYS_JOB_LOG','SYS_STATUS_JOB','SYS_MODEL_EXECUTION') ";

				try {
					rs = connection.prepareStatement(sql).executeQuery();
				} catch (Exception e) {
					System.out.println("Problems in creating the database or database created");
				}

				if (rs != null && rs.next()) {
					int num = rs.getInt("nTable");
					if (num == 4)
						result = true;
					else
						result = false;
				}

			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}
		return result;
	}

	public boolean initDB() {

		boolean result = false;
		try {
			connection = getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			// creaDatabase();
			if (connection != null) {
				InputStream initialStream = ConfigurationProperties.class.getClass()
						.getResourceAsStream("/resources/scriptDB.sql");
				Reader targetReader = new InputStreamReader(initialStream);
				BufferedReader reader = new BufferedReader(targetReader);
				String line = reader.readLine().trim();
				while (line != null && !line.isEmpty()) {
					System.out.println(line);
					connection.prepareStatement(line).execute();
					line = reader.readLine();
				}
				result = true;
				// aggiorno il file di properties
				try {
					ConfigurationProperties.setProperties(ConfigurationProperties.PROP_DB_CREATO,
							ConfigurationProperties.PROP_DB_CREATO_SI);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				result = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

	public boolean resetDB() {

		String sql;

		boolean result = false;
		try {
			connection = getConnectionDB("", Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			if (connection != null) {
				sql = "DROP DATABASE IF EXISTS " + Home.getDB_NAME();
				connection.prepareStatement(sql).execute();
				result = true;
				try {
					ConfigurationProperties.setProperties(ConfigurationProperties.PROP_DB_CREATO,
							ConfigurationProperties.PROP_DB_CREATO_SI);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				result = false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

}
