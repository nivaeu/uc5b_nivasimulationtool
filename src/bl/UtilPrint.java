package bl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import org.jfree.ui.Align;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import gui.Home;

public class UtilPrint {

	// private static String FILE =
	// Home.getPathJar()+File.separator+ConfigurationProperties.FOLDER_DATA+File.separator+"FirstPdf.pdf";
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	private static BaseColor colorHeaderTable = WebColors.getRGBColor("#447294");
	private static BaseColor colorRowTable = WebColors.getRGBColor("#D9E2F3");
	private static BaseColor colorBorderHeader = new BaseColor(0, 110, 171);
	private static BaseColor colorHeaderTableLegend = WebColors.getRGBColor("#A2A2A2");
	private String nameFile;
	private String type;
	private String id_segnalazione;

	public UtilPrint() {
	}

	public void loadStampaPDF(String nomeFile) {
		String FILE = Home.getPathJar() + File.separator + ConfigurationProperties.FOLDER_DATA + File.separator
				+ nomeFile.trim() + ".pdf";
		try {
			java.awt.Desktop.getDesktop().open(new File(FILE));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Stampa(String data) {

		nameFile = null;
		type = null;
		getParameterPrint(data);

		if (nameFile == null || type == null || id_segnalazione == null)
			return;

		String FILE = Home.getPathJar() + File.separator + ConfigurationProperties.FOLDER_DATA + File.separator
				+ nameFile.trim() + ".pdf";
		try {
			UtilPrint.HeaderTable event = new UtilPrint.HeaderTable();
			Document document = new Document(PageSize.A4, 36, 36, 20 + event.getTableHeight(),
					20 + event.getTableFooterHeight());
			if (type.equalsIgnoreCase("1")) {
				ArrayList<PrintModel1Values> list = getStampaModel1(id_segnalazione);
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
				writer.setPageEvent(event);
				writer.setPageEvent(event);
				document.open();
				addContentModel1(document, list);

			}

			if (type.equalsIgnoreCase("2")) {
				ArrayList<PrintModel2Values> list = getStampaModel2(id_segnalazione);
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
				writer.setPageEvent(event);
				writer.setPageEvent(event);
				document.open();
				addContentModel2(document, list);

			}

			if (type.equalsIgnoreCase("3")) {
				ArrayList<PrintModel3Values> list = getStampaModel3(id_segnalazione);
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
				writer.setPageEvent(event);
				writer.setPageEvent(event);
				document.open();
				addContentModel3(document, list);

			}

			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void getParameterPrint(String data) {

		Connection connection = null;
		ConnectionDB c = new ConnectionDB();

		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());

			Statement stm = connection.createStatement();
			ResultSet rs = stm.executeQuery("SELECT " + " SME.id_model_execution, " + " SME.id_model,  "
					+ " SME.dat_model_execution, " + " SME.nam_model_execution,  " + " SME.des_model_execution,  "
					+ " SME.dat_start,  " + " SME.dat_end,  " + " SME.flg_model_execution_status, "
					+ " CASE WHEN SSJ.job_status_type IN (0 , 1) THEN "
					+ " CASE WHEN SME.id_model=1 THEN \'PYH_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYH_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
					+ " WHEN SME.id_model=2 THEN \'PYR_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYR_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
					+ " WHEN SME.id_model=3 THEN \'PYE_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYE_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
					+ " END " + "  ELSE NULL END DATA_TABLES," + " SSJ.id_status_job,  " + " SSJ.id_job,  "
					+ " SSJ.nam_job,  " + " SSJ.dat_exec_job, " + " SSJ.job_status_type, " + " SSJ.job_status, "
					+ " SJL.log_field " + " FROM sys_model_execution SME " + " LEFT JOIN sys_status_job SSJ "
					+ " ON SME.dat_model_execution=SSJ.dat_exec_job " + " LEFT JOIN sys_job_log SJL "
					+ " ON SSJ.id_job=SJL.id_job " + "WHERE SME.dat_model_execution='" + data
					+ "' and SSJ.job_status_type in (0,1)");

			while (rs.next()) {
				nameFile = rs.getString("id_model_execution").trim() + "-" + rs.getString("nam_model_execution").trim();
				type = rs.getString("id_model").trim();
				id_segnalazione = rs.getString("id_model_execution").trim();
			}
			stm.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

	}

	private ArrayList<PrintModel1Values> getStampaModel1(String id_segnalazione) {
		/*
		 * ################################################## REPORT 01-Pay to hectare
		 * ##################################################
		 */

		ArrayList<PrintModel1Values> result = new ArrayList<PrintModel1Values>();
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();

		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			Statement stm = connection.createStatement();

			String sql = " SELECT " + " ID_MODEL, " + " COD_MODEL, " + " NAM_MODEL, " + " DES_MODEL, "
					+ " ID_MODEL_EXECUTION, " + " DAT_MODEL_EXECUTION, " + " NAM_MODEL_EXECUTION, "
					+ " DES_MODEL_EXECUTION, " + " DAT_START, " + " DAT_END, " + " FLG_MODEL_EXECUTION_STATUS, "
					+ " ID_STATUS_JOB, " + " ID_JOB, " + " NAM_JOB, " + " JOB_STATUS_TYPE, " + " JOB_STATUS, "
					+ " FD_NUM_RECORD, " + " FD_NUM_CUAA, " + " FD_NUM_SUP_0, " + " MD_NUM_RECORD, " + " MD_NUM_CUAA, "
					+ " MD_NUM_SUP_0, " + " MD_AMT_MAX_23, " + " MD_AMT_MAX_24, " + " MD_AMT_MAX_25, "
					+ " MD_AMT_MAX_26, " + " MD_PCT_BISS_23, " + " MD_PCT_BISS_24, " + " MD_PCT_BISS_25, "
					+ " MD_PCT_BISS_26, " + " MD_NUM_MIN_SUP_LEVEL, " + " MD_AMT_23, " + " MD_AMT_24, " + " MD_AMT_25, "
					+ " MD_AMT_26, " + " MD_AMT_HA_23, " + " MD_AMT_HA_24, " + " MD_AMT_HA_25, " + " MD_AMT_HA_26, "
					+ " MD_NUM_SUP_AVG, " + " MD_NUM_CUAA_AVG, " + " MD_NUM_SUP_0_AVG, " + " MD_AMT_23_AVG, "
					+ " MD_AMT_24_AVG, " + " MD_AMT_25_AVG, " + " MD_AMT_26_AVG " + " 	FROM  pyh_rep_"
					+ id_segnalazione;

			ResultSet rs = stm.executeQuery(sql);
			PrintModel1Values obj;
			while (rs.next()) {
				obj = new PrintModel1Values();
				obj.setId_model(rs.getString("ID_MODEL"));
				obj.setCod_model(rs.getString("COD_MODEL"));
				obj.setNam_model(rs.getString("NAM_MODEL"));
				obj.setDes_model(rs.getString("DES_MODEL"));
				obj.setId_model_execution(rs.getString("ID_MODEL_EXECUTION"));
				obj.setDat_model_execution(rs.getString("DAT_MODEL_EXECUTION"));
				obj.setNam_model_execution(rs.getString("NAM_MODEL_EXECUTION"));
				obj.setDes_model_execution(rs.getString("DES_MODEL_EXECUTION"));
				obj.setDat_start(rs.getString("DAT_START"));
				obj.setDat_end(rs.getString("DAT_END"));
				obj.setFlg_model_execution_status(rs.getString("FLG_MODEL_EXECUTION_STATUS"));
				obj.setId_status_job(rs.getString("ID_STATUS_JOB"));
				obj.setId_job(rs.getString("ID_JOB"));
				obj.setNam_job(rs.getString("NAM_JOB"));
				obj.setJob_satus_type(rs.getString("JOB_STATUS_TYPE"));
				obj.setJob_status(rs.getString("JOB_STATUS"));
				obj.setFd_num_record(rs.getString("FD_NUM_RECORD"));
				obj.setFd_num_cuaa(rs.getString("FD_NUM_CUAA"));
				obj.setFd_num_sup_0(rs.getString("FD_NUM_SUP_0"));
				obj.setMd_num_record(rs.getString("MD_NUM_RECORD"));
				obj.setMd_num_cuaa(rs.getString("MD_NUM_CUAA"));
				obj.setMd_num_sup_0(rs.getString("MD_NUM_SUP_0"));
				obj.setMd_amt_max_23(rs.getString("MD_AMT_MAX_23"));
				obj.setMd_amt_max_24(rs.getString("MD_AMT_MAX_24"));
				obj.setMd_amt_max_25(rs.getString("MD_AMT_MAX_25"));
				obj.setMd_amt_max_26(rs.getString("MD_AMT_MAX_26"));
				obj.setMd_pct_biss_23(rs.getString("MD_PCT_BISS_23"));
				obj.setMd_pct_biss_24(rs.getString("MD_PCT_BISS_24"));
				obj.setMd_pct_biss_25(rs.getString("MD_PCT_BISS_25"));
				obj.setMd_pct_biss_26(rs.getString("MD_PCT_BISS_26"));
				obj.setMd_num_min_sup_level(rs.getString("MD_NUM_MIN_SUP_LEVEL"));
				obj.setMd_amt_23(rs.getString("MD_AMT_23"));
				obj.setMd_amt_24(rs.getString("MD_AMT_24"));
				obj.setMd_amt_25(rs.getString("MD_AMT_25"));
				obj.setMd_amt_26(rs.getString("MD_AMT_26"));
				obj.setMd_amt_ha_23(rs.getString("MD_AMT_HA_23"));
				obj.setMd_amt_ha_24(rs.getString("MD_AMT_HA_24"));
				obj.setMd_amt_ha_25(rs.getString("MD_AMT_HA_25"));
				obj.setMd_amt_ha_26(rs.getString("MD_AMT_HA_26"));
				obj.setMd_num_sup_avg(rs.getString("MD_NUM_SUP_AVG"));
				obj.setMd_num_cuaa_avg(rs.getString("MD_NUM_CUAA_AVG"));
				obj.setMd_num_sup_0_avg(rs.getString("MD_NUM_SUP_0_AVG"));
				obj.setMd_amt_23_avg(rs.getString("MD_AMT_23_AVG"));
				obj.setMd_amt_24_avg(rs.getString("MD_AMT_24_AVG"));
				obj.setMd_amt_25_avg(rs.getString("MD_AMT_25_AVG"));
				obj.setMd_amt_26_avg(rs.getString("MD_AMT_26_AVG"));
				result.add(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

	private ArrayList<PrintModel2Values> getStampaModel2(String id_segnalazione) {
		ArrayList<PrintModel2Values> result = new ArrayList<PrintModel2Values>();
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			Statement stm = connection.createStatement();
			String sql = " SELECT " + " ID_MODEL, " + " COD_MODEL, " + " NAM_MODEL, " + " DES_MODEL, "
					+ " ID_MODEL_EXECUTION, " + " DAT_MODEL_EXECUTION, " + " NAM_MODEL_EXECUTION,  "
					+ " DES_MODEL_EXECUTION, " + " DAT_START, " + " DAT_END, " + " FLG_MODEL_EXECUTION_STATUS, "
					+ " ID_STATUS_JOB, " + " ID_JOB, " + " NAM_JOB, " + " JOB_SATUS_TYPE, " + " JOB_STATUS, "
					+ " FD_NUM_RECORD, " + " FD_NUM_CUAA, " + " FD_NUM_SUP_0, " + " MD_NUM_RECORD, " + " MD_NUM_CUAA, "
					+ " MD_NUM_SUP_0, " + " MD_NUM_MIN_SUP_LEVEL, " + " MD_ONLY_PURE, " + " MD_AMT_HA_23, "
					+ " MD_AMT_HA_24, " + " MD_AMT_HA_25, " + " MD_AMT_HA_26, " + " MD_PCT_BISS_23_X1, "
					+ " MD_PCT_BISS_23_X2, " + " MD_PCT_BISS_23_X3, " + " MD_PCT_BISS_24_X1, " + " MD_PCT_BISS_24_X2, "
					+ " MD_PCT_BISS_24_X3, " + " MD_PCT_BISS_25_X1, " + " MD_PCT_BISS_25_X2, " + " MD_PCT_BISS_25_X3, "
					+ " MD_PCT_BISS_26_X1, " + " MD_PCT_BISS_26_X2, " + " MD_PCT_BISS_26_X3, "
					+ " MD_NUM_SUP_RNG_23_X1, " + " MD_NUM_SUP_RNG_23_X2, " + " MD_NUM_SUP_RNG_23_X3, "
					+ " MD_NUM_SUP_RNG_24_X1, " + " MD_NUM_SUP_RNG_24_X2, " + " MD_NUM_SUP_RNG_24_X3, "
					+ " MD_NUM_SUP_RNG_25_X1, " + " MD_NUM_SUP_RNG_25_X2, " + " MD_NUM_SUP_RNG_25_X3, "
					+ " MD_NUM_SUP_RNG_26_X1, " + " MD_NUM_SUP_RNG_26_X2, " + " MD_NUM_SUP_RNG_26_X3, "
					+ " MD_NUM_CUAA_23, " + " MD_NUM_CUAA_24, " + " MD_NUM_CUAA_25, " + " MD_NUM_CUAA_26,"
					+ " MD_AMT_MAX_23, MD_AMT_MAX_24, MD_AMT_MAX_25, MD_AMT_MAX_26,  MD_PCT_CRISS_23, MD_PCT_CRISS_24, MD_PCT_CRISS_25, MD_PCT_CRISS_26, "
					+ " MD_NUM_SUP_23_X, " + " MD_NUM_SUP_24_X, " + " MD_NUM_SUP_25_X, " + " MD_NUM_SUP_26_X, " + " MD_AMT_23, "
					+ " MD_AMT_24, " + " MD_AMT_25, " + " MD_AMT_26, " + " MD_NUM_SUP_AVG, " + " MD_NUM_SUP_23_AVG, "
					+ " MD_NUM_SUP_24_AVG, " + " MD_NUM_SUP_25_AVG, " + " MD_NUM_SUP_26_AVG, " + " MD_AMT_HA_23, "
					+ " MD_AMT_HA_24, " + " MD_AMT_HA_25, " + " MD_AMT_HA_26, " + " MD_NUM_CUAA_23_X1, "
					+ " MD_NUM_SUP_23_X1, " + " MD_AMT_23_X1, " + " MD_NUM_SUP_23_X1_AVG, " + " MD_NUM_CUAA_23_X2, "
					+ " MD_NUM_SUP_23_X2, " + " MD_AMT_23_X2, " + " MD_NUM_SUP_23_X2_AVG, " + " MD_NUM_CUAA_23_X3, "
					+ " MD_NUM_SUP_23_X3, " + " MD_AMT_23_X3, " + " MD_NUM_SUP_23_X3_AVG, " + " MD_NUM_CUAA_24_X1, "
					+ " MD_NUM_SUP_24_X1, " + " MD_AMT_24_X1, " + " MD_NUM_SUP_24_X1_AVG, " + " MD_NUM_CUAA_24_X2, "
					+ " MD_NUM_SUP_24_X2, " + " MD_AMT_24_X2, " + " MD_NUM_SUP_24_X2_AVG, " + " MD_NUM_CUAA_24_X3, "
					+ " MD_NUM_SUP_24_X3, " + " MD_AMT_24_X3, " + " MD_NUM_SUP_24_X3_AVG, " + " MD_NUM_CUAA_25_X1, "
					+ " MD_NUM_SUP_25_X1, " + " MD_AMT_25_X1, " + " MD_NUM_SUP_25_X1_AVG, " + " MD_NUM_CUAA_25_X2, "
					+ " MD_NUM_SUP_25_X2, " + " MD_AMT_25_X2, " + " MD_NUM_SUP_25_X2_AVG, " + " MD_NUM_CUAA_25_X3, "
					+ " MD_NUM_SUP_25_X3, " + " MD_AMT_25_X3, " + " MD_NUM_SUP_25_X3_AVG, " + " MD_NUM_CUAA_26_X1, "
					+ " MD_NUM_SUP_26_X1, " + " MD_AMT_26_X1, " + " MD_NUM_SUP_26_X1_AVG, " + " MD_NUM_CUAA_26_X2, "
					+ " MD_NUM_SUP_26_X2, " + " MD_AMT_26_X2, " + " MD_NUM_SUP_26_X2_AVG, " + " MD_NUM_CUAA_26_X3, "
					+ " MD_NUM_SUP_26_X3, " + " MD_AMT_26_X3, " + " MD_NUM_SUP_26_X3_AVG " + " 	FROM  pyr_rep_"
					+ id_segnalazione;

			ResultSet rs = stm.executeQuery(sql);
			PrintModel2Values obj;
			while (rs.next()) {
				obj = new PrintModel2Values();
				obj.setId_model(rs.getString("ID_MODEL"));
				obj.setCod_model(rs.getString("COD_MODEL"));
				obj.setNam_model(rs.getString("NAM_MODEL"));
				obj.setDes_model(rs.getString("DES_MODEL"));
				obj.setId_model_execution(rs.getString("ID_MODEL_EXECUTION"));
				obj.setDat_model_execution(rs.getString("DAT_MODEL_EXECUTION"));
				obj.setNam_model_execution(rs.getString("NAM_MODEL_EXECUTION"));
				obj.setDes_model_execution(rs.getString("DES_MODEL_EXECUTION"));
				obj.setDat_start(rs.getString("DAT_START"));
				obj.setDat_end(rs.getString("DAT_END"));
				obj.setFlg_model_execution_status(rs.getString("FLG_MODEL_EXECUTION_STATUS"));
				obj.setId_status_job(rs.getString("ID_STATUS_JOB"));
				obj.setId_job(rs.getString("ID_JOB"));
				obj.setNam_job(rs.getString("NAM_JOB"));
				obj.setJob_satus_type(rs.getString("JOB_SATUS_TYPE"));
				obj.setJob_status(rs.getString("JOB_STATUS"));
				obj.setFd_num_record(rs.getString("FD_NUM_RECORD"));
				obj.setFd_num_cuaa(rs.getString("FD_NUM_CUAA"));
				obj.setFd_num_sup_0(rs.getString("FD_NUM_SUP_0"));
				obj.setMd_num_record(rs.getString("MD_NUM_RECORD"));
				obj.setMd_num_cuaa(rs.getString("MD_NUM_CUAA"));
				obj.setMd_num_sup_0(rs.getString("MD_NUM_SUP_0"));
				obj.setMd_num_min_sup_level(rs.getString("MD_NUM_MIN_SUP_LEVEL"));
				obj.setMd_only_pure(rs.getString("MD_ONLY_PURE"));
				obj.setMd_amt_ha_23(rs.getString("MD_AMT_HA_23"));
				obj.setMd_amt_ha_24(rs.getString("MD_AMT_HA_24"));
				obj.setMd_amt_ha_25(rs.getString("MD_AMT_HA_25"));
				obj.setMd_amt_ha_26(rs.getString("MD_AMT_HA_26"));
				obj.setMd_pct_biss_23_x1(rs.getString("MD_PCT_BISS_23_X1"));
				obj.setMd_pct_biss_23_x2(rs.getString("MD_PCT_BISS_23_X2"));
				obj.setMd_pct_biss_23_x3(rs.getString("MD_PCT_BISS_23_X3"));
				obj.setMd_pct_biss_24_x1(rs.getString("MD_PCT_BISS_24_X1"));
				obj.setMd_pct_biss_24_x2(rs.getString("MD_PCT_BISS_24_X2"));
				obj.setMd_pct_biss_24_x3(rs.getString("MD_PCT_BISS_24_X3"));
				obj.setMd_pct_biss_25_x1(rs.getString("MD_PCT_BISS_25_X1"));
				obj.setMd_pct_biss_25_x2(rs.getString("MD_PCT_BISS_25_X2"));
				obj.setMd_pct_biss_25_x3(rs.getString("MD_PCT_BISS_25_X3"));
				obj.setMd_pct_biss_26_x1(rs.getString("MD_PCT_BISS_26_X1"));
				obj.setMd_pct_biss_26_x2(rs.getString("MD_PCT_BISS_26_X2"));
				obj.setMd_pct_biss_26_x3(rs.getString("MD_PCT_BISS_26_X3"));
				obj.setMd_num_sup_rng_23_x1(rs.getString("MD_NUM_SUP_RNG_23_X1"));
				obj.setMd_num_sup_rng_23_x2(rs.getString("MD_NUM_SUP_RNG_23_X2"));
				obj.setMd_num_sup_rng_23_x3(rs.getString("MD_NUM_SUP_RNG_23_X3"));
				obj.setMd_num_sup_rng_24_x1(rs.getString("MD_NUM_SUP_RNG_24_X1"));
				obj.setMd_num_sup_rng_24_x2(rs.getString("MD_NUM_SUP_RNG_24_X2"));
				obj.setMd_num_sup_rng_24_x3(rs.getString("MD_NUM_SUP_RNG_24_X3"));
				obj.setMd_num_sup_rng_25_x1(rs.getString("MD_NUM_SUP_RNG_25_X1"));
				obj.setMd_num_sup_rng_25_x2(rs.getString("MD_NUM_SUP_RNG_25_X2"));
				obj.setMd_num_sup_rng_25_x3(rs.getString("MD_NUM_SUP_RNG_25_X3"));
				obj.setMd_num_sup_rng_26_x1(rs.getString("MD_NUM_SUP_RNG_26_X1"));
				obj.setMd_num_sup_rng_26_x2(rs.getString("MD_NUM_SUP_RNG_26_X2"));
				obj.setMd_num_sup_rng_26_x3(rs.getString("MD_NUM_SUP_RNG_26_X3"));
				obj.setMd_num_cuaa_23(rs.getString("MD_NUM_CUAA_23"));
				obj.setMd_num_cuaa_24(rs.getString("MD_NUM_CUAA_24"));
				obj.setMd_num_cuaa_25(rs.getString("MD_NUM_CUAA_25"));
				obj.setMd_num_cuaa_26(rs.getString("MD_NUM_CUAA_26"));
				obj.setMd_num_sup_23_x(rs.getString("MD_NUM_SUP_23_X"));
				obj.setMd_num_sup_24_x(rs.getString("MD_NUM_SUP_24_X"));
				obj.setMd_num_sup_25_x(rs.getString("MD_NUM_SUP_25_X"));
				obj.setMd_num_sup_26_x(rs.getString("MD_NUM_SUP_26_X"));
				obj.setAmt_max_23(rs.getString("MD_AMT_MAX_23"));
				obj.setAmt_max_24(rs.getString("MD_AMT_MAX_24"));
				obj.setAmt_max_25(rs.getString("MD_AMT_MAX_25"));
				obj.setAmt_max_26(rs.getString("MD_AMT_MAX_26"));
				obj.setPct_criss_23(rs.getString("MD_PCT_CRISS_23"));
				obj.setPct_criss_24(rs.getString("MD_PCT_CRISS_24"));
				obj.setPct_criss_25(rs.getString("MD_PCT_CRISS_25"));
				obj.setPct_criss_26(rs.getString("MD_PCT_CRISS_26"));
				obj.setMd_amt_23(rs.getString("MD_AMT_23"));
				obj.setMd_amt_24(rs.getString("MD_AMT_24"));
				obj.setMd_amt_25(rs.getString("MD_AMT_25"));
				obj.setMd_amt_26(rs.getString("MD_AMT_26"));
				obj.setMd_num_sup_avg(rs.getString("MD_NUM_SUP_AVG"));
				obj.setMd_num_sup_23_avg(rs.getString("MD_NUM_SUP_23_AVG"));
				obj.setMd_num_sup_24_avg(rs.getString("MD_NUM_SUP_24_AVG"));
				obj.setMd_num_sup_25_avg(rs.getString("MD_NUM_SUP_25_AVG"));
				obj.setMd_num_sup_26_avg(rs.getString("MD_NUM_SUP_26_AVG"));
				obj.setMd_num_cuaa_23_x1(rs.getString("MD_NUM_CUAA_23_X1"));
				obj.setMd_num_sup_23_x1(rs.getString("MD_NUM_SUP_23_X1"));
				obj.setMd_amt_23_x1(rs.getString("MD_AMT_23_X1"));
				obj.setMd_num_sup_23_x1_avg(rs.getString("MD_NUM_SUP_23_X1_AVG"));
				obj.setMd_num_cuaa_23_x2(rs.getString("MD_NUM_CUAA_23_X2"));
				obj.setMd_num_sup_23_x2(rs.getString("MD_NUM_SUP_23_X2"));
				obj.setMd_amt_23_x2(rs.getString("MD_AMT_23_X2"));
				obj.setMd_num_sup_23_x2_avg(rs.getString("MD_NUM_SUP_23_X2_AVG"));
				obj.setMd_num_cuaa_23_x3(rs.getString("MD_NUM_CUAA_23_X3"));
				obj.setMd_num_sup_23_x3(rs.getString("MD_NUM_SUP_23_X3"));
				obj.setMd_amt_23_x3(rs.getString("MD_AMT_23_X3"));
				obj.setMd_num_sup_23_x3_avg(rs.getString("MD_NUM_SUP_23_X3_AVG"));
				obj.setMd_num_cuaa_24_x1(rs.getString("MD_NUM_CUAA_24_X1"));
				obj.setMd_num_sup_24_x1(rs.getString("MD_NUM_SUP_24_X1"));
				obj.setMd_amt_24_x1(rs.getString("MD_AMT_24_X1"));
				obj.setMd_num_sup_24_x1_avg(rs.getString("MD_NUM_SUP_24_X1_AVG"));
				obj.setMd_num_cuaa_24_x2(rs.getString("MD_NUM_CUAA_24_X2"));
				obj.setMd_num_sup_24_x2(rs.getString("MD_NUM_SUP_24_X2"));
				obj.setMd_amt_24_x2(rs.getString("MD_AMT_24_X2"));
				obj.setMd_num_sup_24_x2_avg(rs.getString("MD_NUM_SUP_24_X2_AVG"));
				obj.setMd_num_cuaa_24_x3(rs.getString("MD_NUM_CUAA_24_X3"));
				obj.setMd_num_sup_24_x3(rs.getString("MD_NUM_SUP_24_X3"));
				obj.setMd_amt_24_x3(rs.getString("MD_AMT_24_X3"));
				obj.setMd_num_sup_24_x3_avg(rs.getString("MD_NUM_SUP_24_X3_AVG"));
				obj.setMd_num_cuaa_25_x1(rs.getString("MD_NUM_CUAA_25_X1"));
				obj.setMd_num_sup_25_x1(rs.getString("MD_NUM_SUP_25_X1"));
				obj.setMd_amt_25_x1(rs.getString("MD_AMT_25_X1"));
				obj.setMd_num_sup_25_x1_avg(rs.getString("MD_NUM_SUP_25_X1_AVG"));
				obj.setMd_num_cuaa_25_x2(rs.getString("MD_NUM_CUAA_25_X2"));
				obj.setMd_num_sup_25_x2(rs.getString("MD_NUM_SUP_25_X2"));
				obj.setMd_amt_25_x2(rs.getString("MD_AMT_25_X2"));
				obj.setMd_num_sup_25_x2_avg(rs.getString("MD_NUM_SUP_25_X2_AVG"));
				obj.setMd_num_cuaa_25_x3(rs.getString("MD_NUM_CUAA_25_X3"));
				obj.setMd_num_sup_25_x3(rs.getString("MD_NUM_SUP_25_X3"));
				obj.setMd_amt_25_x3(rs.getString("MD_AMT_25_X3"));
				obj.setMd_num_sup_25_x3_avg(rs.getString("MD_NUM_SUP_25_X3_AVG"));
				obj.setMd_num_cuaa_26_x1(rs.getString("MD_NUM_CUAA_26_X1"));
				obj.setMd_num_sup_26_x1(rs.getString("MD_NUM_SUP_26_X1"));
				obj.setMd_amt_26_x1(rs.getString("MD_AMT_26_X1"));
				obj.setMd_num_sup_26_x1_avg(rs.getString("MD_NUM_SUP_26_X1_AVG"));
				obj.setMd_num_cuaa_26_x2(rs.getString("MD_NUM_CUAA_26_X2"));
				obj.setMd_num_sup_26_x2(rs.getString("MD_NUM_SUP_26_X2"));
				obj.setMd_amt_26_x2(rs.getString("MD_AMT_26_X2"));
				obj.setMd_num_sup_26_x2_avg(rs.getString("MD_NUM_SUP_26_X2_AVG"));
				obj.setMd_num_cuaa_26_x3(rs.getString("MD_NUM_CUAA_26_X3"));
				obj.setMd_num_sup_26_x3(rs.getString("MD_NUM_SUP_26_X3"));
				obj.setMd_amt_26_x3(rs.getString("MD_AMT_26_X3"));
				obj.setMd_num_sup_26_x3_avg(rs.getString("MD_NUM_SUP_26_X3_AVG"));

				result.add(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

	private ArrayList<PrintModel3Values> getStampaModel3(String id_segnalazione) {
		/*
		 * ################################################## REPORT 01-Pay to hectare
		 * ##################################################
		 */

		ArrayList<PrintModel3Values> result = new ArrayList<PrintModel3Values>();
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());

			Statement stm = connection.createStatement();
			String sql = " SELECT ID_MODEL, " + " COD_MODEL, " + " NAM_MODEL, " + " DES_MODEL, "
					+ " ID_MODEL_EXECUTION, " + " DAT_MODEL_EXECUTION, " + " NAM_MODEL_EXECUTION, "
					+ " DES_MODEL_EXECUTION, " + " DAT_START, " + " DAT_END, " + " FLG_MODEL_EXECUTION_STATUS, "
					+ " ID_STATUS_JOB, " + " ID_JOB, " + " NAM_JOB, " + " JOB_SATUS_TYPE, " + " JOB_STATUS, "
					+ " FD_NUM_RECORD, " + " FD_NUM_CUAA, " + " FD_NUM_ENTITLEMENT, " + " FD_NUM_SUP_22, "
					+ " FD_AMT_BPS_22, " + " MD_NUM_RECORD, " + " MD_NUM_CUAA, " + " MD_NUM_ENTITLEMENT, "
					+ " MD_NUM_SUP_22, " + " MD_AMT_BPS_22, " + " MD_AMT_MAX_23, " + " MD_AMT_MAX_24, "
					+ " MD_AMT_MAX_25, " + " MD_AMT_MAX_26, " + " MD_PCT_BISS_23, " + " MD_PCT_BISS_24, "
					+ " MD_PCT_BISS_25, " + " MD_PCT_BISS_26, " + " MD_PCT_GRE, " + " MD_AMT_HA_GRE, " + " MD_PCT_AUA, "
					+ " MD_AMT_MAX_LEVEL, " + " MD_NUM_YEAR_MAX_LEVEL, " + " MD_NUM_YEARS_CONV, " + " MD_PCT_SL, "
					+ " MD_NUM_CY_SL, " + " MD_NUM_MAX_SUP_LEVEL, " + " MD_AMT_PE_23, " + " MD_AMT_PE_24, "
					+ " MD_AMT_PE_25, " + " MD_AMT_PE_26, " + " MD_AMT_HA_AUA, " + " MD_AVG_UNIT_AMT_01, "
					+ " MD_AVG_UNIT_AMT_02, " + " MD_NUM_CUAA_NEED, " + " MD_NUM_ENTITLEMENT_NEED, "
					+ " MD_NUM_CUAA_CONT_23, " + " MD_NUM_ENTITLEMENT_CONT_23, " + " MD_AMT_SUM_PE_NEED, "
					+ " MD_AMT_SUM_YEAR_PE_NEED, " + " MD_AMT_SUM_CONT_23, " + " MD_AMT_SUM_CONT_24, "
					+ " MD_AMT_SUM_CONT_25, " + " MD_AMT_SUM_CONT_26, " + " MD_AMT_SUM_DTA_MAX_LEVEL_23, "
					+ " MD_AMT_SUM_DTA_MAX_LEVEL_24, " + " MD_AMT_SUM_DTA_MAX_LEVEL_25, "
					+ " MD_AMT_SUM_DTA_MAX_LEVEL_26, " + " MD_NUM_CUAA_MAX_23, " + " MD_NUM_CUAA_MAX_24, "
					+ " MD_NUM_CUAA_MAX_25, " + " MD_NUM_CUAA_MAX_26, " + " MD_NUM_ENTITLEMENT_MAX_23, "
					+ " MD_NUM_ENTITLEMENT_MAX_24, " + " MD_NUM_ENTITLEMENT_MAX_25, " + " MD_NUM_ENTITLEMENT_MAX_26, "
					+ " MD_NUM_CUAA_SL_23, " + " MD_NUM_CUAA_SL_24, " + " MD_NUM_CUAA_SL_25, " + " MD_NUM_CUAA_SL_26, "
					+ " MD_NUM_ENTITLEMENT_SL_23, " + " MD_NUM_ENTITLEMENT_SL_24, " + " MD_NUM_ENTITLEMENT_SL_25, "
					+ " MD_NUM_ENTITLEMENT_SL_26, " + " MD_AMT_SUM_PE_SL_NEED_23, " + " MD_AMT_SUM_PE_SL_NEED_24, "
					+ " MD_AMT_SUM_PE_SL_NEED_25, " + " MD_AMT_SUM_PE_SL_NEED_26 " + " 	FROM  pye_rep_"
					+ id_segnalazione;

			ResultSet rs = stm.executeQuery(sql);
			PrintModel3Values obj;
			while (rs.next()) {
				obj = new PrintModel3Values();
				obj.setId_model(rs.getString("ID_MODEL"));
				obj.setCod_model(rs.getString("COD_MODEL"));
				obj.setNam_model(rs.getString("NAM_MODEL"));
				obj.setDes_model(rs.getString("DES_MODEL"));
				obj.setId_model_execution(rs.getString("ID_MODEL_EXECUTION"));
				obj.setDat_model_execution(rs.getString("DAT_MODEL_EXECUTION"));
				obj.setNam_model_execution(rs.getString("NAM_MODEL_EXECUTION"));
				obj.setDes_model_execution(rs.getString("DES_MODEL_EXECUTION"));
				obj.setDat_start(rs.getString("DAT_START"));
				obj.setDat_end(rs.getString("DAT_END"));
				obj.setFlg_model_execution_status(rs.getString("FLG_MODEL_EXECUTION_STATUS"));
				obj.setId_status_job(rs.getString("ID_STATUS_JOB"));
				obj.setId_job(rs.getString("ID_JOB"));
				obj.setNam_job(rs.getString("NAM_JOB"));
				obj.setJob_satus_type(rs.getString("JOB_SATUS_TYPE"));
				obj.setJob_status(rs.getString("JOB_STATUS"));
				obj.setFd_num_record(rs.getString("FD_NUM_RECORD"));
				obj.setFd_num_cuaa(rs.getString("FD_NUM_CUAA"));
				obj.setFd_num_entitlement(rs.getString("FD_NUM_ENTITLEMENT"));
				obj.setFd_num_sup_22(rs.getString("FD_NUM_SUP_22"));
				obj.setFd_amt_bps_22(rs.getString("FD_AMT_BPS_22"));
				obj.setMd_num_record(rs.getString("MD_NUM_RECORD"));
				obj.setMd_num_cuaa(rs.getString("MD_NUM_CUAA"));
				obj.setMd_num_entitlement(rs.getString("MD_NUM_ENTITLEMENT"));
				obj.setMd_num_sup_22(rs.getString("MD_NUM_SUP_22"));
				obj.setMd_amt_bps_22(rs.getString("MD_AMT_BPS_22"));
				obj.setMd_amt_max_23(rs.getString("MD_AMT_MAX_23"));
				obj.setMd_amt_max_24(rs.getString("MD_AMT_MAX_24"));
				obj.setMd_amt_max_25(rs.getString("MD_AMT_MAX_25"));
				obj.setMd_amt_max_26(rs.getString("MD_AMT_MAX_26"));
				obj.setMd_pct_biss_23(rs.getString("MD_PCT_BISS_23"));
				obj.setMd_pct_biss_24(rs.getString("MD_PCT_BISS_24"));
				obj.setMd_pct_biss_25(rs.getString("MD_PCT_BISS_25"));
				obj.setMd_pct_biss_26(rs.getString("MD_PCT_BISS_26"));
				obj.setMd_pct_gre(rs.getString("MD_PCT_GRE"));
				obj.setMd_amt_ha_gre(rs.getString("MD_AMT_HA_GRE"));
				obj.setMd_pct_aua(rs.getString("MD_PCT_AUA"));
				obj.setMd_amt_max_level(rs.getString("MD_AMT_MAX_LEVEL"));
				obj.setMd_num_year_max_level(rs.getString("MD_NUM_YEAR_MAX_LEVEL"));
				obj.setMd_num_years_conv(rs.getString("MD_NUM_YEARS_CONV"));
				obj.setMd_pct_sl(rs.getString("MD_PCT_SL"));
				obj.setMd_num_cy_sl(rs.getString("MD_NUM_CY_SL"));
				obj.setMd_num_max_sup_level(rs.getString("MD_NUM_MAX_SUP_LEVEL"));
				obj.setMd_amt_pe_23(rs.getString("MD_AMT_PE_23"));
				obj.setMd_amt_pe_24(rs.getString("MD_AMT_PE_24"));
				obj.setMd_amt_pe_25(rs.getString("MD_AMT_PE_25"));
				obj.setMd_amt_pe_26(rs.getString("MD_AMT_PE_26"));
				obj.setMd_amt_ha_aua(rs.getString("MD_AMT_HA_AUA"));
				obj.setMd_avg_unit_amt_01(rs.getString("MD_AVG_UNIT_AMT_01"));
				obj.setMd_avg_unit_amt_02(rs.getString("MD_AVG_UNIT_AMT_02"));
				obj.setMd_num_cuaa_need(rs.getString("MD_NUM_CUAA_NEED"));
				obj.setMd_num_entitlement_need(rs.getString("MD_NUM_ENTITLEMENT_NEED"));
				obj.setMd_num_cuaa_cont_23(rs.getString("MD_NUM_CUAA_CONT_23"));
				obj.setMd_num_entitlement_cont_23(rs.getString("MD_NUM_ENTITLEMENT_CONT_23"));
				obj.setMd_amt_sum_pe_need(rs.getString("MD_AMT_SUM_PE_NEED"));
				obj.setMd_amt_sum_year_pe_need(rs.getString("MD_AMT_SUM_YEAR_PE_NEED"));
				obj.setMd_amt_sum_cont_23(rs.getString("MD_AMT_SUM_CONT_23"));
				obj.setMd_amt_sum_cont_24(rs.getString("MD_AMT_SUM_CONT_24"));
				obj.setMd_amt_sum_cont_25(rs.getString("MD_AMT_SUM_CONT_25"));
				obj.setMd_amt_sum_cont_26(rs.getString("MD_AMT_SUM_CONT_26"));
				obj.setMd_amt_sum_dta_max_level_23(rs.getString("MD_AMT_SUM_DTA_MAX_LEVEL_23"));
				obj.setMd_amt_sum_dta_max_level_24(rs.getString("MD_AMT_SUM_DTA_MAX_LEVEL_24"));
				obj.setMd_amt_sum_dta_max_level_25(rs.getString("MD_AMT_SUM_DTA_MAX_LEVEL_25"));
				obj.setMd_amt_sum_dta_max_level_26(rs.getString("MD_AMT_SUM_DTA_MAX_LEVEL_26"));
				obj.setMd_num_cuaa_sl_23(rs.getString("MD_NUM_CUAA_SL_23"));
				obj.setMd_num_cuaa_sl_24(rs.getString("MD_NUM_CUAA_SL_24"));
				obj.setMd_num_cuaa_sl_25(rs.getString("MD_NUM_CUAA_SL_25"));
				obj.setMd_num_cuaa_sl_26(rs.getString("MD_NUM_CUAA_SL_26"));
				obj.setMd_num_entitlement_sl_23(rs.getString("MD_NUM_ENTITLEMENT_SL_23"));
				obj.setMd_num_entitlement_sl_24(rs.getString("MD_NUM_ENTITLEMENT_SL_24"));
				obj.setMd_num_entitlement_sl_25(rs.getString("MD_NUM_ENTITLEMENT_SL_25"));
				obj.setMd_num_entitlement_sl_26(rs.getString("MD_NUM_ENTITLEMENT_SL_26"));
				obj.setMd_amt_sum_pe_sl_need_23(rs.getString("MD_AMT_SUM_PE_SL_NEED_23"));
				obj.setMd_amt_sum_pe_sl_need_24(rs.getString("MD_AMT_SUM_PE_SL_NEED_24"));
				obj.setMd_amt_sum_pe_sl_need_25(rs.getString("MD_AMT_SUM_PE_SL_NEED_25"));
				obj.setMd_amt_sum_pe_sl_need_26(rs.getString("MD_AMT_SUM_PE_SL_NEED_26"));
				obj.setMd_num_cuaa_max_23(rs.getString("MD_NUM_CUAA_MAX_23"));
				obj.setMd_num_cuaa_max_24(rs.getString("MD_NUM_CUAA_MAX_24"));
				obj.setMd_num_cuaa_max_25(rs.getString("MD_NUM_CUAA_MAX_25"));
				obj.setMd_num_cuaa_max_26(rs.getString("MD_NUM_CUAA_MAX_26"));
				obj.setMd_num_entitlement_max_23(rs.getString("MD_NUM_ENTITLEMENT_MAX_23"));
				obj.setMd_num_entitlement_max_24(rs.getString("MD_NUM_ENTITLEMENT_MAX_24"));
				obj.setMd_num_entitlement_max_25(rs.getString("MD_NUM_ENTITLEMENT_MAX_25"));
				obj.setMd_num_entitlement_max_26(rs.getString("MD_NUM_ENTITLEMENT_MAX_26"));

				result.add(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

	private static void addMetaData(Document document) {
		document.addTitle("My first PDF");
		document.addSubject("Using iText");
		document.addKeywords("Java, PDF, iText");
		document.addAuthor("Lars Vogel");
		document.addCreator("Lars Vogel");
	}

	private static void addTitlePage(Document document) throws DocumentException {
		Paragraph preface = new Paragraph();
		// We add one empty line
		addEmptyLine(preface, 1);
		// Lets write a big header
		preface.add(new Paragraph("Title of the document", catFont));

		addEmptyLine(preface, 1);
		// Will create: Report generated by: _name, _date
		preface.add(new Paragraph("Report generated by: " + System.getProperty("user.name") + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				smallBold));
		addEmptyLine(preface, 3);
		preface.add(new Paragraph("This document describes something which is very important ", smallBold));

		addEmptyLine(preface, 8);

		preface.add(new Paragraph(
				"This document is a preliminary version and not subject to your license agreement or any other agreement with vogella.com ;-).",
				redFont));

		document.add(preface);
		// Start a new page
		document.newPage();
	}

	private static void addContentModel1(Document document, ArrayList<PrintModel1Values> list)
			throws DocumentException {

		Paragraph preface = new Paragraph();
		// We add one empty line
		addEmptyLine(preface, 1);
		// Lets write a big header

		for (PrintModel1Values model : list) {
			preface.add(
					new Paragraph(model.getId_model_execution() + " - " + model.getNam_model().toUpperCase(), catFont));

			addEmptyLine(preface, 1);

			document.add(preface);
			// add a table
			createTableModel1(document, model);
		}

	}

	private static void createTableModel1(Document document, PrintModel1Values obj) {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		PdfPCell cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		PdfPCell tHeader = new PdfPCell(
				new Phrase("EXECUTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		PdfPCell c1 = new PdfPCell(new Phrase("Model", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution ID", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		Paragraph p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getNam_model() + "\n");
		p.add("\n");
		p.add(obj.getDes_model() + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getId_model_execution(), new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Date", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Status", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getDat_model_execution() + "\n");
		p.add("\n");
		p.add(obj.getDat_end() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getJob_satus_type() + "\n");
		p.add("\n");
		p.add(obj.getJob_status() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		table = new PdfPTable(2);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************** BEGIN CONTACTS ******************/

		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		tHeader = new PdfPCell(new Phrase("CONTACTS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		c1 = new PdfPCell(new Phrase("Algorithm Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("IT Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Fabio Pierangeli" + "\n");
		p.add("\n");
		p.add("fabio.pierangeli@crea.gov.it" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Luca Ruscio" + "\n");
		p.add("\n");
		p.add("l.ruscio@tecso.biz" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/************** END CONTACTS ******************/

		/********************************/

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Minimum Area");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_min_sup_level() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_min_sup_level())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_sup_0() == null ? "" : String.format("%,.2f", Double.parseDouble(obj.getFd_num_sup_0())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_0() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_0())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Unit Amount 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Unit Amount 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Unit Amount 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Unit Amount 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_avg() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_avg())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_0_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_0_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023 with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23_avg())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024 with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025 with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25_avg())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026 with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************* LEGEND *****************/

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("MS Ceiling Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("National ceiling for direct payments for claim year N as laid down in Annex IV");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% BISS Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Percentage of national ceiling allocated to Basic income support for sustainability (BISS) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Minimum Area");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Minimum area (art. 18). By default, set equal to 1 ha");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total amount of Basic income support for sustainability (BISS) distributed for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average unit amount Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual decoupled payment per eligible hectare for claim year N. (Total Amount/N. Hectares)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average Farm Size (ha)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average farm size expressed in hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa) with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms having a farm size lower that the Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Hectares with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares held by farms having a farm size lower than the Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N with farm size < Average Farm Size");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total amount of BISS received by farms having a farm size lower than the Average Farm Size for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void createList(Section subCatPart) {
		List list = new List(true, false, 10);
		list.add(new ListItem("First point"));
		list.add(new ListItem("Second point"));
		list.add(new ListItem("Third point"));
		subCatPart.add(list);
	}

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

	/*
	 * ************************************************** MODEL 2 *
	 ****************************************************/

	private static void addContentModel2(Document document, ArrayList<PrintModel2Values> list)
			throws DocumentException {

		Paragraph preface = new Paragraph();
		// We add one empty line
		addEmptyLine(preface, 1);
		// Lets write a big header

		for (PrintModel2Values model : list) {
			preface.add(
					new Paragraph(model.getId_model_execution() + " - " + model.getNam_model().toUpperCase(), catFont));

			addEmptyLine(preface, 1);

			document.add(preface);
			// add a table
			createTableModel2(document, model);
		}

	}

	private static void createTableModel2(Document document, PrintModel2Values obj) {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		PdfPCell cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		PdfPCell tHeader = new PdfPCell(
				new Phrase("EXECUTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		PdfPCell c1 = new PdfPCell(new Phrase("Model", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution ID", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		Paragraph p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getNam_model() + "\n");
		p.add("\n");
		p.add(obj.getDes_model() + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getId_model_execution(), new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Date", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Status", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getDat_model_execution() + "\n");
		p.add("\n");
		p.add(obj.getDat_end() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getJob_satus_type() + "\n");
		p.add("\n");
		p.add(obj.getJob_status() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/********************************/

		table = new PdfPTable(2);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************** BEGIN CONTACTS ******************/

		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		tHeader = new PdfPCell(new Phrase("CONTACTS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		c1 = new PdfPCell(new Phrase("Algorithm Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("IT Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Fabio Pierangeli" + "\n");
		p.add("\n");
		p.add("fabio.pierangeli@crea.gov.it" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Luca Ruscio" + "\n");
		p.add("\n");
		p.add("l.ruscio@tecso.biz" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/************** END CONTACTS ******************/

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 5, 3, 2, 2 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(4);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS CEILING 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getAmt_max_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getAmt_max_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% CRISS 23");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getPct_criss_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getPct_criss_23())), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (1) (FROM - TO) 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_23_x1(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2023 (1)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_23_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_23_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (2) (FROM - TO) 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_23_x2(), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2023 (2)");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_23_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_23_x2())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (3) (FROM - TO) 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_23_x3(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2023 (3)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_23_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_23_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS CEILING 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getAmt_max_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getAmt_max_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% CRISS 24");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getPct_criss_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getPct_criss_24())), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (1) (FROM - TO) 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_24_x1(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2024 (1)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_24_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_24_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (2) (FROM - TO) 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_24_x2(), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2024 (2)");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_24_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_24_x2())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (3) (FROM - TO) 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_24_x3(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2024 (3)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_24_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_24_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS CEILING 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getAmt_max_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getAmt_max_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% CRISS 25");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getPct_criss_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getPct_criss_25())), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (1) (FROM - TO) 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_25_x1(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2025 (1)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_25_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_25_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (2) (FROM - TO) 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_25_x2(), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2025 (2)");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_25_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_25_x2())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (3) (FROM - TO) 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_25_x3(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2025 (3)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_25_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_25_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS CEILING 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getAmt_max_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getAmt_max_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% CRISS 26");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getPct_criss_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getPct_criss_26())), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (1) (FROM - TO) 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_26_x1(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2026 (1)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_26_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_26_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (2) (FROM - TO) 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_26_x2(), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2026 (2)");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_26_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_26_x2())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("RANGE (3) (FROM - TO) 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_num_sup_rng_26_x3(), new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% ADP 2026 (3)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_26_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_26_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("CRISS");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getMd_only_pure(), new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Minimum Area");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_min_sup_level() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_min_sup_level())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_sup_0() == null ? "" : String.format("%,.2f", Double.parseDouble(obj.getFd_num_sup_0())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_0() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_0())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year 2023");
		c1 = new PdfPCell(p);
		c1.setColspan(2);
		c1.setBackgroundColor(colorHeaderTable);
		c1.setPadding(4);
		table.addCell(c1);

//	        c1 = new PdfPCell(new Phrase(String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24())),new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
//	        c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	        c1.setPadding(4);
//	        table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_23() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_23_x() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_23_x())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Decoupled Payment Per Ha 2023 (�/Ha)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_23())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		
		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha) 2023");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_23_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_23_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2023 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_23_x1() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_23_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2023 - Range(X1)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_23_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_23_x1())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23_x1() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2023 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_23_x2() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_23_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2023 - Range(X2)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_23_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_23_x2())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23_x2() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2023 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_23_x3() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_23_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2023 - Range(X3)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_23_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_23_x3())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_23_x3() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_23_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		
	

		

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year 2024");
		c1 = new PdfPCell(p);
		c1.setColspan(2);
		c1.setBackgroundColor(colorHeaderTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_24() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_24_x() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_24_x())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Decoupled Payment Per Ha 2024 (�/Ha)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_24())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		
		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha) 2024");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_24_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_24_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2024 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_24_x1() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_24_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2024 - Range(X1)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_24_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_24_x1())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24_x1() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2024 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_24_x2() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_24_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2024 - Range(X2)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_24_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_24_x2())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24_x2() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2024 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_24_x3() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_24_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2024 - Range(X3)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_24_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_24_x3())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_24_x3() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_24_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year 2025");
		c1 = new PdfPCell(p);
		c1.setColspan(2);
		c1.setBackgroundColor(colorHeaderTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_25() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_25_x() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_25_x())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Decoupled Payment Per Ha 2025 (�/Ha)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_25())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha) 2025");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_25_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_25_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2025 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_25_x1() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_25_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2025 - Range(X1)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_25_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_25_x1())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25_x1() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2025 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_25_x2() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_25_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2025 - Range(X2)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_25_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_25_x2())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25_x2() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2025 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_25_x3() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_25_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2025 - Range(X3)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_25_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_25_x3())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_25_x3() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_25_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year 2026");
		c1 = new PdfPCell(p);
		c1.setColspan(2);
		c1.setBackgroundColor(colorHeaderTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_26() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_26_x() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_26_x())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Decoupled Payment Per Ha 2026 (�/Ha)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_26())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Farm Size (ha) 2026");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_26_avg() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_26_avg())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2026 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_26_x1() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_26_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2026 - Range(X1)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_26_x1() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_26_x1())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026 - Range(X1)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26_x1() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26_x1())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2026 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_26_x2() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_26_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2026 - Range(X2)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_26_x2() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_26_x2())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026 - Range(X2)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26_x2() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26_x2())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add(" ");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(" ", new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa) 2026 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_26_x3() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_26_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares 2026 - Range(X3)");
		c1 = new PdfPCell(p);
	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_26_x3() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_26_x3())),
				new Font(FontFamily.HELVETICA, 10)));
	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026 - Range(X3)");
		c1 = new PdfPCell(p);
//		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_26_x3() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_26_x3())),
				new Font(FontFamily.HELVETICA, 10)));
//		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************* LEGEND *****************/

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("MS CEILING YEAR N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("National ceiling for direct payments for claim year N as laid down in Annex IV");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% CRISS N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Percentage of national ceiling allocated to complementary redistributive income support for sustainability (CRISS) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);
		
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("RANGE (1) (FROM - TO) Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("First ranges of hectares (from � to) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% ADP Year N (Range 1)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual decoupled payment percentual weight by range 1 for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("RANGE (2) (FROM - TO) Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Second ranges of hectares (from � to) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% ADP Year N (Range 2)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual decoupled payment percentual weight by range 2 for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("RANGE (3) (FROM - TO) Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Third Ranges of hectares (from � to) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% ADP Year N (Range 3)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual decoupled payment percentual weight by range 3 for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("CRISS");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Select the type of model: amount per first hectare (first hectares = NO) or amount only for farms size belonging to the ranges (pure CRISS = YES)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Minimum Area");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Minimum area (art. 18). By default, set equal to 1 ha");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Decoupled Payment Per Ha Year N (�/Ha)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual decoupled payment per eligible hectare (claim year N)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average Farm Size (ha)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average farm size in hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(new Phrase("Year N", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(new Phrase("", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa) Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of Complementary redistributive income support for sustainability (CRISS) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average farm size (ha) Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average farm size in hectares (claim year N)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(new Phrase("RANGE 1", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(new Phrase("", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa) Year N - Range(X1)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms for claim year N belonging to the range n. 1");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares Year N � Range (X1)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares for claim year N belonging to the range n. 1");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N � Range (X1)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of Complementary redistributive income support for sustainability (CRISS) for claim year N received by farms belonging to the range n.1");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(new Phrase("RANGE 2", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(new Phrase("", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa) Year N - Range(X2)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms for claim year N belonging to the range n. 2");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares Year N � Range (X2)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares for claim year N belonging to the range n. 2");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N � Range (X2)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of Complementary redistributive income support for sustainability (CRISS) for claim year N received by farms belonging to the range n.2");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(new Phrase("RANGE 3", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(new Phrase("", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa) Year N - Range(X3)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible farms for claim year N belonging to the range n. 3");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. hectares Year N � Range (X3)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of eligible hectares for claim year N belonging to the range n. 3");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N � Range (X3)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of Complementary redistributive income support for sustainability (CRISS) for claim year N received by farms belonging to the range n.3");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * ************************************************** MODEL 3 *
	 ****************************************************/

	private static void addContentModel3(Document document, ArrayList<PrintModel3Values> list)
			throws DocumentException {

		Paragraph preface = new Paragraph();
		// We add one empty line
		addEmptyLine(preface, 1);
		// Lets write a big header

		for (PrintModel3Values model : list) {
			preface.add(
					new Paragraph(model.getId_model_execution() + " - " + model.getNam_model().toUpperCase(), catFont));

			addEmptyLine(preface, 1);

			document.add(preface);
			// add a table
			createTableModel3(document, model);
		}

	}

	private static void createTableModel3(Document document, PrintModel3Values obj) {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		PdfPCell cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		PdfPCell tHeader = new PdfPCell(
				new Phrase("EXECUTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		PdfPCell c1 = new PdfPCell(new Phrase("Model", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution ID", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		Paragraph p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getNam_model() + "\n");
		p.add("\n");
		p.add(obj.getDes_model() + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(obj.getId_model_execution(), new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Date", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Execution Status", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getDat_model_execution() + "\n");
		p.add("\n");
		p.add(obj.getDat_end() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add(obj.getJob_satus_type() + "\n");
		p.add("\n");
		p.add(obj.getJob_status() + "\n");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/********************************/

		table = new PdfPTable(2);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************** BEGIN CONTACTS ******************/

		table = new PdfPTable(2);
		table.setWidthPercentage(100);
		cEmpty = new PdfPCell(new Phrase(" "));
		cEmpty.setBackgroundColor(colorRowTable);

		tHeader = new PdfPCell(new Phrase("CONTACTS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		c1 = new PdfPCell(new Phrase("Algorithm Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("IT Project Manager", new Font(FontFamily.HELVETICA, 10, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Fabio Pierangeli" + "\n");
		p.add("\n");
		p.add("fabio.pierangeli@crea.gov.it" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10));
		p.add("Luca Ruscio" + "\n");
		p.add("\n");
		p.add("l.ruscio@tecso.biz" + "\n");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/************** END CONTACTS ******************/

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("MS Ceiling 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% BISS 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_biss_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_pct_biss_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Years of Convergence");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_years_conv() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_years_conv())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Max value of PE (�/Ha)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_max_level() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_max_level())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Area for activation of PE");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_max_sup_level() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_max_sup_level())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year max value");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_year_max_level() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_year_max_level())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% Greening");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_gre() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_pct_gre())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Amount Greening (�/Ha)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_gre() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_gre())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("% AUA");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_aua() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_pct_aua())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Maximum decreas");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_pct_sl() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_pct_sl())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Max Cycles SL");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cy_sl() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_num_cy_sl())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getFd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Entitlements");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_entitlement() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getFd_num_entitlement())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_num_sup_22() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getFd_num_sup_22())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getFd_amt_bps_22() == null ? ""
						: String.format("%,.2f", Double.parseDouble(obj.getFd_amt_bps_22())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		table = new PdfPTable(new float[] { 8, 4 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setColspan(2);
		tHeader.setBackgroundColor(colorHeaderTable);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_record() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_record())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa() == null ? "" : String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Entitlements");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_sup_22() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_num_sup_22())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2023");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_pe_23() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_pe_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2024");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_pe_24() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_pe_24())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2025");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_pe_25() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_pe_25())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total Amount 2026");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_pe_26() == null ? "" : String.format("%,.4f", Double.parseDouble(obj.getMd_amt_pe_26())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		/*
		 * p = new Paragraph(); p.setFont(new Font(FontFamily.HELVETICA, 10,
		 * Font.BOLD)); p.add("Average Unit Amount (Total Amount/N. Entitlements)"); c1=
		 * new PdfPCell(p); c1.setBackgroundColor(colorRowTable); c1.setPadding(4);
		 * table.addCell(c1);
		 * 
		 * c1 = new PdfPCell(new Phrase(obj.getMd_avg_unit_amt_01()==null ? "":
		 * String.format("%,.4f", Double.parseDouble(obj.getMd_avg_unit_amt_01())),new
		 * Font(FontFamily.HELVETICA, 10))); c1.setBackgroundColor(colorRowTable);
		 * c1.setHorizontalAlignment(Element.ALIGN_RIGHT); c1.setPadding(4);
		 * table.addCell(c1);
		 * 
		 * 
		 * p = new Paragraph(); p.setFont(new Font(FontFamily.HELVETICA, 10,
		 * Font.BOLD)); p.add("Average Unit Amount (Total Amount/N. Hectares)"); c1= new
		 * PdfPCell(p); // c1.setBackgroundColor(colorRowTable); c1.setPadding(4);
		 * table.addCell(c1);
		 * 
		 * c1 = new PdfPCell(new Phrase(obj.getMd_avg_unit_amt_02()==null ? "":
		 * String.format("%,.4f", Double.parseDouble(obj.getMd_avg_unit_amt_02())),new
		 * Font(FontFamily.HELVETICA, 10))); // c1.setBackgroundColor(colorRowTable);
		 * c1.setHorizontalAlignment(Element.ALIGN_RIGHT); c1.setPadding(4);
		 * table.addCell(c1);
		 */

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Average Unit Amount");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_ha_aua() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_ha_aua())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) with needs");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_need() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_need())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements with needs");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_need() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_need())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) contributors");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_cont_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_cont_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements contributors");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_cont_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_cont_23())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total needs");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_pe_need() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_pe_need())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year needs");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_year_pe_need() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_year_pe_need())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Total potential contribution");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_cont_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_cont_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);
		/*
		 * p = new Paragraph(); p.setFont(new Font(FontFamily.HELVETICA, 10,
		 * Font.BOLD)); p.add("total potential contribution 2024"); c1= new PdfPCell(p);
		 * c1.setBackgroundColor(colorRowTable); c1.setPadding(4); table.addCell(c1);
		 * 
		 * c1 = new PdfPCell(new Phrase(obj.getMd_amt_sum_cont_24()==null ? "":
		 * String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_cont_24())),new
		 * Font(FontFamily.HELVETICA, 10))); c1.setBackgroundColor(colorRowTable);
		 * c1.setHorizontalAlignment(Element.ALIGN_RIGHT); c1.setPadding(4);
		 * table.addCell(c1);
		 * 
		 * p = new Paragraph(); p.setFont(new Font(FontFamily.HELVETICA, 10,
		 * Font.BOLD)); p.add("total potential contribution 2025"); c1= new PdfPCell(p);
		 * // c1.setBackgroundColor(colorRowTable); c1.setPadding(4); table.addCell(c1);
		 * 
		 * c1 = new PdfPCell(new Phrase(obj.getMd_amt_sum_cont_25()==null ? "":
		 * String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_cont_25())),new
		 * Font(FontFamily.HELVETICA, 10))); // c1.setBackgroundColor(colorRowTable);
		 * c1.setHorizontalAlignment(Element.ALIGN_RIGHT); c1.setPadding(4);
		 * table.addCell(c1);
		 * 
		 * p = new Paragraph(); p.setFont(new Font(FontFamily.HELVETICA, 10,
		 * Font.BOLD)); p.add("Total potential contribution 2026"); c1= new PdfPCell(p);
		 * c1.setBackgroundColor(colorRowTable); c1.setPadding(4); table.addCell(c1);
		 * 
		 * c1 = new PdfPCell(new Phrase(obj.getMd_amt_sum_cont_26()==null ? "":
		 * String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_cont_26())),new
		 * Font(FontFamily.HELVETICA, 10))); c1.setBackgroundColor(colorRowTable);
		 * c1.setHorizontalAlignment(Element.ALIGN_RIGHT); c1.setPadding(4);
		 * table.addCell(c1);
		 */
		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Maximum level total contribution 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_dta_max_level_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_dta_max_level_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Maximum level total contribution 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_dta_max_level_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_dta_max_level_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Maximum level total contribution 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_dta_max_level_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_dta_max_level_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Maximum level total contribution 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_dta_max_level_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_dta_max_level_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum level 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_max_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_max_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum level 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_max_24() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_max_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum level 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_max_25() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_max_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum level 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_max_26() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_max_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum level 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_max_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_max_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum level 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_max_24() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_max_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum level 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_max_25() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_max_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum level 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_max_26() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_max_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum decrease 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_sl_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_sl_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum decrease 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_sl_24() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_sl_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum decrease 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_sl_25() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_sl_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. id farm (cuaa) affected by maximum decrease 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_cuaa_sl_26() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_cuaa_sl_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum decrease 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_sl_23() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_sl_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum decrease 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_sl_24() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_sl_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum decrease 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_sl_25() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_sl_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("N. entitlements affected by maximum decrease 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_num_entitlement_sl_26() == null ? ""
						: String.format("%,d", Integer.parseInt(obj.getMd_num_entitlement_sl_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year needs due to stop loss 2023");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_pe_sl_need_23() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_pe_sl_need_23())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year needs due to stop loss 2024");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_pe_sl_need_24() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_pe_sl_need_24())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year needs due to stop loss 2025");
		c1 = new PdfPCell(p);
//	        c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_pe_sl_need_25() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_pe_sl_need_25())),
				new Font(FontFamily.HELVETICA, 10)));
//	        c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.BOLD));
		p.add("Year needs due to stop loss 2026");
		c1 = new PdfPCell(p);
		c1.setBackgroundColor(colorRowTable);
		c1.setPadding(4);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase(
				obj.getMd_amt_sum_pe_sl_need_26() == null ? ""
						: String.format("%,.4f", Double.parseDouble(obj.getMd_amt_sum_pe_sl_need_26())),
				new Font(FontFamily.HELVETICA, 10)));
		c1.setBackgroundColor(colorRowTable);
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		c1.setPadding(4);
		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/************* LEGEND *****************/

		table = new PdfPTable(new float[] { 6, 6 });
		table.setWidthPercentage(100);

		tHeader = new PdfPCell(
				new Phrase("INPUT PARAMETERS", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("MS Ceiling Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("National ceiling for direct payments for claim year N as laid down in Annex IV");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% BISS Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Percentage of national ceiling allocated to Basic income support for sustainability (BISS) for claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Years of Convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Years of length of the convergence process (1=2023; 2=2024; 3=2025; 4=2026)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Max value of PE (�/Ha)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum level for the value of individual payment entitlements for the Member State or for each group of territories (art. 20, par. 3)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Area for activation of PE");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Area for activation of payment entitlements. Generally equal to 1ha or less than 1ha.");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Year max value");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Years of application of the maximum level for the value of individual payment entitlements (1=2023; 2=2024; 3=2025; 4=2026)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% Greening");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Share of Payment for agricultural practices beneficial for the climate and the environment respect to the Basic payment scheme (art. 43.9 par. 3 Reg. (EU) n. 1307(2013))");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Amount Greening (�/Ha)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Annual payment for Payment for agricultural practices beneficial for the climate and the Environment (art. 43.9 par. 2 Reg. (EU) n. 1307(2013))");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("% AUA");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Percentage of the planned average unit amount. Each Member State shall ensure that, for claim year 2026 at the latest, all payment entitlements have a value of at least 85% of the planned average unit amount. However, MS may fix higher percentage (art. 20 par. 5)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum decreas");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum reduction of individual payment entitlement that may not be lower than 30% (art. 20 par. 7)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Max Cycles SL");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum number of cycles of the routine 'maximum decreases' ");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("INPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of farms eligible");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of hectares for activation of payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of payment entitlements uploaded (Sum)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		tHeader = new PdfPCell(
				new Phrase("OUTPUT DATA", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		tHeader = new PdfPCell(
				new Phrase("DESCRIPTION", new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE)));
		tHeader.setHorizontalAlignment(Element.ALIGN_LEFT);
		tHeader.setBackgroundColor(colorHeaderTableLegend);
		tHeader.setPadding(4);
		table.addCell(tHeader);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Record");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of records uploaded");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Id Farm (cuaa)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of farms eligible");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. Hectares");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of hectares for activation of payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total Amount Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total value of payment entitlements after convergence for claim year N (Sum)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Average Unit Amount");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Planned average unit amount for the basic income support for claim year 2026 as laid down in its CAP Strategic Plan for the Member State or for the group of territories. It is quantified by the tool.");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. id farm (cuaa) with needs");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of benefiters from the process of convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. entitlements with needs");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements benefitting from the process of convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. id farm (cuaa) contributors");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of contributors of the process of convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. entitlements contributors");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements contributors of the process of convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total needs");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total financial need to achieve the target of convergence for the period");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Year needs");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Financial need by single year due to the process of convergence");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Total potential contribution");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum potential contribution available by contributors");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Maximum level total contribution Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Amounts available through the application of the maximum level for the value of individual payment entitlements in the claim year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. id farm (cuaa) affected by maximum level Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of farms affected by the application of the maximum level for the value of individual payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. entitlements affected by maximum level Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements affected by the application of the maximum level for the value of individual payment entitlements");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. id farm (cuaa) affected by maximum decrease Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of farms affected by the application of the maximum reduction of individual payment entitlement (art. 20 par. 7)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("N. entitlements affected by maximum decrease Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Number of payment entitlements affected by the application of the maximum reduction of individual payment entitlement (art. 20 par. 7)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Year needs due to stop loss Year N");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		p = new Paragraph();
		p.setFont(new Font(FontFamily.HELVETICA, 10, Font.ITALIC));
		p.add("Amount of financial resources needed to guarantee the application of the maximum reduction of individual payment entitlement (art. 20 par. 7)");
		c1 = new PdfPCell(p);
		c1.setPadding(4);
		table.addCell(c1);

		table.addCell(c1);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public class HeaderTable extends PdfPageEventHelper {

		protected PdfPTable tableHeader;
		protected PdfPTable tableFooter;

		protected float tableHeight;
		protected float tableFooterHeight;

		public HeaderTable() {
			tableHeader = new PdfPTable(new float[] { 2, 7, 3 });
			tableHeader.getDefaultCell().setFixedHeight(50f);
			tableHeader.setTotalWidth(523);
			tableHeader.setLockedWidth(true);

			try {
				PdfPCell cell = createImageCell("" + Home.class.getResource("/images/logo_niva.png"), 50f);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell.setBorder(0);
				cell.setPaddingBottom(5f);
				cell.setBorderWidthBottom(0.5f);
				cell.setBorderColorBottom(colorBorderHeader);
				tableHeader.addCell(cell);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				PdfPCell cell = new PdfPCell(new Paragraph(Messages.getString("niva.eu.wide")));
				cell.setBorder(0);
				cell.setPaddingBottom(5f);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setBorderWidthBottom(0.5f);
				cell.setBorderColorBottom(colorBorderHeader);
				tableHeader.addCell(cell);
			} catch (Exception e) {
				tableHeader.addCell(" ");

			}
			try {
				PdfPCell cell = createImageCell("" + Home.class.getResource("/images/logo_cliente.png"), 50f);
				cell.setBorder(0);
				cell.setPaddingBottom(5f);
				cell.setHorizontalAlignment(Align.RIGHT);
				cell.setBorderWidthBottom(0.5f);
				cell.setBorderColorBottom(colorBorderHeader);
				tableHeader.addCell(cell);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tableHeight = tableHeader.getTotalHeight();

			tableFooter = new PdfPTable(new float[] { 2, 7, 3 });
			tableFooter.getDefaultCell().setFixedHeight(20f);
			tableFooter.setTotalWidth(523);
			tableFooter.setLockedWidth(true);

			try {
				PdfPCell cell = createImageCell("" + Home.class.getResource("/images/unione_europea.png"), 20f);
				cell.setBorder(0);
				cell.setPaddingTop(5f);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell.setBorderWidthTop(0.5f);
				cell.setBorderColorTop(colorBorderHeader);
				tableFooter.addCell(cell);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {

				Font f = new Font(FontFamily.TIMES_ROMAN, 8f);
				Paragraph p = new Paragraph(Messages.getString("niva.footer.ue.no.html"), f);
				PdfPCell cell = new PdfPCell(p);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				cell.setBorder(0);
				cell.setPaddingTop(5f);
				cell.setBorderWidthTop(0.5f);
				cell.setBorderColorTop(colorBorderHeader);
				tableFooter.addCell(cell);
			} catch (Exception e) {
				tableFooter.addCell(" ");

			}
			try {
				PdfPCell cell = createImageCell("" + Home.class.getResource("/images/fornitore.png"), 20f);
				cell.setBorder(0);
				cell.setPaddingTop(5f);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setBorderWidthTop(0.5f);
				cell.setBorderColorTop(colorBorderHeader);
				tableFooter.addCell(cell);
			} catch (Exception e) {
				e.printStackTrace();
			}
			tableFooterHeight = tableFooter.getTotalHeight();

		}

		public PdfPCell createImageCell(String path, float height) throws DocumentException, IOException {
			Image img = Image.getInstance(path);
			PdfPCell cell = new PdfPCell(img, true);
			cell.setFixedHeight(height);
			return cell;
		}

		public float getTableHeight() {
			return tableHeight;
		}

		public float getTableFooterHeight() {
			return tableFooterHeight;
		}

		public void onEndPage(PdfWriter writer, Document document) {
			tableHeader.writeSelectedRows(0, -1, document.left(),
					document.top() + ((document.topMargin() + tableHeight) / 2), writer.getDirectContent());

			tableFooter.writeSelectedRows(0, -1, document.left(), document.bottom(), writer.getDirectContent());
		}
	}

}
