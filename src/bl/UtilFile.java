package bl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class UtilFile {

	public UtilFile() {
	}

	public static void copiaFile(String pathOrigin, String pathDestination) throws FileNotFoundException, IOException {
		File dbOrig = new File(pathOrigin);
		File dbCopy = new File(pathDestination);
		InputStream in = new FileInputStream(dbOrig);
		OutputStream out = new FileOutputStream(dbCopy);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	public static void copiaFile(InputStream in, String pathDestination) throws FileNotFoundException, IOException {
		File dbCopy = new File(pathDestination);
		OutputStream out = new FileOutputStream(dbCopy);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

}
