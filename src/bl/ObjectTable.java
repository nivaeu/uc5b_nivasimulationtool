package bl;

public class ObjectTable {
	private String sColumn1;
	private String sColumn2;
	private String sColumn3;
	private String sColumn4;
	private String sColumn5;
	private String sColumn6;
	private String sColumn7;
	private String sColumn8;
	private String sColumn9;
	private String sColumn10;

	private Integer iColumn1;
	private Integer iColumn2;
	private Integer iColumn3;
	private Integer iColumn4;
	private Integer iColumn5;
	private Integer iColumn6;
	private Integer iColumn7;
	private Integer iColumn8;
	private Integer iColumn9;
	private Integer iColumn10;

	private Double dColumn1;
	private Double dColumn2;
	private Double dColumn3;
	private Double dColumn4;
	private Double dColumn5;
	private Double dColumn6;
	private Double dColumn7;
	private Double dColumn8;
	private Double dColumn9;
	private Double dColumn10;

	public ObjectTable() {
	}

	public String getsColumn1() {
		return sColumn1;
	}

	public void setsColumn1(String sColumn1) {
		this.sColumn1 = sColumn1;
	}

	public String getsColumn2() {
		return sColumn2;
	}

	public void setsColumn2(String sColumn2) {
		this.sColumn2 = sColumn2;
	}

	public String getsColumn3() {
		return sColumn3;
	}

	public void setsColumn3(String sColumn3) {
		this.sColumn3 = sColumn3;
	}

	public String getsColumn4() {
		return sColumn4;
	}

	public void setsColumn4(String sColumn4) {
		this.sColumn4 = sColumn4;
	}

	public String getsColumn5() {
		return sColumn5;
	}

	public void setsColumn5(String sColumn5) {
		this.sColumn5 = sColumn5;
	}

	public String getsColumn6() {
		return sColumn6;
	}

	public void setsColumn6(String sColumn6) {
		this.sColumn6 = sColumn6;
	}

	public String getsColumn7() {
		return sColumn7;
	}

	public void setsColumn7(String sColumn7) {
		this.sColumn7 = sColumn7;
	}

	public String getsColumn8() {
		return sColumn8;
	}

	public void setsColumn8(String sColumn8) {
		this.sColumn8 = sColumn8;
	}

	public String getsColumn9() {
		return sColumn9;
	}

	public void setsColumn9(String sColumn9) {
		this.sColumn9 = sColumn9;
	}

	public String getsColumn10() {
		return sColumn10;
	}

	public void setsColumn10(String sColumn10) {
		this.sColumn10 = sColumn10;
	}

	public Integer getiColumn1() {
		return iColumn1;
	}

	public void setiColumn1(Integer iColumn1) {
		this.iColumn1 = iColumn1;
	}

	public Integer getiColumn2() {
		return iColumn2;
	}

	public void setiColumn2(Integer iColumn2) {
		this.iColumn2 = iColumn2;
	}

	public Integer getiColumn3() {
		return iColumn3;
	}

	public void setiColumn3(Integer iColumn3) {
		this.iColumn3 = iColumn3;
	}

	public Integer getiColumn4() {
		return iColumn4;
	}

	public void setiColumn4(Integer iColumn4) {
		this.iColumn4 = iColumn4;
	}

	public Integer getiColumn5() {
		return iColumn5;
	}

	public void setiColumn5(Integer iColumn5) {
		this.iColumn5 = iColumn5;
	}

	public Integer getiColumn6() {
		return iColumn6;
	}

	public void setiColumn6(Integer iColumn6) {
		this.iColumn6 = iColumn6;
	}

	public Integer getiColumn7() {
		return iColumn7;
	}

	public void setiColumn7(Integer iColumn7) {
		this.iColumn7 = iColumn7;
	}

	public Integer getiColumn8() {
		return iColumn8;
	}

	public void setiColumn8(Integer iColumn8) {
		this.iColumn8 = iColumn8;
	}

	public Integer getiColumn9() {
		return iColumn9;
	}

	public void setiColumn9(Integer iColumn9) {
		this.iColumn9 = iColumn9;
	}

	public Integer getiColumn10() {
		return iColumn10;
	}

	public void setiColumn10(Integer iColumn10) {
		this.iColumn10 = iColumn10;
	}

	public Double getdColumn1() {
		return dColumn1;
	}

	public void setdColumn1(Double dColumn1) {
		this.dColumn1 = dColumn1;
	}

	public Double getdColumn2() {
		return dColumn2;
	}

	public void setdColumn2(Double dColumn2) {
		this.dColumn2 = dColumn2;
	}

	public Double getdColumn3() {
		return dColumn3;
	}

	public void setdColumn3(Double dColumn3) {
		this.dColumn3 = dColumn3;
	}

	public Double getdColumn4() {
		return dColumn4;
	}

	public void setdColumn4(Double dColumn4) {
		this.dColumn4 = dColumn4;
	}

	public Double getdColumn5() {
		return dColumn5;
	}

	public void setdColumn5(Double dColumn5) {
		this.dColumn5 = dColumn5;
	}

	public Double getdColumn6() {
		return dColumn6;
	}

	public void setdColumn6(Double dColumn6) {
		this.dColumn6 = dColumn6;
	}

	public Double getdColumn7() {
		return dColumn7;
	}

	public void setdColumn7(Double dColumn7) {
		this.dColumn7 = dColumn7;
	}

	public Double getdColumn8() {
		return dColumn8;
	}

	public void setdColumn8(Double dColumn8) {
		this.dColumn8 = dColumn8;
	}

	public Double getdColumn9() {
		return dColumn9;
	}

	public void setdColumn9(Double dColumn9) {
		this.dColumn9 = dColumn9;
	}

	public Double getdColumn10() {
		return dColumn10;
	}

	public void setdColumn10(Double dColumn10) {
		this.dColumn10 = dColumn10;
	}

}
