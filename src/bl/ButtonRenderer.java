package bl;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import gui.Home;

public class ButtonRenderer extends JButton implements TableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5646840301098687045L;

	// CONSTRUCTOR
	public ButtonRenderer(String image) {
		// SET BUTTON PROPERTIES

		setOpaque(true);
		setSize(30, 30);
		setToolTipText(Messages.getString("button.print.duoble.click"));
		// setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		// this.setBorderPainted(false);
		// this.setBorder(null);
		// button.setFocusable(false);
		// this.setMargin(new Insets(0, 0, 0, 0));
		// this.setContentAreaFilled(false);
		if (image != null) {
			ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/" + image));
			Image imageHome = iconHome.getImage();
			imageHome = imageHome.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
			iconHome = new ImageIcon(imageHome);
			this.setIcon(iconHome);
		}

	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object obj, boolean selected, boolean focused, int row,
			int col) {

		// SET PASSED OBJECT AS BUTTON TEXT

		setText((obj == null) ? "" : obj.toString());
		if (Integer.parseInt("" + table.getModel().getValueAt(row, 10)) != 0
				&& (("" + table.getModel().getValueAt(row, 9)).equalsIgnoreCase("0")
						|| ("" + table.getModel().getValueAt(row, 9)).equalsIgnoreCase("1"))) {
			setEnabled(true);
		} else {
			setEnabled(false);
		}

		return this;
	}

}
