package bl;

public class PrintModel3Values {

	private String id_model;
	private String cod_model;
	private String nam_model;
	private String des_model;
	private String id_model_execution;
	private String dat_model_execution;
	private String nam_model_execution;
	private String des_model_execution;
	private String dat_start;
	private String dat_end;
	private String flg_model_execution_status;
	private String id_status_job;
	private String id_job;
	private String nam_job;
	private String job_satus_type;
	private String job_status;
	private String fd_num_record;
	private String fd_num_cuaa;
	private String fd_num_entitlement;
	private String fd_num_sup_22;
	private String fd_amt_bps_22;
	private String md_num_record;
	private String md_num_cuaa;
	private String md_num_entitlement;
	private String md_num_sup_22;
	private String md_amt_bps_22;
	private String md_amt_max_23;
	private String md_amt_max_24;
	private String md_amt_max_25;
	private String md_amt_max_26;
	private String md_pct_biss_23;
	private String md_pct_biss_24;
	private String md_pct_biss_25;
	private String md_pct_biss_26;
	private String md_pct_gre;
	private String md_amt_ha_gre;
	private String md_pct_aua;
	private String md_amt_max_level;
	private String md_num_year_max_level;
	private String md_num_years_conv;
	private String md_pct_sl;
	private String md_num_cy_sl;
	private String md_num_max_sup_level;
	private String md_amt_pe_23;
	private String md_amt_pe_24;
	private String md_amt_pe_25;
	private String md_amt_pe_26;
	private String md_amt_ha_aua;
	private String md_avg_unit_amt_01;
	private String md_avg_unit_amt_02;
	private String md_num_cuaa_need;
	private String md_num_entitlement_need;
	private String md_num_cuaa_cont_23;
	private String md_num_entitlement_cont_23;
	private String md_amt_sum_pe_need;
	private String md_amt_sum_year_pe_need;
	private String md_amt_sum_cont_23;
	private String md_amt_sum_cont_24;
	private String md_amt_sum_cont_25;
	private String md_amt_sum_cont_26;
	private String md_amt_sum_dta_max_level_23;
	private String md_amt_sum_dta_max_level_24;
	private String md_amt_sum_dta_max_level_25;
	private String md_amt_sum_dta_max_level_26;
	private String md_num_cuaa_max_23;
	private String md_num_cuaa_max_24;
	private String md_num_cuaa_max_25;
	private String md_num_cuaa_max_26;
	private String md_num_entitlement_max_23;
	private String md_num_entitlement_max_24;
	private String md_num_entitlement_max_25;
	private String md_num_entitlement_max_26;

//	private String md_num_cuaa_max_23;
//	private String md_num_cuaa_max_24;
//	private String md_num_cuaa_max_25;
//	private String md_num_cuaa_max_26;
//	private String md_num_entitlement_max_23;
//	private String md_num_entitlement_max_24;
//	private String md_num_entitlement_max_25;
//	private String md_num_entitlement_max_26;
	private String md_num_cuaa_sl_23;
	private String md_num_cuaa_sl_24;
	private String md_num_cuaa_sl_25;
	private String md_num_cuaa_sl_26;
	private String md_num_entitlement_sl_23;
	private String md_num_entitlement_sl_24;
	private String md_num_entitlement_sl_25;
	private String md_num_entitlement_sl_26;
	private String md_amt_sum_pe_sl_need_23;
	private String md_amt_sum_pe_sl_need_24;
	private String md_amt_sum_pe_sl_need_25;
	private String md_amt_sum_pe_sl_need_26;

	public PrintModel3Values() {
		super();
	}

	public String getId_model() {
		return id_model;
	}

	public void setId_model(String id_model) {
		this.id_model = id_model;
	}

	public String getCod_model() {
		return cod_model;
	}

	public void setCod_model(String cod_model) {
		this.cod_model = cod_model;
	}

	public String getNam_model() {
		return nam_model;
	}

	public void setNam_model(String nam_model) {
		this.nam_model = nam_model;
	}

	public String getDes_model() {
		return des_model;
	}

	public void setDes_model(String des_model) {
		this.des_model = des_model;
	}

	public String getId_model_execution() {
		return id_model_execution;
	}

	public void setId_model_execution(String id_model_execution) {
		this.id_model_execution = id_model_execution;
	}

	public String getDat_model_execution() {
		return dat_model_execution;
	}

	public void setDat_model_execution(String dat_model_execution) {
		this.dat_model_execution = dat_model_execution;
	}

	public String getNam_model_execution() {
		return nam_model_execution;
	}

	public void setNam_model_execution(String nam_model_execution) {
		this.nam_model_execution = nam_model_execution;
	}

	public String getDes_model_execution() {
		return des_model_execution;
	}

	public void setDes_model_execution(String des_model_execution) {
		this.des_model_execution = des_model_execution;
	}

	public String getDat_start() {
		return dat_start;
	}

	public void setDat_start(String dat_start) {
		this.dat_start = dat_start;
	}

	public String getDat_end() {
		return dat_end;
	}

	public void setDat_end(String dat_end) {
		this.dat_end = dat_end;
	}

	public String getFlg_model_execution_status() {
		return flg_model_execution_status;
	}

	public void setFlg_model_execution_status(String flg_model_execution_status) {
		this.flg_model_execution_status = flg_model_execution_status;
	}

	public String getId_status_job() {
		return id_status_job;
	}

	public void setId_status_job(String id_status_job) {
		this.id_status_job = id_status_job;
	}

	public String getId_job() {
		return id_job;
	}

	public void setId_job(String id_job) {
		this.id_job = id_job;
	}

	public String getNam_job() {
		return nam_job;
	}

	public void setNam_job(String nam_job) {
		this.nam_job = nam_job;
	}

	public String getJob_satus_type() {
		return job_satus_type;
	}

	public void setJob_satus_type(String job_satus_type) {
		this.job_satus_type = job_satus_type;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getFd_num_record() {
		return fd_num_record;
	}

	public void setFd_num_record(String fd_num_record) {
		this.fd_num_record = fd_num_record;
	}

	public String getFd_num_cuaa() {
		return fd_num_cuaa;
	}

	public void setFd_num_cuaa(String fd_num_cuaa) {
		this.fd_num_cuaa = fd_num_cuaa;
	}

	public String getFd_num_entitlement() {
		return fd_num_entitlement;
	}

	public void setFd_num_entitlement(String fd_num_entitlement) {
		this.fd_num_entitlement = fd_num_entitlement;
	}

	public String getFd_num_sup_22() {
		return fd_num_sup_22;
	}

	public void setFd_num_sup_22(String fd_num_sup_22) {
		this.fd_num_sup_22 = fd_num_sup_22;
	}

	public String getFd_amt_bps_22() {
		return fd_amt_bps_22;
	}

	public void setFd_amt_bps_22(String fd_amt_bps_22) {
		this.fd_amt_bps_22 = fd_amt_bps_22;
	}

	public String getMd_num_record() {
		return md_num_record;
	}

	public void setMd_num_record(String md_num_record) {
		this.md_num_record = md_num_record;
	}

	public String getMd_num_cuaa() {
		return md_num_cuaa;
	}

	public void setMd_num_cuaa(String md_num_cuaa) {
		this.md_num_cuaa = md_num_cuaa;
	}

	public String getMd_num_entitlement() {
		return md_num_entitlement;
	}

	public void setMd_num_entitlement(String md_num_entitlement) {
		this.md_num_entitlement = md_num_entitlement;
	}

	public String getMd_num_sup_22() {
		return md_num_sup_22;
	}

	public void setMd_num_sup_22(String md_num_sup_22) {
		this.md_num_sup_22 = md_num_sup_22;
	}

	public String getMd_amt_bps_22() {
		return md_amt_bps_22;
	}

	public void setMd_amt_bps_22(String md_amt_bps_22) {
		this.md_amt_bps_22 = md_amt_bps_22;
	}

	public String getMd_amt_max_23() {
		return md_amt_max_23;
	}

	public void setMd_amt_max_23(String md_amt_max_23) {
		this.md_amt_max_23 = md_amt_max_23;
	}

	public String getMd_amt_max_24() {
		return md_amt_max_24;
	}

	public void setMd_amt_max_24(String md_amt_max_24) {
		this.md_amt_max_24 = md_amt_max_24;
	}

	public String getMd_amt_max_25() {
		return md_amt_max_25;
	}

	public void setMd_amt_max_25(String md_amt_max_25) {
		this.md_amt_max_25 = md_amt_max_25;
	}

	public String getMd_amt_max_26() {
		return md_amt_max_26;
	}

	public void setMd_amt_max_26(String md_amt_max_26) {
		this.md_amt_max_26 = md_amt_max_26;
	}

	public String getMd_pct_biss_23() {
		return md_pct_biss_23;
	}

	public void setMd_pct_biss_23(String md_pct_biss_23) {
		this.md_pct_biss_23 = md_pct_biss_23;
	}

	public String getMd_pct_biss_24() {
		return md_pct_biss_24;
	}

	public void setMd_pct_biss_24(String md_pct_biss_24) {
		this.md_pct_biss_24 = md_pct_biss_24;
	}

	public String getMd_pct_biss_25() {
		return md_pct_biss_25;
	}

	public void setMd_pct_biss_25(String md_pct_biss_25) {
		this.md_pct_biss_25 = md_pct_biss_25;
	}

	public String getMd_pct_biss_26() {
		return md_pct_biss_26;
	}

	public void setMd_pct_biss_26(String md_pct_biss_26) {
		this.md_pct_biss_26 = md_pct_biss_26;
	}

	public String getMd_pct_gre() {
		return md_pct_gre;
	}

	public void setMd_pct_gre(String md_pct_gre) {
		this.md_pct_gre = md_pct_gre;
	}

	public String getMd_amt_ha_gre() {
		return md_amt_ha_gre;
	}

	public void setMd_amt_ha_gre(String md_amt_ha_gre) {
		this.md_amt_ha_gre = md_amt_ha_gre;
	}

	public String getMd_pct_aua() {
		return md_pct_aua;
	}

	public void setMd_pct_aua(String md_pct_aua) {
		this.md_pct_aua = md_pct_aua;
	}

	public String getMd_amt_max_level() {
		return md_amt_max_level;
	}

	public void setMd_amt_max_level(String md_amt_max_level) {
		this.md_amt_max_level = md_amt_max_level;
	}

	public String getMd_num_year_max_level() {
		return md_num_year_max_level;
	}

	public void setMd_num_year_max_level(String md_num_year_max_level) {
		this.md_num_year_max_level = md_num_year_max_level;
	}

	public String getMd_num_years_conv() {
		return md_num_years_conv;
	}

	public void setMd_num_years_conv(String md_num_years_conv) {
		this.md_num_years_conv = md_num_years_conv;
	}

	public String getMd_pct_sl() {
		return md_pct_sl;
	}

	public void setMd_pct_sl(String md_pct_sl) {
		this.md_pct_sl = md_pct_sl;
	}

	public String getMd_num_cy_sl() {
		return md_num_cy_sl;
	}

	public void setMd_num_cy_sl(String md_num_cy_sl) {
		this.md_num_cy_sl = md_num_cy_sl;
	}

	public String getMd_num_max_sup_level() {
		return md_num_max_sup_level;
	}

	public void setMd_num_max_sup_level(String md_num_max_sup_level) {
		this.md_num_max_sup_level = md_num_max_sup_level;
	}

	public String getMd_amt_pe_23() {
		return md_amt_pe_23;
	}

	public void setMd_amt_pe_23(String md_amt_pe_23) {
		this.md_amt_pe_23 = md_amt_pe_23;
	}

	public String getMd_amt_pe_24() {
		return md_amt_pe_24;
	}

	public void setMd_amt_pe_24(String md_amt_pe_24) {
		this.md_amt_pe_24 = md_amt_pe_24;
	}

	public String getMd_amt_pe_25() {
		return md_amt_pe_25;
	}

	public void setMd_amt_pe_25(String md_amt_pe_25) {
		this.md_amt_pe_25 = md_amt_pe_25;
	}

	public String getMd_amt_pe_26() {
		return md_amt_pe_26;
	}

	public void setMd_amt_pe_26(String md_amt_pe_26) {
		this.md_amt_pe_26 = md_amt_pe_26;
	}

	public String getMd_amt_ha_aua() {
		return md_amt_ha_aua;
	}

	public void setMd_amt_ha_aua(String md_amt_ha_aua) {
		this.md_amt_ha_aua = md_amt_ha_aua;
	}

	public String getMd_avg_unit_amt_01() {
		return md_avg_unit_amt_01;
	}

	public void setMd_avg_unit_amt_01(String md_avg_unit_amt_01) {
		this.md_avg_unit_amt_01 = md_avg_unit_amt_01;
	}

	public String getMd_avg_unit_amt_02() {
		return md_avg_unit_amt_02;
	}

	public void setMd_avg_unit_amt_02(String md_avg_unit_amt_02) {
		this.md_avg_unit_amt_02 = md_avg_unit_amt_02;
	}

	public String getMd_num_cuaa_need() {
		return md_num_cuaa_need;
	}

	public void setMd_num_cuaa_need(String md_num_cuaa_need) {
		this.md_num_cuaa_need = md_num_cuaa_need;
	}

	public String getMd_num_entitlement_need() {
		return md_num_entitlement_need;
	}

	public void setMd_num_entitlement_need(String md_num_entitlement_need) {
		this.md_num_entitlement_need = md_num_entitlement_need;
	}

	public String getMd_num_cuaa_cont_23() {
		return md_num_cuaa_cont_23;
	}

	public void setMd_num_cuaa_cont_23(String md_num_cuaa_cont_23) {
		this.md_num_cuaa_cont_23 = md_num_cuaa_cont_23;
	}

	public String getMd_num_entitlement_cont_23() {
		return md_num_entitlement_cont_23;
	}

	public void setMd_num_entitlement_cont_23(String md_num_entitlement_cont_23) {
		this.md_num_entitlement_cont_23 = md_num_entitlement_cont_23;
	}

	public String getMd_amt_sum_pe_need() {
		return md_amt_sum_pe_need;
	}

	public void setMd_amt_sum_pe_need(String md_amt_sum_pe_need) {
		this.md_amt_sum_pe_need = md_amt_sum_pe_need;
	}

	public String getMd_amt_sum_year_pe_need() {
		return md_amt_sum_year_pe_need;
	}

	public void setMd_amt_sum_year_pe_need(String md_amt_sum_year_pe_need) {
		this.md_amt_sum_year_pe_need = md_amt_sum_year_pe_need;
	}

	public String getMd_amt_sum_cont_23() {
		return md_amt_sum_cont_23;
	}

	public void setMd_amt_sum_cont_23(String md_amt_sum_cont_23) {
		this.md_amt_sum_cont_23 = md_amt_sum_cont_23;
	}

	public String getMd_amt_sum_cont_24() {
		return md_amt_sum_cont_24;
	}

	public void setMd_amt_sum_cont_24(String md_amt_sum_cont_24) {
		this.md_amt_sum_cont_24 = md_amt_sum_cont_24;
	}

	public String getMd_amt_sum_cont_25() {
		return md_amt_sum_cont_25;
	}

	public void setMd_amt_sum_cont_25(String md_amt_sum_cont_25) {
		this.md_amt_sum_cont_25 = md_amt_sum_cont_25;
	}

	public String getMd_amt_sum_cont_26() {
		return md_amt_sum_cont_26;
	}

	public void setMd_amt_sum_cont_26(String md_amt_sum_cont_26) {
		this.md_amt_sum_cont_26 = md_amt_sum_cont_26;
	}

	public String getMd_amt_sum_dta_max_level_23() {
		return md_amt_sum_dta_max_level_23;
	}

	public void setMd_amt_sum_dta_max_level_23(String md_amt_sum_dta_max_level_23) {
		this.md_amt_sum_dta_max_level_23 = md_amt_sum_dta_max_level_23;
	}

	public String getMd_amt_sum_dta_max_level_24() {
		return md_amt_sum_dta_max_level_24;
	}

	public void setMd_amt_sum_dta_max_level_24(String md_amt_sum_dta_max_level_24) {
		this.md_amt_sum_dta_max_level_24 = md_amt_sum_dta_max_level_24;
	}

	public String getMd_amt_sum_dta_max_level_25() {
		return md_amt_sum_dta_max_level_25;
	}

	public void setMd_amt_sum_dta_max_level_25(String md_amt_sum_dta_max_level_25) {
		this.md_amt_sum_dta_max_level_25 = md_amt_sum_dta_max_level_25;
	}

	public String getMd_amt_sum_dta_max_level_26() {
		return md_amt_sum_dta_max_level_26;
	}

	public void setMd_amt_sum_dta_max_level_26(String md_amt_sum_dta_max_level_26) {
		this.md_amt_sum_dta_max_level_26 = md_amt_sum_dta_max_level_26;
	}

//	public String getMd_num_cuaa_max_23() {
//		return md_num_cuaa_max_23;
//	}
//
//	public void setMd_num_cuaa_max_23(String md_num_cuaa_max_23) {
//		this.md_num_cuaa_max_23 = md_num_cuaa_max_23;
//	}
//
//	public String getMd_num_cuaa_max_24() {
//		return md_num_cuaa_max_24;
//	}
//
//	public void setMd_num_cuaa_max_24(String md_num_cuaa_max_24) {
//		this.md_num_cuaa_max_24 = md_num_cuaa_max_24;
//	}
//
//	public String getMd_num_cuaa_max_25() {
//		return md_num_cuaa_max_25;
//	}
//
//	public void setMd_num_cuaa_max_25(String md_num_cuaa_max_25) {
//		this.md_num_cuaa_max_25 = md_num_cuaa_max_25;
//	}
//
//	public String getMd_num_cuaa_max_26() {
//		return md_num_cuaa_max_26;
//	}
//
//	public void setMd_num_cuaa_max_26(String md_num_cuaa_max_26) {
//		this.md_num_cuaa_max_26 = md_num_cuaa_max_26;
//	}
//
//	public String getMd_num_entitlement_max_23() {
//		return md_num_entitlement_max_23;
//	}
//
//	public void setMd_num_entitlement_max_23(String md_num_entitlement_max_23) {
//		this.md_num_entitlement_max_23 = md_num_entitlement_max_23;
//	}
//
//	public String getMd_num_entitlement_max_24() {
//		return md_num_entitlement_max_24;
//	}
//
//	public void setMd_num_entitlement_max_24(String md_num_entitlement_max_24) {
//		this.md_num_entitlement_max_24 = md_num_entitlement_max_24;
//	}
//
//	public String getMd_num_entitlement_max_25() {
//		return md_num_entitlement_max_25;
//	}
//
//	public void setMd_num_entitlement_max_25(String md_num_entitlement_max_25) {
//		this.md_num_entitlement_max_25 = md_num_entitlement_max_25;
//	}
//
//	public String getMd_num_entitlement_max_26() {
//		return md_num_entitlement_max_26;
//	}
//
//	public void setMd_num_entitlement_max_26(String md_num_entitlement_max_26) {
//		this.md_num_entitlement_max_26 = md_num_entitlement_max_26;
//	}

	public String getMd_num_cuaa_sl_23() {
		return md_num_cuaa_sl_23;
	}

	public void setMd_num_cuaa_sl_23(String md_num_cuaa_sl_23) {
		this.md_num_cuaa_sl_23 = md_num_cuaa_sl_23;
	}

	public String getMd_num_cuaa_sl_24() {
		return md_num_cuaa_sl_24;
	}

	public void setMd_num_cuaa_sl_24(String md_num_cuaa_sl_24) {
		this.md_num_cuaa_sl_24 = md_num_cuaa_sl_24;
	}

	public String getMd_num_cuaa_sl_25() {
		return md_num_cuaa_sl_25;
	}

	public void setMd_num_cuaa_sl_25(String md_num_cuaa_sl_25) {
		this.md_num_cuaa_sl_25 = md_num_cuaa_sl_25;
	}

	public String getMd_num_cuaa_sl_26() {
		return md_num_cuaa_sl_26;
	}

	public void setMd_num_cuaa_sl_26(String md_num_cuaa_sl_26) {
		this.md_num_cuaa_sl_26 = md_num_cuaa_sl_26;
	}

	public String getMd_num_entitlement_sl_23() {
		return md_num_entitlement_sl_23;
	}

	public void setMd_num_entitlement_sl_23(String md_num_entitlement_sl_23) {
		this.md_num_entitlement_sl_23 = md_num_entitlement_sl_23;
	}

	public String getMd_num_entitlement_sl_24() {
		return md_num_entitlement_sl_24;
	}

	public void setMd_num_entitlement_sl_24(String md_num_entitlement_sl_24) {
		this.md_num_entitlement_sl_24 = md_num_entitlement_sl_24;
	}

	public String getMd_num_entitlement_sl_25() {
		return md_num_entitlement_sl_25;
	}

	public void setMd_num_entitlement_sl_25(String md_num_entitlement_sl_25) {
		this.md_num_entitlement_sl_25 = md_num_entitlement_sl_25;
	}

	public String getMd_num_entitlement_sl_26() {
		return md_num_entitlement_sl_26;
	}

	public void setMd_num_entitlement_sl_26(String md_num_entitlement_sl_26) {
		this.md_num_entitlement_sl_26 = md_num_entitlement_sl_26;
	}

	public String getMd_amt_sum_pe_sl_need_23() {
		return md_amt_sum_pe_sl_need_23;
	}

	public void setMd_amt_sum_pe_sl_need_23(String md_amt_sum_pe_sl_need_23) {
		this.md_amt_sum_pe_sl_need_23 = md_amt_sum_pe_sl_need_23;
	}

	public String getMd_amt_sum_pe_sl_need_24() {
		return md_amt_sum_pe_sl_need_24;
	}

	public void setMd_amt_sum_pe_sl_need_24(String md_amt_sum_pe_sl_need_24) {
		this.md_amt_sum_pe_sl_need_24 = md_amt_sum_pe_sl_need_24;
	}

	public String getMd_amt_sum_pe_sl_need_25() {
		return md_amt_sum_pe_sl_need_25;
	}

	public void setMd_amt_sum_pe_sl_need_25(String md_amt_sum_pe_sl_need_25) {
		this.md_amt_sum_pe_sl_need_25 = md_amt_sum_pe_sl_need_25;
	}

	public String getMd_amt_sum_pe_sl_need_26() {
		return md_amt_sum_pe_sl_need_26;
	}

	public void setMd_amt_sum_pe_sl_need_26(String md_amt_sum_pe_sl_need_26) {
		this.md_amt_sum_pe_sl_need_26 = md_amt_sum_pe_sl_need_26;
	}

	public String getMd_num_cuaa_max_23() {
		return md_num_cuaa_max_23;
	}

	public void setMd_num_cuaa_max_23(String md_num_cuaa_max_23) {
		this.md_num_cuaa_max_23 = md_num_cuaa_max_23;
	}

	public String getMd_num_cuaa_max_24() {
		return md_num_cuaa_max_24;
	}

	public void setMd_num_cuaa_max_24(String md_num_cuaa_max_24) {
		this.md_num_cuaa_max_24 = md_num_cuaa_max_24;
	}

	public String getMd_num_cuaa_max_25() {
		return md_num_cuaa_max_25;
	}

	public void setMd_num_cuaa_max_25(String md_num_cuaa_max_25) {
		this.md_num_cuaa_max_25 = md_num_cuaa_max_25;
	}

	public String getMd_num_cuaa_max_26() {
		return md_num_cuaa_max_26;
	}

	public void setMd_num_cuaa_max_26(String md_num_cuaa_max_26) {
		this.md_num_cuaa_max_26 = md_num_cuaa_max_26;
	}

	public String getMd_num_entitlement_max_23() {
		return md_num_entitlement_max_23;
	}

	public void setMd_num_entitlement_max_23(String md_num_entitlement_max_23) {
		this.md_num_entitlement_max_23 = md_num_entitlement_max_23;
	}

	public String getMd_num_entitlement_max_24() {
		return md_num_entitlement_max_24;
	}

	public void setMd_num_entitlement_max_24(String md_num_entitlement_max_24) {
		this.md_num_entitlement_max_24 = md_num_entitlement_max_24;
	}

	public String getMd_num_entitlement_max_25() {
		return md_num_entitlement_max_25;
	}

	public void setMd_num_entitlement_max_25(String md_num_entitlement_max_25) {
		this.md_num_entitlement_max_25 = md_num_entitlement_max_25;
	}

	public String getMd_num_entitlement_max_26() {
		return md_num_entitlement_max_26;
	}

	public void setMd_num_entitlement_max_26(String md_num_entitlement_max_26) {
		this.md_num_entitlement_max_26 = md_num_entitlement_max_26;
	}

}
