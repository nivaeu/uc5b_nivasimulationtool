package bl;

public class PrintModel2Values {

	private String id_model;
	private String cod_model;
	private String nam_model;
	private String des_model;
	private String id_model_execution;
	private String dat_model_execution;
	private String nam_model_execution;
	private String des_model_execution;
	private String dat_start;
	private String dat_end;
	private String flg_model_execution_status;
	private String id_status_job;
	private String id_job;
	private String nam_job;
	private String job_satus_type;
	private String job_status;
	private String fd_num_record;
	private String fd_num_cuaa;
	private String fd_num_sup_0;
	private String md_num_record;
	private String md_num_cuaa;
	private String md_num_sup_0;
	private String md_num_min_sup_level;
	private String md_only_pure;
	private String md_amt_ha_23;
	private String md_amt_ha_24;
	private String md_amt_ha_25;
	private String md_amt_ha_26;
	private String amt_max_23;
	private String amt_max_24;
	private String amt_max_25;
	private String amt_max_26;
	private String pct_criss_23;
	private String pct_criss_24;
	private String pct_criss_25;
	private String pct_criss_26;
	private String md_pct_biss_23_x1;
	private String md_pct_biss_23_x2;
	private String md_pct_biss_23_x3;
	private String md_pct_biss_24_x1;
	private String md_pct_biss_24_x2;
	private String md_pct_biss_24_x3;
	private String md_pct_biss_25_x1;
	private String md_pct_biss_25_x2;
	private String md_pct_biss_25_x3;
	private String md_pct_biss_26_x1;
	private String md_pct_biss_26_x2;
	private String md_pct_biss_26_x3;
	private String md_num_sup_rng_23_x1;
	private String md_num_sup_rng_23_x2;
	private String md_num_sup_rng_23_x3;
	private String md_num_sup_rng_24_x1;
	private String md_num_sup_rng_24_x2;
	private String md_num_sup_rng_24_x3;
	private String md_num_sup_rng_25_x1;
	private String md_num_sup_rng_25_x2;
	private String md_num_sup_rng_25_x3;
	private String md_num_sup_rng_26_x1;
	private String md_num_sup_rng_26_x2;
	private String md_num_sup_rng_26_x3;
	private String md_num_cuaa_23;
	private String md_num_cuaa_24;
	private String md_num_cuaa_25;
	private String md_num_cuaa_26;
	private String md_num_sup_23_x;
	private String md_num_sup_24_x;
	private String md_num_sup_25_x;
	private String md_num_sup_26_x;
	private String md_amt_23;
	private String md_amt_24;
	private String md_amt_25;
	private String md_amt_26;
	private String md_num_sup_avg;
	private String md_num_sup_23_avg;
	private String md_num_sup_24_avg;
	private String md_num_sup_25_avg;
	private String md_num_sup_26_avg;
	private String md_num_cuaa_23_x1;
	private String md_num_sup_23_x1;
	private String md_amt_23_x1;
	private String md_num_sup_23_x1_avg;
	private String md_num_cuaa_23_x2;
	private String md_num_sup_23_x2;
	private String md_amt_23_x2;
	private String md_num_sup_23_x2_avg;
	private String md_num_cuaa_23_x3;
	private String md_num_sup_23_x3;
	private String md_amt_23_x3;
	private String md_num_sup_23_x3_avg;
	private String md_num_cuaa_24_x1;
	private String md_num_sup_24_x1;
	private String md_amt_24_x1;
	private String md_num_sup_24_x1_avg;
	private String md_num_cuaa_24_x2;
	private String md_num_sup_24_x2;
	private String md_amt_24_x2;
	private String md_num_sup_24_x2_avg;
	private String md_num_cuaa_24_x3;
	private String md_num_sup_24_x3;
	private String md_amt_24_x3;
	private String md_num_sup_24_x3_avg;
	private String md_num_cuaa_25_x1;
	private String md_num_sup_25_x1;
	private String md_amt_25_x1;
	private String md_num_sup_25_x1_avg;
	private String md_num_cuaa_25_x2;
	private String md_num_sup_25_x2;
	private String md_amt_25_x2;
	private String md_num_sup_25_x2_avg;
	private String md_num_cuaa_25_x3;
	private String md_num_sup_25_x3;
	private String md_amt_25_x3;
	private String md_num_sup_25_x3_avg;
	private String md_num_cuaa_26_x1;
	private String md_num_sup_26_x1;
	private String md_amt_26_x1;
	private String md_num_sup_26_x1_avg;
	private String md_num_cuaa_26_x2;
	private String md_num_sup_26_x2;
	private String md_amt_26_x2;
	private String md_num_sup_26_x2_avg;
	private String md_num_cuaa_26_x3;
	private String md_num_sup_26_x3;
	private String md_amt_26_x3;
	private String md_num_sup_26_x3_avg;

	public PrintModel2Values() {
		super();
	}

	public String getId_model() {
		return id_model;
	}

	public void setId_model(String id_model) {
		this.id_model = id_model;
	}

	public String getCod_model() {
		return cod_model;
	}

	public void setCod_model(String cod_model) {
		this.cod_model = cod_model;
	}

	public String getNam_model() {
		return nam_model;
	}

	public void setNam_model(String nam_model) {
		this.nam_model = nam_model;
	}

	public String getDes_model() {
		return des_model;
	}

	public void setDes_model(String des_model) {
		this.des_model = des_model;
	}

	public String getId_model_execution() {
		return id_model_execution;
	}

	public void setId_model_execution(String id_model_execution) {
		this.id_model_execution = id_model_execution;
	}

	public String getDat_model_execution() {
		return dat_model_execution;
	}

	public void setDat_model_execution(String dat_model_execution) {
		this.dat_model_execution = dat_model_execution;
	}

	public String getNam_model_execution() {
		return nam_model_execution;
	}

	public void setNam_model_execution(String nam_model_execution) {
		this.nam_model_execution = nam_model_execution;
	}

	public String getDes_model_execution() {
		return des_model_execution;
	}

	public void setDes_model_execution(String des_model_execution) {
		this.des_model_execution = des_model_execution;
	}

	public String getDat_start() {
		return dat_start;
	}

	public void setDat_start(String dat_start) {
		this.dat_start = dat_start;
	}

	public String getDat_end() {
		return dat_end;
	}

	public void setDat_end(String dat_end) {
		this.dat_end = dat_end;
	}

	public String getFlg_model_execution_status() {
		return flg_model_execution_status;
	}

	public void setFlg_model_execution_status(String flg_model_execution_status) {
		this.flg_model_execution_status = flg_model_execution_status;
	}

	public String getId_status_job() {
		return id_status_job;
	}

	public void setId_status_job(String id_status_job) {
		this.id_status_job = id_status_job;
	}

	public String getId_job() {
		return id_job;
	}

	public void setId_job(String id_job) {
		this.id_job = id_job;
	}

	public String getNam_job() {
		return nam_job;
	}

	public void setNam_job(String nam_job) {
		this.nam_job = nam_job;
	}

	public String getJob_satus_type() {
		return job_satus_type;
	}

	public void setJob_satus_type(String job_satus_type) {
		this.job_satus_type = job_satus_type;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getFd_num_record() {
		return fd_num_record;
	}

	public void setFd_num_record(String fd_num_record) {
		this.fd_num_record = fd_num_record;
	}

	public String getFd_num_cuaa() {
		return fd_num_cuaa;
	}

	public void setFd_num_cuaa(String fd_num_cuaa) {
		this.fd_num_cuaa = fd_num_cuaa;
	}

	public String getFd_num_sup_0() {
		return fd_num_sup_0;
	}

	public void setFd_num_sup_0(String fd_num_sup_0) {
		this.fd_num_sup_0 = fd_num_sup_0;
	}

	public String getMd_num_record() {
		return md_num_record;
	}

	public void setMd_num_record(String md_num_record) {
		this.md_num_record = md_num_record;
	}

	public String getMd_num_cuaa() {
		return md_num_cuaa;
	}

	public void setMd_num_cuaa(String md_num_cuaa) {
		this.md_num_cuaa = md_num_cuaa;
	}

	public String getMd_num_sup_0() {
		return md_num_sup_0;
	}

	public void setMd_num_sup_0(String md_num_sup_0) {
		this.md_num_sup_0 = md_num_sup_0;
	}

	public String getMd_num_min_sup_level() {
		return md_num_min_sup_level;
	}

	public void setMd_num_min_sup_level(String md_num_min_sup_level) {
		this.md_num_min_sup_level = md_num_min_sup_level;
	}

	public String getMd_only_pure() {
		return md_only_pure;
	}

	public void setMd_only_pure(String md_only_pure) {
		this.md_only_pure = md_only_pure;
	}

	public String getMd_amt_ha_23() {
		return md_amt_ha_23;
	}

	public void setMd_amt_ha_23(String md_amt_ha_23) {
		this.md_amt_ha_23 = md_amt_ha_23;
	}

	public String getMd_amt_ha_24() {
		return md_amt_ha_24;
	}

	public void setMd_amt_ha_24(String md_amt_ha_24) {
		this.md_amt_ha_24 = md_amt_ha_24;
	}

	public String getMd_amt_ha_25() {
		return md_amt_ha_25;
	}

	public void setMd_amt_ha_25(String md_amt_ha_25) {
		this.md_amt_ha_25 = md_amt_ha_25;
	}

	public String getMd_amt_ha_26() {
		return md_amt_ha_26;
	}

	public void setMd_amt_ha_26(String md_amt_ha_26) {
		this.md_amt_ha_26 = md_amt_ha_26;
	}

	public String getMd_pct_biss_23_x1() {
		return md_pct_biss_23_x1;
	}

	public void setMd_pct_biss_23_x1(String md_pct_biss_23_x1) {
		this.md_pct_biss_23_x1 = md_pct_biss_23_x1;
	}

	public String getMd_pct_biss_23_x2() {
		return md_pct_biss_23_x2;
	}

	public void setMd_pct_biss_23_x2(String md_pct_biss_23_x2) {
		this.md_pct_biss_23_x2 = md_pct_biss_23_x2;
	}

	public String getMd_pct_biss_23_x3() {
		return md_pct_biss_23_x3;
	}

	public void setMd_pct_biss_23_x3(String md_pct_biss_23_x3) {
		this.md_pct_biss_23_x3 = md_pct_biss_23_x3;
	}

	public String getMd_pct_biss_24_x1() {
		return md_pct_biss_24_x1;
	}

	public void setMd_pct_biss_24_x1(String md_pct_biss_24_x1) {
		this.md_pct_biss_24_x1 = md_pct_biss_24_x1;
	}

	public String getMd_pct_biss_24_x2() {
		return md_pct_biss_24_x2;
	}

	public void setMd_pct_biss_24_x2(String md_pct_biss_24_x2) {
		this.md_pct_biss_24_x2 = md_pct_biss_24_x2;
	}

	public String getMd_pct_biss_24_x3() {
		return md_pct_biss_24_x3;
	}

	public void setMd_pct_biss_24_x3(String md_pct_biss_24_x3) {
		this.md_pct_biss_24_x3 = md_pct_biss_24_x3;
	}

	public String getMd_pct_biss_25_x1() {
		return md_pct_biss_25_x1;
	}

	public void setMd_pct_biss_25_x1(String md_pct_biss_25_x1) {
		this.md_pct_biss_25_x1 = md_pct_biss_25_x1;
	}

	public String getMd_pct_biss_25_x2() {
		return md_pct_biss_25_x2;
	}

	public void setMd_pct_biss_25_x2(String md_pct_biss_25_x2) {
		this.md_pct_biss_25_x2 = md_pct_biss_25_x2;
	}

	public String getMd_pct_biss_25_x3() {
		return md_pct_biss_25_x3;
	}

	public void setMd_pct_biss_25_x3(String md_pct_biss_25_x3) {
		this.md_pct_biss_25_x3 = md_pct_biss_25_x3;
	}

	public String getMd_pct_biss_26_x1() {
		return md_pct_biss_26_x1;
	}

	public void setMd_pct_biss_26_x1(String md_pct_biss_26_x1) {
		this.md_pct_biss_26_x1 = md_pct_biss_26_x1;
	}

	public String getMd_pct_biss_26_x2() {
		return md_pct_biss_26_x2;
	}

	public void setMd_pct_biss_26_x2(String md_pct_biss_26_x2) {
		this.md_pct_biss_26_x2 = md_pct_biss_26_x2;
	}

	public String getMd_pct_biss_26_x3() {
		return md_pct_biss_26_x3;
	}

	public void setMd_pct_biss_26_x3(String md_pct_biss_26_x3) {
		this.md_pct_biss_26_x3 = md_pct_biss_26_x3;
	}

	public String getMd_num_sup_rng_23_x1() {
		return md_num_sup_rng_23_x1;
	}

	public void setMd_num_sup_rng_23_x1(String md_num_sup_rng_23_x1) {
		this.md_num_sup_rng_23_x1 = md_num_sup_rng_23_x1;
	}

	public String getMd_num_sup_rng_23_x2() {
		return md_num_sup_rng_23_x2;
	}

	public void setMd_num_sup_rng_23_x2(String md_num_sup_rng_23_x2) {
		this.md_num_sup_rng_23_x2 = md_num_sup_rng_23_x2;
	}

	public String getMd_num_sup_rng_23_x3() {
		return md_num_sup_rng_23_x3;
	}

	public void setMd_num_sup_rng_23_x3(String md_num_sup_rng_23_x3) {
		this.md_num_sup_rng_23_x3 = md_num_sup_rng_23_x3;
	}

	public String getMd_num_sup_rng_24_x1() {
		return md_num_sup_rng_24_x1;
	}

	public void setMd_num_sup_rng_24_x1(String md_num_sup_rng_24_x1) {
		this.md_num_sup_rng_24_x1 = md_num_sup_rng_24_x1;
	}

	public String getMd_num_sup_rng_24_x2() {
		return md_num_sup_rng_24_x2;
	}

	public void setMd_num_sup_rng_24_x2(String md_num_sup_rng_24_x2) {
		this.md_num_sup_rng_24_x2 = md_num_sup_rng_24_x2;
	}

	public String getMd_num_sup_rng_24_x3() {
		return md_num_sup_rng_24_x3;
	}

	public void setMd_num_sup_rng_24_x3(String md_num_sup_rng_24_x3) {
		this.md_num_sup_rng_24_x3 = md_num_sup_rng_24_x3;
	}

	public String getMd_num_sup_rng_25_x1() {
		return md_num_sup_rng_25_x1;
	}

	public void setMd_num_sup_rng_25_x1(String md_num_sup_rng_25_x1) {
		this.md_num_sup_rng_25_x1 = md_num_sup_rng_25_x1;
	}

	public String getMd_num_sup_rng_25_x2() {
		return md_num_sup_rng_25_x2;
	}

	public void setMd_num_sup_rng_25_x2(String md_num_sup_rng_25_x2) {
		this.md_num_sup_rng_25_x2 = md_num_sup_rng_25_x2;
	}

	public String getMd_num_sup_rng_25_x3() {
		return md_num_sup_rng_25_x3;
	}

	public void setMd_num_sup_rng_25_x3(String md_num_sup_rng_25_x3) {
		this.md_num_sup_rng_25_x3 = md_num_sup_rng_25_x3;
	}

	public String getMd_num_sup_rng_26_x1() {
		return md_num_sup_rng_26_x1;
	}

	public void setMd_num_sup_rng_26_x1(String md_num_sup_rng_26_x1) {
		this.md_num_sup_rng_26_x1 = md_num_sup_rng_26_x1;
	}

	public String getMd_num_sup_rng_26_x2() {
		return md_num_sup_rng_26_x2;
	}

	public void setMd_num_sup_rng_26_x2(String md_num_sup_rng_26_x2) {
		this.md_num_sup_rng_26_x2 = md_num_sup_rng_26_x2;
	}

	public String getMd_num_sup_rng_26_x3() {
		return md_num_sup_rng_26_x3;
	}

	public void setMd_num_sup_rng_26_x3(String md_num_sup_rng_26_x3) {
		this.md_num_sup_rng_26_x3 = md_num_sup_rng_26_x3;
	}

	public String getMd_num_cuaa_23() {
		return md_num_cuaa_23;
	}

	public void setMd_num_cuaa_23(String md_num_cuaa_23) {
		this.md_num_cuaa_23 = md_num_cuaa_23;
	}

	public String getMd_num_cuaa_24() {
		return md_num_cuaa_24;
	}

	public void setMd_num_cuaa_24(String md_num_cuaa_24) {
		this.md_num_cuaa_24 = md_num_cuaa_24;
	}

	public String getMd_num_cuaa_25() {
		return md_num_cuaa_25;
	}

	public void setMd_num_cuaa_25(String md_num_cuaa_25) {
		this.md_num_cuaa_25 = md_num_cuaa_25;
	}

	public String getMd_num_cuaa_26() {
		return md_num_cuaa_26;
	}

	public void setMd_num_cuaa_26(String md_num_cuaa_26) {
		this.md_num_cuaa_26 = md_num_cuaa_26;
	}

	

	public String getMd_amt_23() {
		return md_amt_23;
	}

	public void setMd_amt_23(String md_amt_23) {
		this.md_amt_23 = md_amt_23;
	}

	public String getMd_amt_24() {
		return md_amt_24;
	}

	public void setMd_amt_24(String md_amt_24) {
		this.md_amt_24 = md_amt_24;
	}

	public String getMd_amt_25() {
		return md_amt_25;
	}

	public void setMd_amt_25(String md_amt_25) {
		this.md_amt_25 = md_amt_25;
	}

	public String getMd_amt_26() {
		return md_amt_26;
	}

	public void setMd_amt_26(String md_amt_26) {
		this.md_amt_26 = md_amt_26;
	}

	public String getMd_num_sup_avg() {
		return md_num_sup_avg;
	}

	public void setMd_num_sup_avg(String md_num_sup_avg) {
		this.md_num_sup_avg = md_num_sup_avg;
	}

	public String getMd_num_sup_23_avg() {
		return md_num_sup_23_avg;
	}

	public void setMd_num_sup_23_avg(String md_num_sup_23_avg) {
		this.md_num_sup_23_avg = md_num_sup_23_avg;
	}

	public String getMd_num_sup_24_avg() {
		return md_num_sup_24_avg;
	}

	public void setMd_num_sup_24_avg(String md_num_sup_24_avg) {
		this.md_num_sup_24_avg = md_num_sup_24_avg;
	}

	public String getMd_num_sup_25_avg() {
		return md_num_sup_25_avg;
	}

	public void setMd_num_sup_25_avg(String md_num_sup_25_avg) {
		this.md_num_sup_25_avg = md_num_sup_25_avg;
	}

	public String getMd_num_sup_26_avg() {
		return md_num_sup_26_avg;
	}

	public void setMd_num_sup_26_avg(String md_num_sup_26_avg) {
		this.md_num_sup_26_avg = md_num_sup_26_avg;
	}

	public String getMd_num_cuaa_23_x1() {
		return md_num_cuaa_23_x1;
	}

	public void setMd_num_cuaa_23_x1(String md_num_cuaa_23_x1) {
		this.md_num_cuaa_23_x1 = md_num_cuaa_23_x1;
	}

	public String getMd_num_sup_23_x1() {
		return md_num_sup_23_x1;
	}

	public void setMd_num_sup_23_x1(String md_num_sup_23_x1) {
		this.md_num_sup_23_x1 = md_num_sup_23_x1;
	}

	public String getMd_amt_23_x1() {
		return md_amt_23_x1;
	}

	public void setMd_amt_23_x1(String md_amt_23_x1) {
		this.md_amt_23_x1 = md_amt_23_x1;
	}

	public String getMd_num_sup_23_x1_avg() {
		return md_num_sup_23_x1_avg;
	}

	public void setMd_num_sup_23_x1_avg(String md_num_sup_23_x1_avg) {
		this.md_num_sup_23_x1_avg = md_num_sup_23_x1_avg;
	}

	public String getMd_num_cuaa_23_x2() {
		return md_num_cuaa_23_x2;
	}

	public void setMd_num_cuaa_23_x2(String md_num_cuaa_23_x2) {
		this.md_num_cuaa_23_x2 = md_num_cuaa_23_x2;
	}

	public String getMd_num_sup_23_x2() {
		return md_num_sup_23_x2;
	}

	public void setMd_num_sup_23_x2(String md_num_sup_23_x2) {
		this.md_num_sup_23_x2 = md_num_sup_23_x2;
	}

	public String getMd_amt_23_x2() {
		return md_amt_23_x2;
	}

	public void setMd_amt_23_x2(String md_amt_23_x2) {
		this.md_amt_23_x2 = md_amt_23_x2;
	}

	public String getMd_num_sup_23_x2_avg() {
		return md_num_sup_23_x2_avg;
	}

	public void setMd_num_sup_23_x2_avg(String md_num_sup_23_x2_avg) {
		this.md_num_sup_23_x2_avg = md_num_sup_23_x2_avg;
	}

	public String getMd_num_cuaa_23_x3() {
		return md_num_cuaa_23_x3;
	}

	public void setMd_num_cuaa_23_x3(String md_num_cuaa_23_x3) {
		this.md_num_cuaa_23_x3 = md_num_cuaa_23_x3;
	}

	public String getMd_num_sup_23_x3() {
		return md_num_sup_23_x3;
	}

	public void setMd_num_sup_23_x3(String md_num_sup_23_x3) {
		this.md_num_sup_23_x3 = md_num_sup_23_x3;
	}

	public String getMd_amt_23_x3() {
		return md_amt_23_x3;
	}

	public void setMd_amt_23_x3(String md_amt_23_x3) {
		this.md_amt_23_x3 = md_amt_23_x3;
	}

	public String getMd_num_sup_23_x3_avg() {
		return md_num_sup_23_x3_avg;
	}

	public void setMd_num_sup_23_x3_avg(String md_num_sup_23_x3_avg) {
		this.md_num_sup_23_x3_avg = md_num_sup_23_x3_avg;
	}

	public String getMd_num_cuaa_24_x1() {
		return md_num_cuaa_24_x1;
	}

	public void setMd_num_cuaa_24_x1(String md_num_cuaa_24_x1) {
		this.md_num_cuaa_24_x1 = md_num_cuaa_24_x1;
	}

	public String getMd_num_sup_24_x1() {
		return md_num_sup_24_x1;
	}

	public void setMd_num_sup_24_x1(String md_num_sup_24_x1) {
		this.md_num_sup_24_x1 = md_num_sup_24_x1;
	}

	public String getMd_amt_24_x1() {
		return md_amt_24_x1;
	}

	public void setMd_amt_24_x1(String md_amt_24_x1) {
		this.md_amt_24_x1 = md_amt_24_x1;
	}

	public String getMd_num_sup_24_x1_avg() {
		return md_num_sup_24_x1_avg;
	}

	public void setMd_num_sup_24_x1_avg(String md_num_sup_24_x1_avg) {
		this.md_num_sup_24_x1_avg = md_num_sup_24_x1_avg;
	}

	public String getMd_num_cuaa_24_x2() {
		return md_num_cuaa_24_x2;
	}

	public void setMd_num_cuaa_24_x2(String md_num_cuaa_24_x2) {
		this.md_num_cuaa_24_x2 = md_num_cuaa_24_x2;
	}

	public String getMd_num_sup_24_x2() {
		return md_num_sup_24_x2;
	}

	public void setMd_num_sup_24_x2(String md_num_sup_24_x2) {
		this.md_num_sup_24_x2 = md_num_sup_24_x2;
	}

	public String getMd_amt_24_x2() {
		return md_amt_24_x2;
	}

	public void setMd_amt_24_x2(String md_amt_24_x2) {
		this.md_amt_24_x2 = md_amt_24_x2;
	}

	public String getMd_num_sup_24_x2_avg() {
		return md_num_sup_24_x2_avg;
	}

	public void setMd_num_sup_24_x2_avg(String md_num_sup_24_x2_avg) {
		this.md_num_sup_24_x2_avg = md_num_sup_24_x2_avg;
	}

	public String getMd_num_cuaa_24_x3() {
		return md_num_cuaa_24_x3;
	}

	public void setMd_num_cuaa_24_x3(String md_num_cuaa_24_x3) {
		this.md_num_cuaa_24_x3 = md_num_cuaa_24_x3;
	}

	public String getMd_num_sup_24_x3() {
		return md_num_sup_24_x3;
	}

	public void setMd_num_sup_24_x3(String md_num_sup_24_x3) {
		this.md_num_sup_24_x3 = md_num_sup_24_x3;
	}

	public String getMd_amt_24_x3() {
		return md_amt_24_x3;
	}

	public void setMd_amt_24_x3(String md_amt_24_x3) {
		this.md_amt_24_x3 = md_amt_24_x3;
	}

	public String getMd_num_sup_24_x3_avg() {
		return md_num_sup_24_x3_avg;
	}

	public void setMd_num_sup_24_x3_avg(String md_num_sup_24_x3_avg) {
		this.md_num_sup_24_x3_avg = md_num_sup_24_x3_avg;
	}

	public String getMd_num_cuaa_25_x1() {
		return md_num_cuaa_25_x1;
	}

	public void setMd_num_cuaa_25_x1(String md_num_cuaa_25_x1) {
		this.md_num_cuaa_25_x1 = md_num_cuaa_25_x1;
	}

	public String getMd_num_sup_25_x1() {
		return md_num_sup_25_x1;
	}

	public void setMd_num_sup_25_x1(String md_num_sup_25_x1) {
		this.md_num_sup_25_x1 = md_num_sup_25_x1;
	}

	public String getMd_amt_25_x1() {
		return md_amt_25_x1;
	}

	public void setMd_amt_25_x1(String md_amt_25_x1) {
		this.md_amt_25_x1 = md_amt_25_x1;
	}

	public String getMd_num_sup_25_x1_avg() {
		return md_num_sup_25_x1_avg;
	}

	public void setMd_num_sup_25_x1_avg(String md_num_sup_25_x1_avg) {
		this.md_num_sup_25_x1_avg = md_num_sup_25_x1_avg;
	}

	public String getMd_num_cuaa_25_x2() {
		return md_num_cuaa_25_x2;
	}

	public void setMd_num_cuaa_25_x2(String md_num_cuaa_25_x2) {
		this.md_num_cuaa_25_x2 = md_num_cuaa_25_x2;
	}

	public String getMd_num_sup_25_x2() {
		return md_num_sup_25_x2;
	}

	public void setMd_num_sup_25_x2(String md_num_sup_25_x2) {
		this.md_num_sup_25_x2 = md_num_sup_25_x2;
	}

	public String getMd_amt_25_x2() {
		return md_amt_25_x2;
	}

	public void setMd_amt_25_x2(String md_amt_25_x2) {
		this.md_amt_25_x2 = md_amt_25_x2;
	}

	public String getMd_num_sup_25_x2_avg() {
		return md_num_sup_25_x2_avg;
	}

	public void setMd_num_sup_25_x2_avg(String md_num_sup_25_x2_avg) {
		this.md_num_sup_25_x2_avg = md_num_sup_25_x2_avg;
	}

	public String getMd_num_cuaa_25_x3() {
		return md_num_cuaa_25_x3;
	}

	public void setMd_num_cuaa_25_x3(String md_num_cuaa_25_x3) {
		this.md_num_cuaa_25_x3 = md_num_cuaa_25_x3;
	}

	public String getMd_num_sup_25_x3() {
		return md_num_sup_25_x3;
	}

	public void setMd_num_sup_25_x3(String md_num_sup_25_x3) {
		this.md_num_sup_25_x3 = md_num_sup_25_x3;
	}

	public String getMd_amt_25_x3() {
		return md_amt_25_x3;
	}

	public void setMd_amt_25_x3(String md_amt_25_x3) {
		this.md_amt_25_x3 = md_amt_25_x3;
	}

	public String getMd_num_sup_25_x3_avg() {
		return md_num_sup_25_x3_avg;
	}

	public void setMd_num_sup_25_x3_avg(String md_num_sup_25_x3_avg) {
		this.md_num_sup_25_x3_avg = md_num_sup_25_x3_avg;
	}

	public String getMd_num_cuaa_26_x1() {
		return md_num_cuaa_26_x1;
	}

	public void setMd_num_cuaa_26_x1(String md_num_cuaa_26_x1) {
		this.md_num_cuaa_26_x1 = md_num_cuaa_26_x1;
	}

	public String getMd_num_sup_26_x1() {
		return md_num_sup_26_x1;
	}

	public void setMd_num_sup_26_x1(String md_num_sup_26_x1) {
		this.md_num_sup_26_x1 = md_num_sup_26_x1;
	}

	public String getMd_amt_26_x1() {
		return md_amt_26_x1;
	}

	public void setMd_amt_26_x1(String md_amt_26_x1) {
		this.md_amt_26_x1 = md_amt_26_x1;
	}

	public String getMd_num_sup_26_x1_avg() {
		return md_num_sup_26_x1_avg;
	}

	public void setMd_num_sup_26_x1_avg(String md_num_sup_26_x1_avg) {
		this.md_num_sup_26_x1_avg = md_num_sup_26_x1_avg;
	}

	public String getMd_num_cuaa_26_x2() {
		return md_num_cuaa_26_x2;
	}

	public void setMd_num_cuaa_26_x2(String md_num_cuaa_26_x2) {
		this.md_num_cuaa_26_x2 = md_num_cuaa_26_x2;
	}

	public String getMd_num_sup_26_x2() {
		return md_num_sup_26_x2;
	}

	public void setMd_num_sup_26_x2(String md_num_sup_26_x2) {
		this.md_num_sup_26_x2 = md_num_sup_26_x2;
	}

	public String getMd_amt_26_x2() {
		return md_amt_26_x2;
	}

	public void setMd_amt_26_x2(String md_amt_26_x2) {
		this.md_amt_26_x2 = md_amt_26_x2;
	}

	public String getMd_num_sup_26_x2_avg() {
		return md_num_sup_26_x2_avg;
	}

	public void setMd_num_sup_26_x2_avg(String md_num_sup_26_x2_avg) {
		this.md_num_sup_26_x2_avg = md_num_sup_26_x2_avg;
	}

	public String getMd_num_cuaa_26_x3() {
		return md_num_cuaa_26_x3;
	}

	public void setMd_num_cuaa_26_x3(String md_num_cuaa_26_x3) {
		this.md_num_cuaa_26_x3 = md_num_cuaa_26_x3;
	}

	public String getMd_num_sup_26_x3() {
		return md_num_sup_26_x3;
	}

	public void setMd_num_sup_26_x3(String md_num_sup_26_x3) {
		this.md_num_sup_26_x3 = md_num_sup_26_x3;
	}

	public String getMd_amt_26_x3() {
		return md_amt_26_x3;
	}

	public void setMd_amt_26_x3(String md_amt_26_x3) {
		this.md_amt_26_x3 = md_amt_26_x3;
	}

	public String getMd_num_sup_26_x3_avg() {
		return md_num_sup_26_x3_avg;
	}

	public void setMd_num_sup_26_x3_avg(String md_num_sup_26_x3_avg) {
		this.md_num_sup_26_x3_avg = md_num_sup_26_x3_avg;
	}

	public String getAmt_max_23() {
		return amt_max_23;
	}

	public void setAmt_max_23(String amt_max_23) {
		this.amt_max_23 = amt_max_23;
	}

	public String getAmt_max_24() {
		return amt_max_24;
	}

	public void setAmt_max_24(String amt_max_24) {
		this.amt_max_24 = amt_max_24;
	}

	public String getAmt_max_25() {
		return amt_max_25;
	}

	public void setAmt_max_25(String amt_max_25) {
		this.amt_max_25 = amt_max_25;
	}

	public String getAmt_max_26() {
		return amt_max_26;
	}

	public void setAmt_max_26(String amt_max_26) {
		this.amt_max_26 = amt_max_26;
	}

	public String getPct_criss_23() {
		return pct_criss_23;
	}

	public void setPct_criss_23(String pct_criss_23) {
		this.pct_criss_23 = pct_criss_23;
	}

	public String getPct_criss_24() {
		return pct_criss_24;
	}

	public void setPct_criss_24(String pct_criss_24) {
		this.pct_criss_24 = pct_criss_24;
	}

	public String getPct_criss_25() {
		return pct_criss_25;
	}

	public void setPct_criss_25(String pct_criss_25) {
		this.pct_criss_25 = pct_criss_25;
	}

	public String getPct_criss_26() {
		return pct_criss_26;
	}

	public void setPct_criss_26(String pct_criss_26) {
		this.pct_criss_26 = pct_criss_26;
	}

	public String getMd_num_sup_23_x() {
		return md_num_sup_23_x;
	}

	public void setMd_num_sup_23_x(String md_num_sup_23_x) {
		this.md_num_sup_23_x = md_num_sup_23_x;
	}

	public String getMd_num_sup_24_x() {
		return md_num_sup_24_x;
	}

	public void setMd_num_sup_24_x(String md_num_sup_24_x) {
		this.md_num_sup_24_x = md_num_sup_24_x;
	}

	public String getMd_num_sup_25_x() {
		return md_num_sup_25_x;
	}

	public void setMd_num_sup_25_x(String md_num_sup_25_x) {
		this.md_num_sup_25_x = md_num_sup_25_x;
	}

	public String getMd_num_sup_26_x() {
		return md_num_sup_26_x;
	}

	public void setMd_num_sup_26_x(String md_num_sup_26_x) {
		this.md_num_sup_26_x = md_num_sup_26_x;
	}

}
