package bl;

public class PrintModel1Values {

	private String id_model;
	private String cod_model;
	private String nam_model;
	private String des_model;
	private String id_model_execution;
	private String dat_model_execution;
	private String nam_model_execution;
	private String des_model_execution;
	private String dat_start;
	private String dat_end;
	private String flg_model_execution_status;
	private String id_status_job;
	private String id_job;
	private String nam_job;
	private String job_satus_type;
	private String job_status;
	private String fd_num_record;
	private String fd_num_cuaa;
	private String fd_num_sup_0;
	private String md_num_record;
	private String md_num_cuaa;
	private String md_num_sup_0;
	private String md_amt_max_23;
	private String md_amt_max_24;
	private String md_amt_max_25;
	private String md_amt_max_26;
	private String md_pct_biss_23;
	private String md_pct_biss_24;
	private String md_pct_biss_25;
	private String md_pct_biss_26;
	private String md_num_min_sup_level;
	private String md_amt_23;
	private String md_amt_24;
	private String md_amt_25;
	private String md_amt_26;
	private String md_amt_ha_23;
	private String md_amt_ha_24;
	private String md_amt_ha_25;
	private String md_amt_ha_26;
	private String md_num_sup_avg;
	private String md_num_cuaa_avg;
	private String md_num_sup_0_avg;
	private String md_amt_23_avg;
	private String md_amt_24_avg;
	private String md_amt_25_avg;
	private String md_amt_26_avg;

	public PrintModel1Values() {
		super();
	}

	public String getId_model() {
		return id_model;
	}

	public void setId_model(String id_model) {
		this.id_model = id_model;
	}

	public String getCod_model() {
		return cod_model;
	}

	public void setCod_model(String cod_model) {
		this.cod_model = cod_model;
	}

	public String getNam_model() {
		return nam_model;
	}

	public void setNam_model(String nam_model) {
		this.nam_model = nam_model;
	}

	public String getDes_model() {
		return des_model;
	}

	public void setDes_model(String des_model) {
		this.des_model = des_model;
	}

	public String getId_model_execution() {
		return id_model_execution;
	}

	public void setId_model_execution(String id_model_execution) {
		this.id_model_execution = id_model_execution;
	}

	public String getDat_model_execution() {
		return dat_model_execution;
	}

	public void setDat_model_execution(String dat_model_execution) {
		this.dat_model_execution = dat_model_execution;
	}

	public String getNam_model_execution() {
		return nam_model_execution;
	}

	public void setNam_model_execution(String nam_model_execution) {
		this.nam_model_execution = nam_model_execution;
	}

	public String getDes_model_execution() {
		return des_model_execution;
	}

	public void setDes_model_execution(String des_model_execution) {
		this.des_model_execution = des_model_execution;
	}

	public String getDat_start() {
		return dat_start;
	}

	public void setDat_start(String dat_start) {
		this.dat_start = dat_start;
	}

	public String getDat_end() {
		return dat_end;
	}

	public void setDat_end(String dat_end) {
		this.dat_end = dat_end;
	}

	public String getFlg_model_execution_status() {
		return flg_model_execution_status;
	}

	public void setFlg_model_execution_status(String flg_model_execution_status) {
		this.flg_model_execution_status = flg_model_execution_status;
	}

	public String getId_status_job() {
		return id_status_job;
	}

	public void setId_status_job(String id_status_job) {
		this.id_status_job = id_status_job;
	}

	public String getId_job() {
		return id_job;
	}

	public void setId_job(String id_job) {
		this.id_job = id_job;
	}

	public String getNam_job() {
		return nam_job;
	}

	public void setNam_job(String nam_job) {
		this.nam_job = nam_job;
	}

	public String getJob_satus_type() {
		return job_satus_type;
	}

	public void setJob_satus_type(String job_satus_type) {
		this.job_satus_type = job_satus_type;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public String getFd_num_record() {
		return fd_num_record;
	}

	public void setFd_num_record(String fd_num_record) {
		this.fd_num_record = fd_num_record;
	}

	public String getFd_num_cuaa() {
		return fd_num_cuaa;
	}

	public void setFd_num_cuaa(String fd_num_cuaa) {
		this.fd_num_cuaa = fd_num_cuaa;
	}

	public String getFd_num_sup_0() {
		return fd_num_sup_0;
	}

	public void setFd_num_sup_0(String fd_num_sup_0) {
		this.fd_num_sup_0 = fd_num_sup_0;
	}

	public String getMd_num_record() {
		return md_num_record;
	}

	public void setMd_num_record(String md_num_record) {
		this.md_num_record = md_num_record;
	}

	public String getMd_num_cuaa() {
		return md_num_cuaa;
	}

	public void setMd_num_cuaa(String md_num_cuaa) {
		this.md_num_cuaa = md_num_cuaa;
	}

	public String getMd_num_sup_0() {
		return md_num_sup_0;
	}

	public void setMd_num_sup_0(String md_num_sup_0) {
		this.md_num_sup_0 = md_num_sup_0;
	}

	public String getMd_amt_max_23() {
		return md_amt_max_23;
	}

	public void setMd_amt_max_23(String md_amt_max_23) {
		this.md_amt_max_23 = md_amt_max_23;
	}

	public String getMd_amt_max_24() {
		return md_amt_max_24;
	}

	public void setMd_amt_max_24(String md_amt_max_24) {
		this.md_amt_max_24 = md_amt_max_24;
	}

	public String getMd_amt_max_25() {
		return md_amt_max_25;
	}

	public void setMd_amt_max_25(String md_amt_max_25) {
		this.md_amt_max_25 = md_amt_max_25;
	}

	public String getMd_amt_max_26() {
		return md_amt_max_26;
	}

	public void setMd_amt_max_26(String md_amt_max_26) {
		this.md_amt_max_26 = md_amt_max_26;
	}

	public String getMd_pct_biss_23() {
		return md_pct_biss_23;
	}

	public void setMd_pct_biss_23(String md_pct_biss_23) {
		this.md_pct_biss_23 = md_pct_biss_23;
	}

	public String getMd_pct_biss_24() {
		return md_pct_biss_24;
	}

	public void setMd_pct_biss_24(String md_pct_biss_24) {
		this.md_pct_biss_24 = md_pct_biss_24;
	}

	public String getMd_pct_biss_25() {
		return md_pct_biss_25;
	}

	public void setMd_pct_biss_25(String md_pct_biss_25) {
		this.md_pct_biss_25 = md_pct_biss_25;
	}

	public String getMd_pct_biss_26() {
		return md_pct_biss_26;
	}

	public void setMd_pct_biss_26(String md_pct_biss_26) {
		this.md_pct_biss_26 = md_pct_biss_26;
	}

	public String getMd_num_min_sup_level() {
		return md_num_min_sup_level;
	}

	public void setMd_num_min_sup_level(String md_num_min_sup_level) {
		this.md_num_min_sup_level = md_num_min_sup_level;
	}

	public String getMd_amt_23() {
		return md_amt_23;
	}

	public void setMd_amt_23(String md_amt_23) {
		this.md_amt_23 = md_amt_23;
	}

	public String getMd_amt_24() {
		return md_amt_24;
	}

	public void setMd_amt_24(String md_amt_24) {
		this.md_amt_24 = md_amt_24;
	}

	public String getMd_amt_25() {
		return md_amt_25;
	}

	public void setMd_amt_25(String md_amt_25) {
		this.md_amt_25 = md_amt_25;
	}

	public String getMd_amt_26() {
		return md_amt_26;
	}

	public void setMd_amt_26(String md_amt_26) {
		this.md_amt_26 = md_amt_26;
	}

	public String getMd_amt_ha_23() {
		return md_amt_ha_23;
	}

	public void setMd_amt_ha_23(String md_amt_ha_23) {
		this.md_amt_ha_23 = md_amt_ha_23;
	}

	public String getMd_amt_ha_24() {
		return md_amt_ha_24;
	}

	public void setMd_amt_ha_24(String md_amt_ha_24) {
		this.md_amt_ha_24 = md_amt_ha_24;
	}

	public String getMd_amt_ha_25() {
		return md_amt_ha_25;
	}

	public void setMd_amt_ha_25(String md_amt_ha_25) {
		this.md_amt_ha_25 = md_amt_ha_25;
	}

	public String getMd_amt_ha_26() {
		return md_amt_ha_26;
	}

	public void setMd_amt_ha_26(String md_amt_ha_26) {
		this.md_amt_ha_26 = md_amt_ha_26;
	}

	public String getMd_num_sup_avg() {
		return md_num_sup_avg;
	}

	public void setMd_num_sup_avg(String md_num_sup_avg) {
		this.md_num_sup_avg = md_num_sup_avg;
	}

	public String getMd_num_cuaa_avg() {
		return md_num_cuaa_avg;
	}

	public void setMd_num_cuaa_avg(String md_num_cuaa_avg) {
		this.md_num_cuaa_avg = md_num_cuaa_avg;
	}

	public String getMd_num_sup_0_avg() {
		return md_num_sup_0_avg;
	}

	public void setMd_num_sup_0_avg(String md_num_sup_0_avg) {
		this.md_num_sup_0_avg = md_num_sup_0_avg;
	}

	public String getMd_amt_23_avg() {
		return md_amt_23_avg;
	}

	public void setMd_amt_23_avg(String md_amt_23_avg) {
		this.md_amt_23_avg = md_amt_23_avg;
	}

	public String getMd_amt_24_avg() {
		return md_amt_24_avg;
	}

	public void setMd_amt_24_avg(String md_amt_24_avg) {
		this.md_amt_24_avg = md_amt_24_avg;
	}

	public String getMd_amt_25_avg() {
		return md_amt_25_avg;
	}

	public void setMd_amt_25_avg(String md_amt_25_avg) {
		this.md_amt_25_avg = md_amt_25_avg;
	}

	public String getMd_amt_26_avg() {
		return md_amt_26_avg;
	}

	public void setMd_amt_26_avg(String md_amt_26_avg) {
		this.md_amt_26_avg = md_amt_26_avg;
	}

}
