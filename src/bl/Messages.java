package bl;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import gui.Home;

public class Messages {
	
	private Messages() {
		// do not instantiate
	}

	////////////////////////////////////////////////////////////////////////////
	private static final String BUNDLE_NAME = "international.messages"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = loadBundle();

	public static ResourceBundle loadBundle() {

		if (Home.getLANGUAGE().equalsIgnoreCase(ConfigurationProperties.PROP_LANGUAGE_EN)) {

			return ResourceBundle.getBundle(BUNDLE_NAME, Locale.ENGLISH);

		} else {
			return ResourceBundle.getBundle(BUNDLE_NAME, Locale.ITALIAN);

		}

	}

	public static String getString(String key) {
		try {
			// ResourceBundle bundle = Beans.isDesignTime() ? loadBundle() :
			// RESOURCE_BUNDLE;
			ResourceBundle bundle = loadBundle();
			return bundle.getString(key);

		} catch (MissingResourceException e) {
			return "!" + key + "!";
		}
	}
}
