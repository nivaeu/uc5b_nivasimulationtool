package bl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import gui.Home;

public class ConfigurationProperties {
	public static final String FOLDER_DATA = "data";
	public static final String FOLDER_CONF = "conf";
	public static final String NAME_FILE_CONFIG = "config.properties";
	public static final String PROP_DATABASE = "database";
	public static final String PROP_IP = "ip";
	public static final String PROP_PORTA = "porta";
	public static final String PROP_USERNAME = "username";
	public static final String PROP_PASSWORD = "password";
	public static final String PROP_DB_CREATO = "db_creato";
	public static final String PROP_LANGUAGE = "language";
	public static final String PROP_LANGUAGE_IT = "it";
	public static final String PROP_LANGUAGE_EN = "en";
	public static final String PROP_DB_CREATO_SI = "S";
	public static final String PROP_DB_CREATO_NO = "N";

	private static String[] key = { PROP_DATABASE, PROP_IP, PROP_PORTA, PROP_USERNAME, PROP_PASSWORD, PROP_DB_CREATO,
			PROP_LANGUAGE };

	public static void main(String args[]) {

	}

	public ConfigurationProperties() {
		super();

	}

	public static void configureEnviroment() {
		File dirData = new File(Home.getPathJar() + "/" + FOLDER_DATA);
		File dirConf = new File(Home.getPathJar() + "/" + FOLDER_CONF);
		File fileConf = new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG);
		// cartella contenente i file csv
		if (!dirData.exists())
			dirData.mkdir();

		if (!fileConf.exists()) {
			if (!dirConf.exists())
				dirConf.mkdir();
			try {
				// copio il file
				System.out.println("5555" + ConfigurationProperties.class.getClass()
						.getResource("/resources/config.properties").getPath());
				UtilFile.copiaFile(
						ConfigurationProperties.class.getClass().getResourceAsStream("/resources/config.properties"),
						Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static synchronized String[] getProperties() throws IOException {
		String[] list = new String[7];
		Properties properties = new Properties();
		try {
			properties.load(
					new FileInputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG)));
		} catch (Exception e) {
			e.printStackTrace();

		}

		list[0] = properties.getProperty(PROP_DATABASE);
		list[1] = properties.getProperty(PROP_IP);
		list[2] = properties.getProperty(PROP_PORTA);
		list[3] = properties.getProperty(PROP_USERNAME);
		list[4] = properties.getProperty(PROP_PASSWORD);
		list[5] = properties.getProperty(PROP_DB_CREATO);
		list[6] = properties.getProperty(PROP_LANGUAGE);

		return list;
	}

	public static synchronized void setProperties(String keys[], String value[]) throws IOException {
		Properties properties = new Properties();
		try {
			properties.load(
					new FileInputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG)));
		} catch (Exception e) {
			e.printStackTrace();
		}

		properties.setProperty(keys[0], value[0]);
		properties.setProperty(keys[1], value[1]);
		properties.setProperty(keys[2], value[2]);
		properties.setProperty(keys[3], value[3]);
		properties.setProperty(keys[4], value[4]);
		properties.setProperty(keys[5], value[5]);
		properties.setProperty(keys[6], value[6]);

		properties.store(
				new FileOutputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG), false),
				null);
		Home.setParametriDB();
	}

	public static synchronized void setProperties(String value[]) throws IOException {
		Properties properties = new Properties();
		try {
			properties.load(
					new FileInputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG)));
		} catch (Exception e) {
			e.printStackTrace();

		}
		properties.setProperty(key[0], value[0]);
		properties.setProperty(key[1], value[1]);
		properties.setProperty(key[2], value[2]);
		properties.setProperty(key[3], value[3]);
		properties.setProperty(key[4], value[4]);
		properties.setProperty(key[5], value[5]);
		properties.setProperty(key[6], value[6]);
		properties.store(
				new FileOutputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG), false),
				null);
		Home.setParametriDB();
	}

	public static synchronized void setProperties(String key, String value) throws IOException {
		Properties properties = new Properties();
		try {
			properties.load(
					new FileInputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG)));
		} catch (Exception e) {
			e.printStackTrace();

		}

		properties.setProperty(key, value);
		properties.store(
				new FileOutputStream(new File(Home.getPathJar() + "/" + FOLDER_CONF + "/" + NAME_FILE_CONFIG), false),
				null);

		Home.setParametriDB();
	}

}