package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import bl.ConfigurationProperties;
import bl.ConnectionDB;
import bl.Messages;
import bl.UtilKettle;

public class PanelBodySetting extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2637402220760419849L;
	private JTextField txtIP;
	private JTextField txtPorta;
	private JTextField txtDB;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Create the panel.
	 */
	public PanelBodySetting() {
		setBackground(Color.WHITE);
		setName(PanelBodySetting.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.3, 0.4, 0.3 };
		gridBagLayout.rowWeights = new double[] { 0.2, 0.5, 0.3 };
		setLayout(gridBagLayout);
		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);
		// ImageIcon icon = new ImageIcon(Home.class.getResource("/images/home.png"));
		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);
		// menuLeft.add(button);
		ImageIcon iconBreadcrumbSetting = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage();
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance(160, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbSetting = new ImageIcon(imageBreadcrumbSetting);
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.setText(Messages.getString("setting.setting"));
		labelBreadcrumbSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbSetting);

		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenter = new JPanel();
		bodyCenter.setBackground(Color.WHITE);
		bodyCenter.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(76, 144, 33)));
		GridBagConstraints gbc_bodyCenter = new GridBagConstraints();
		gbc_bodyCenter.insets = new Insets(5, 5, 5, 5);
		gbc_bodyCenter.fill = GridBagConstraints.BOTH;
		gbc_bodyCenter.gridx = 1;
		gbc_bodyCenter.gridy = 1;
		add(bodyCenter, gbc_bodyCenter);
		GridBagLayout gbl_bodyCenter = new GridBagLayout();
		gbl_bodyCenter.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_bodyCenter.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_bodyCenter.columnWeights = new double[] { 0.1, 0.4, 0.1, 0.4 };
		gbl_bodyCenter.rowWeights = new double[] { 0.25, 0.25, 0.25, 0.25 };
		bodyCenter.setLayout(gbl_bodyCenter);

		JLabel lblInfoDB = new JLabel(Messages.getString("setting.info.database"));
		lblInfoDB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblInfoDB.setForeground(new Color(0, 110, 171));
		lblInfoDB.setBorder(new EmptyBorder(5, 5, 0, 0));
		GridBagConstraints gbc_lblInfoDB = new GridBagConstraints();
		gbc_lblInfoDB.anchor = GridBagConstraints.WEST;
		gbc_lblInfoDB.insets = new Insets(0, 0, 5, 0);
		gbc_lblInfoDB.gridx = 0;
		gbc_lblInfoDB.gridy = 0;
		gbc_lblInfoDB.gridwidth = 4;
		bodyCenter.add(lblInfoDB, gbc_lblInfoDB);

		JLabel lblNewLabel_1 = new JLabel(Messages.getString("setting.database.nome"));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		bodyCenter.add(lblNewLabel_1, gbc_lblNewLabel_1);

		txtDB = new JTextField();
		txtDB.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtDB.setText(Home.getDB_NAME());
		GridBagConstraints gbc_txtDB = new GridBagConstraints();
		gbc_txtDB.insets = new Insets(0, 0, 5, 5);
		gbc_txtDB.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtDB.gridx = 1;
		gbc_txtDB.gridy = 1;
		bodyCenter.add(txtDB, gbc_txtDB);
		txtDB.setColumns(10);

		JLabel lblDB = new JLabel(Messages.getString("setting.database.ip"));
		GridBagConstraints gbc_lblDB = new GridBagConstraints();
		gbc_lblDB.anchor = GridBagConstraints.EAST;
		gbc_lblDB.insets = new Insets(5, 0, 5, 5);
		gbc_lblDB.gridx = 0;
		gbc_lblDB.gridy = 2;
		bodyCenter.add(lblDB, gbc_lblDB);

		txtIP = new JTextField();
		txtIP.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtIP.setText(Home.getDB_IP()); // $NON-NLS-1$
		GridBagConstraints gbc_txtIP = new GridBagConstraints();
		gbc_txtIP.insets = new Insets(5, 0, 5, 5);
		gbc_txtIP.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtIP.gridx = 1;
		gbc_txtIP.gridy = 2;
		bodyCenter.add(txtIP, gbc_txtIP);
		txtIP.setColumns(10);

		JLabel lblNewLabel = new JLabel(Messages.getString("setting.database.porta"));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(5, 0, 5, 5);
		gbc_lblNewLabel.gridx = 2;
		gbc_lblNewLabel.gridy = 2;
		bodyCenter.add(lblNewLabel, gbc_lblNewLabel);

		txtPorta = new JTextField();
		txtPorta.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPorta.setText(Home.getDB_PORTA());
		GridBagConstraints gbc_txtPorta = new GridBagConstraints();
		gbc_txtPorta.insets = new Insets(5, 0, 5, 5);
		gbc_txtPorta.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPorta.gridx = 3;
		gbc_txtPorta.gridy = 2;
		bodyCenter.add(txtPorta, gbc_txtPorta);
		txtPorta.setColumns(8);

		JLabel lblDBUser = new JLabel(Messages.getString("setting.user"));
		GridBagConstraints gbc_lblDBUser = new GridBagConstraints();
		gbc_lblDBUser.anchor = GridBagConstraints.EAST;
		gbc_lblDBUser.insets = new Insets(5, 0, 5, 5);
		gbc_lblDBUser.gridx = 0;
		gbc_lblDBUser.gridy = 3;
		bodyCenter.add(lblDBUser, gbc_lblDBUser);

		txtUsername = new JTextField();
		txtUsername.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtUsername.setText(Home.getDB_UTENTE());

		GridBagConstraints gbc_txtUsername = new GridBagConstraints();
		gbc_txtUsername.insets = new Insets(5, 0, 5, 5);
		gbc_txtUsername.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUsername.gridx = 1;
		gbc_txtUsername.gridy = 3;
		bodyCenter.add(txtUsername, gbc_txtUsername);
		txtUsername.setColumns(10);

		JLabel lblDBPassword = new JLabel(Messages.getString("setting.password"));
		GridBagConstraints gbc_lblDBPassword = new GridBagConstraints();
		gbc_lblDBPassword.anchor = GridBagConstraints.EAST;
		gbc_lblDBPassword.insets = new Insets(5, 0, 0, 5);
		gbc_lblDBPassword.gridx = 2;
		gbc_lblDBPassword.gridy = 3;
		bodyCenter.add(lblDBPassword, gbc_lblDBPassword);

		txtPassword = new JPasswordField();
		txtPassword.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPassword.setText(Home.getDB_PASSWORD());
		GridBagConstraints gbc_txtPassword = new GridBagConstraints();
		gbc_txtPassword.insets = new Insets(5, 0, 5, 5);
		gbc_txtPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPassword.gridx = 3;
		gbc_txtPassword.gridy = 3;
		bodyCenter.add(txtPassword, gbc_txtPassword);

		JPanel bodyFooter = new JPanel();
		FlowLayout flowLayout_4 = (FlowLayout) bodyFooter.getLayout();
		flowLayout_4.setVgap(0);
		bodyFooter.setBackground(Color.WHITE);
		GridBagConstraints gbc_bodyFooter = new GridBagConstraints();
		gbc_bodyFooter.insets = new Insets(0, 0, 0, 5);
		gbc_bodyFooter.fill = GridBagConstraints.BOTH;
		gbc_bodyFooter.gridx = 1;
		gbc_bodyFooter.gridy = 2;
		add(bodyFooter, gbc_bodyFooter);

		JButton btnConferma = new JButton(Messages.getString("button.conferma")); //$NON-NLS-1$
		btnConferma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = txtPassword.getText();
				if (txtIP.getText().isEmpty() || txtPorta.getText().isEmpty() || txtUsername.getText().isEmpty()
						|| password.isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("setting.parametri.db.vuoti"));
				} else {
					ConnectionDB c = new ConnectionDB();
					if (c.testConnection(txtDB.getText(), txtIP.getText(), txtPorta.getText(), txtUsername.getText(),
							password)
							|| c.testConnection("", txtIP.getText(), txtPorta.getText(), txtUsername.getText(),
									password)) {

						try {
							String[] value = { txtDB.getText().trim(), txtIP.getText().trim(),
									txtPorta.getText().trim(), txtUsername.getText(), password, Home.getDB_CREATO(),
									Home.getLANGUAGE() };
							ConfigurationProperties.setProperties(value);
							UtilKettle.updateKettleJDBCEnviroment();
							c.creaDatabase();
							c.initDB();
							JOptionPane.showMessageDialog(null, Messages.getString("registrazione.db.ok"));
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(null, Messages.getString("setting.error.write.properties"));
						}
					} else {
						JOptionPane.showMessageDialog(null, Messages.getString("setting.error.connessione"));
					}
				}
			}
		});
		btnConferma.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnConferma.setForeground(Color.WHITE);
		btnConferma.setBackground(new Color(0, 110, 171));
		btnConferma.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bodyFooter.add(btnConferma);

	}

}
