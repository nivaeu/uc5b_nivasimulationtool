package gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import bl.Messages;

public class PanelHeader extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8378225477561500139L;
	private static Label title = null;

	/**
	 * Create the panel.
	 */
	public PanelHeader() {
		setBackground(Color.WHITE);

		// Gestione header

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0 };
		gridBagLayout.columnWeights = new double[] { 0.3, 0.4, 0.3 };
		gridBagLayout.rowWeights = new double[] { 1.0 };
		setLayout(gridBagLayout);

		ImageIcon iconLogoNiva = new ImageIcon(Home.class.getResource("/images/logo_niva.png"));
		Image imageLogoNiva = iconLogoNiva.getImage();
		imageLogoNiva = imageLogoNiva.getScaledInstance(100, 80, java.awt.Image.SCALE_SMOOTH);
		iconLogoNiva = new ImageIcon(imageLogoNiva);

		JLabel labelLogoNiva = new JLabel(iconLogoNiva);
		labelLogoNiva.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoNiva.setToolTipText(Messages.getString("niva"));
		labelLogoNiva.setBorder(new EmptyBorder(0, 0, 0, 0));
		labelLogoNiva.setBorder(new EmptyBorder(0, 0, 0, 0));
		Panel panel = new Panel();

		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(0);
		flowLayout.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.insets = new Insets(0, 0, 0, 0);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		panel.add(labelLogoNiva);

		Panel panel_center = new Panel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_center.getLayout();
		flowLayout_2.setVgap(20);
		GridBagConstraints gbc_panel_center = new GridBagConstraints();
		gbc_panel_center.fill = GridBagConstraints.BOTH;
		gbc_panel_center.insets = new Insets(0, 0, 5, 5);
		gbc_panel_center.gridx = 1;
		gbc_panel_center.gridy = 0;
		add(panel_center, gbc_panel_center);

		// topPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.RED));
		title = new Label(Messages.getString("niva.eu.wide"));
		title.setAlignment(Label.CENTER);
		title.setForeground(new Color(0, 110, 171));
		title.setFont(new Font("Serif", Font.BOLD, 26));
		GridBagConstraints gbc_title = new GridBagConstraints();
		gbc_title.insets = new Insets(0, 0, 0, 5);
		gbc_title.anchor = GridBagConstraints.NORTHWEST;
		gbc_title.gridx = 1;
		gbc_title.gridy = 0;
		panel_center.add(title);

		ImageIcon iconLogoCrea = new ImageIcon(Home.class.getResource("/images/logo_cliente.png"));
		Image imageLogoCrea = iconLogoCrea.getImage();
		imageLogoCrea = imageLogoCrea.getScaledInstance(200, 80, java.awt.Image.SCALE_SMOOTH);
		iconLogoCrea = new ImageIcon(imageLogoCrea);

		Panel panel_right = new Panel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_right.getLayout();
		flowLayout_1.setVgap(0);
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		GridBagConstraints gbc_panel_right = new GridBagConstraints();
		gbc_panel_right.fill = GridBagConstraints.BOTH;
		gbc_panel_right.insets = new Insets(0, 0, 5, 0);
		gbc_panel_right.gridx = 2;
		gbc_panel_right.gridy = 0;
		add(panel_right, gbc_panel_right);
		JLabel labelLogoCrea = new JLabel(iconLogoCrea);
		panel_right.add(labelLogoCrea);
		labelLogoCrea.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoCrea.setToolTipText(Messages.getString("niva"));

	}

	public static void updateLabel() {
		title.setText(Messages.getString("niva.smt"));
	}

}
