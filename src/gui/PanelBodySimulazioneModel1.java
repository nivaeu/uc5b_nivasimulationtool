package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import bl.ConfigurationProperties;
import bl.ConnectionDB;
import bl.Messages;
import bl.UtilFile;
import bl.UtilKettle;
import bl.UtilPrint;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class PanelBodySimulazioneModel1 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7647813675306805072L;
	private JTextField txtPathFile;
	private JFormattedTextField txtParam1;
	private JFormattedTextField txtParam3;
	private JFormattedTextField txtParam6;
	private JFormattedTextField txtParam2;
	private JFormattedTextField txtParam4;
	private JFormattedTextField txtParam7;
	private JFormattedTextField txtParam8;
	private JFormattedTextField txtParam9;
	private JFormattedTextField txtParam10;
	private String dataDB;

	/**
	 * Create the panel.
	 */
	public PanelBodySimulazioneModel1(String modelTitle) {
		setBackground(Color.WHITE);
		setName(PanelBodySimulazioneModel1.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.2, 0.6, 0.2 };
		gridBagLayout.rowWeights = new double[] { 0.2, 0.7, 0.1 };
		setLayout(gridBagLayout);

		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);
		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);

		ImageIcon iconBreadcrumbSetting = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage();
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance(150, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbSetting = new ImageIcon(imageBreadcrumbSetting);
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setText(Messages.getString("simulazione.simulazione"));
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodySimulazione());
			}
		});
		labelBreadcrumbSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbSetting);

		ImageIcon iconBreadcrumbNewSimulazione = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbNewSimulazione = iconBreadcrumbNewSimulazione.getImage();
		imageBreadcrumbNewSimulazione = imageBreadcrumbNewSimulazione.getScaledInstance(180, 30,
				java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbNewSimulazione = new ImageIcon(imageBreadcrumbNewSimulazione);
		JLabel labelBreadcrumbNewSimulazione = new JLabel(iconBreadcrumbNewSimulazione);
		labelBreadcrumbNewSimulazione.setForeground(Color.WHITE);
		labelBreadcrumbNewSimulazione.setText(Messages.getString("simulazione.nuova.simulazione"));
		labelBreadcrumbNewSimulazione.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbNewSimulazione.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		labelBreadcrumbNewSimulazione.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyNewSimulazione());
			}

		});
		menuLeft.add(labelBreadcrumbNewSimulazione);

		ImageIcon iconBreadcrumbModello1 = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbModello1 = iconBreadcrumbModello1.getImage();
		imageBreadcrumbModello1 = imageBreadcrumbModello1.getScaledInstance(300, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbModello1 = new ImageIcon(imageBreadcrumbModello1);
		JLabel labelBreadcrumbModello1 = new JLabel(iconBreadcrumbModello1);
		labelBreadcrumbModello1.setForeground(Color.WHITE);
		labelBreadcrumbModello1.setText(modelTitle.substring(0, modelTitle.indexOf("-")));
		labelBreadcrumbModello1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		menuLeft.add(labelBreadcrumbModello1);

		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenter = new JPanel();
		bodyCenter.setBackground(Color.WHITE);
		// bodyCenter.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED));
		GridBagConstraints gbc_bodyCenter = new GridBagConstraints();
		gbc_bodyCenter.insets = new Insets(10, 10, 10, 10);
		gbc_bodyCenter.fill = GridBagConstraints.BOTH;
		gbc_bodyCenter.gridx = 0;
		gbc_bodyCenter.gridy = 1;
		gbc_bodyCenter.gridwidth = 4;

		add(bodyCenter, gbc_bodyCenter);
		GridBagLayout gbl_bodyCenter = new GridBagLayout();
		gbl_bodyCenter.columnWidths = new int[] { 0 };
		gbl_bodyCenter.rowHeights = new int[] { 0 };
		gbl_bodyCenter.columnWeights = new double[] { 1.0 };
		gbl_bodyCenter.rowWeights = new double[] { 1.0 };

		bodyCenter.setLayout(gbl_bodyCenter);

		JPanel bodyCenterLeft = new JPanel();
		bodyCenterLeft.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_1_1 = new GridBagConstraints();
		gbc_panel_1_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1_1.gridx = 0;
		gbc_panel_1_1.gridy = 0;
		bodyCenter.add(bodyCenterLeft, gbc_panel_1_1);
		GridBagLayout gbl_bodyCenterLeft = new GridBagLayout();
		gbl_bodyCenterLeft.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.columnWeights = new double[] { 0.20, 0.25, 0.20, 0.25, 0.1 };
		gbl_bodyCenterLeft.rowWeights = new double[] { 0.1, 0.3, 0.1, 0.4, 0.1, 0.1, 0.1, 0.1, 0.1 };
		bodyCenterLeft.setLayout(gbl_bodyCenterLeft);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		gbc_panel.gridwidth = 5;
		bodyCenterLeft.add(panel, gbc_panel);

		JLabel lblNewLabel = new JLabel(Messages.getString("simulazione.seleziona.file"));
		panel.add(lblNewLabel);

		txtPathFile = new JTextField();
		txtPathFile.setEditable(false);
		txtPathFile.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_txtPathFile = new GridBagConstraints();
		gbc_txtPathFile.insets = new Insets(0, 0, 5, 5);
		gbc_txtPathFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPathFile.gridx = 0;
		gbc_txtPathFile.gridy = 1;
		gbc_txtPathFile.gridwidth = 3;
		bodyCenterLeft.add(txtPathFile, gbc_txtPathFile);
		txtPathFile.setColumns(10);

		JButton btnNewButton = new JButton(Messages.getString("button.sfoglia"));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 110, 171));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV FILES", "csv", "csv");
				fileChooser.setFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog((Component) e.getSource());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						txtPathFile.setText(file.toString());
					} catch (Exception ex) {
						System.out.println("problem accessing file" + file.getAbsolutePath());
					}
				} else {
					// simulazione.seleziona.file.empty
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.seleziona.file.empty"));
				}

			}

		});
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.gridwidth = 2;
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 1;
		bodyCenterLeft.add(btnNewButton, gbc_btnNewButton);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_3 = (FlowLayout) panel_1.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		gbc_panel_1.gridwidth = 5;
		bodyCenterLeft.add(panel_1, gbc_panel_1);

		JLabel lblNewLabel_1 = new JLabel(Messages.getString("simulazione.scelta.parametri"));
		panel_1.add(lblNewLabel_1);

		JLabel lblNewLabel_5 = new JLabel(Messages.getString("parametro.01") + "*");
		lblNewLabel_5.setToolTipText(Messages.getString("parametro.01.title"));
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_5.gridx = 0;
		gbc_lblNewLabel_5.gridy = 4;
		bodyCenterLeft.add(lblNewLabel_5, gbc_lblNewLabel_5);

		txtParam1 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam1.getText().isEmpty()) {
					txtParam1.setValue(null);
				}
			}
		});
		txtParam1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam1.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam1.getText().length() >= 18 || txtParam1.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtParam1 = new GridBagConstraints();
		gbc_txtParam1.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam1.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam1.gridx = 1;
		gbc_txtParam1.gridy = 4;
		bodyCenterLeft.add(txtParam1, gbc_txtParam1);

		JLabel lblNewLabel_2 = new JLabel("%");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridx = 4;
		gbc_lblNewLabel_2.gridy = 4;
		bodyCenterLeft.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel lblNewLabel_6 = new JLabel(Messages.getString("parametro.02") + "*"); //$NON-NLS-1$
		lblNewLabel_6.setToolTipText(Messages.getString("parametro.02.title"));
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 5;
		bodyCenterLeft.add(lblNewLabel_6, gbc_lblNewLabel_6);

		txtParam2 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam2.getText().isEmpty()) {
					txtParam2.setValue(null);
				}
			}
		});
		txtParam2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam2.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam2.getText().length() >= 18 || txtParam2.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam2 = new GridBagConstraints();
		gbc_txtParam2.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam2.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam2.gridx = 1;
		gbc_txtParam2.gridy = 5;
		bodyCenterLeft.add(txtParam2, gbc_txtParam2);

		JLabel lblNewLabel_2_1 = new JLabel("%");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2_1 = new GridBagConstraints();
		gbc_lblNewLabel_2_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1.gridx = 4;
		gbc_lblNewLabel_2_1.gridy = 5;
		bodyCenterLeft.add(lblNewLabel_2_1, gbc_lblNewLabel_2_1);

		JLabel lblNewLabel_3 = new JLabel(Messages.getString("parametro.03") + "*"); //$NON-NLS-1$
		lblNewLabel_3.setToolTipText(Messages.getString("parametro.03.title"));
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_3, gbc_lblNewLabel_3);

		txtParam3 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam3.getText().isEmpty()) {
					txtParam3.setValue(null);
				}
			}
		});
		txtParam3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam3.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam3.getText().length() >= 18 || txtParam3.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam3 = new GridBagConstraints();
		gbc_txtParam3.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam3.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam3.gridx = 1;
		gbc_txtParam3.gridy = 6;
		bodyCenterLeft.add(txtParam3, gbc_txtParam3);

		JLabel lblNewLabel_2_2 = new JLabel("%");
		lblNewLabel_2_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2_2.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2_2 = new GridBagConstraints();
		gbc_lblNewLabel_2_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_2.gridx = 4;
		gbc_lblNewLabel_2_2.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_2_2, gbc_lblNewLabel_2_2);

		JLabel lblNewLabel_7 = new JLabel(Messages.getString("parametro.04") + "*");
		lblNewLabel_7.setToolTipText(Messages.getString("parametro.04.title"));
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 7;
		bodyCenterLeft.add(lblNewLabel_7, gbc_lblNewLabel_7);

		txtParam4 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam4.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam4.getText().isEmpty()) {
					txtParam4.setValue(null);
				}
			}
		});
		txtParam4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam4.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam4.getText().length() >= 18 || txtParam4.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam4 = new GridBagConstraints();
		gbc_txtParam4.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam4.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam4.gridx = 1;
		gbc_txtParam4.gridy = 7;
		bodyCenterLeft.add(txtParam4, gbc_txtParam4);

		JLabel lblNewLabel_2_3 = new JLabel("%");
		lblNewLabel_2_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2_3.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2_3 = new GridBagConstraints();
		gbc_lblNewLabel_2_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_3.gridx = 4;
		gbc_lblNewLabel_2_3.gridy = 7;
		bodyCenterLeft.add(lblNewLabel_2_3, gbc_lblNewLabel_2_3);

		JLabel lblNewLabel_4 = new JLabel(Messages.getString("parametro.06") + "*"); //$NON-NLS-1$
		lblNewLabel_4.setToolTipText(Messages.getString("parametro.06.title"));
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 8;
		bodyCenterLeft.add(lblNewLabel_4, gbc_lblNewLabel_4);

		txtParam6 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam6.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam6.getText().isEmpty()) {
					txtParam6.setValue(null);
				}
			}
		});
		txtParam6.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam6.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam6.getText().length() >= 18 || txtParam6.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam6 = new GridBagConstraints();
		gbc_txtParam6.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam6.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam6.gridx = 1;
		gbc_txtParam6.gridy = 8;
		bodyCenterLeft.add(txtParam6, gbc_txtParam6);

		JLabel lblNewLabel_8 = new JLabel(Messages.getString("parametro.07") + "*");
		lblNewLabel_8.setToolTipText(Messages.getString("parametro.07.title"));
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 2;
		gbc_lblNewLabel_8.gridy = 4;
		bodyCenterLeft.add(lblNewLabel_8, gbc_lblNewLabel_8);

		txtParam7 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam7.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam7.getText().isEmpty()) {
					txtParam7.setValue(null);
				}
			}
		});
		txtParam7.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam7.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam7.getText().length() >= 18 || txtParam7.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam7 = new GridBagConstraints();
		gbc_txtParam7.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam7.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam7.gridx = 3;
		gbc_txtParam7.gridy = 4;
		bodyCenterLeft.add(txtParam7, gbc_txtParam7);

		JLabel lblNewLabel_9 = new JLabel(Messages.getString("parametro.08") + "*");
		lblNewLabel_9.setToolTipText(Messages.getString("parametro.08.title"));
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 2;
		gbc_lblNewLabel_9.gridy = 5;
		bodyCenterLeft.add(lblNewLabel_9, gbc_lblNewLabel_9);

		txtParam8 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam8.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam8.getText().isEmpty()) {
					txtParam8.setValue(null);
				}
			}
		});
		txtParam8.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam8.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam8.getText().length() >= 18 || txtParam8.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam8 = new GridBagConstraints();
		gbc_txtParam8.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam8.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam8.gridx = 3;
		gbc_txtParam8.gridy = 5;
		bodyCenterLeft.add(txtParam8, gbc_txtParam8);

		JLabel lblNewLabel_10 = new JLabel(Messages.getString("parametro.09") + "*");
		lblNewLabel_10.setToolTipText(Messages.getString("parametro.09.title"));
		GridBagConstraints gbc_lblNewLabel_10 = new GridBagConstraints();
		gbc_lblNewLabel_10.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_10.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_10.gridx = 2;
		gbc_lblNewLabel_10.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_10, gbc_lblNewLabel_10);

		txtParam9 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam9.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam9.getText().isEmpty()) {
					txtParam9.setValue(null);
				}
			}
		});
		txtParam9.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam9.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam9.getText().length() >= 18 || txtParam9.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam9 = new GridBagConstraints();
		gbc_txtParam9.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam9.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam9.gridx = 3;
		gbc_txtParam9.gridy = 6;
		bodyCenterLeft.add(txtParam9, gbc_txtParam9);

		JLabel lblNewLabel_11 = new JLabel(Messages.getString("parametro.10") + "*");
		lblNewLabel_11.setToolTipText(Messages.getString("parametro.10.title"));
		GridBagConstraints gbc_lblNewLabel_11 = new GridBagConstraints();
		gbc_lblNewLabel_11.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_11.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_11.gridx = 2;
		gbc_lblNewLabel_11.gridy = 7;
		bodyCenterLeft.add(lblNewLabel_11, gbc_lblNewLabel_11);

		txtParam10 = new JFormattedTextField(NumberFormat.getInstance());
		txtParam10.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtParam10.getText().isEmpty()) {
					txtParam10.setValue(null);
				}
			}
		});
		txtParam10.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtParam10.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (txtParam10.getText().length() >= 18 || txtParam10.getText().equalsIgnoreCase("-")) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtParam10 = new GridBagConstraints();
		gbc_txtParam10.insets = new Insets(0, 0, 5, 5);
		gbc_txtParam10.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtParam10.gridx = 3;
		gbc_txtParam10.gridy = 7;
		bodyCenterLeft.add(txtParam10, gbc_txtParam10);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 0, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 9;
		gbc_panel_2.gridwidth = 2;
		bodyCenterLeft.add(panel_2, gbc_panel_2);

		JButton button = new JButton(Messages.getString("button.esegui.simulazione"));
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		// set valori di default da eliminare
		txtParam6.setText("1,0000");

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Date curr = new Date();
				String anno = "" + (curr.getYear() + 1900);
				String mese = (curr.getMonth() + 1) > 9 ? "" + (curr.getMonth() + 1) : "0" + (curr.getMonth() + 1);
				String giorno = curr.getDate() > 9 ? "" + curr.getDate() : "0" + curr.getDate();
				String sData = anno + "-" + mese + "-" + giorno;
				String hh = curr.getHours() > 9 ? "" + curr.getHours() : "0" + curr.getHours();
				String mm = curr.getMinutes() > 9 ? "" + curr.getMinutes() : "0" + curr.getMinutes();
				String ss = curr.getSeconds() > 9 ? "" + curr.getSeconds() : "0" + curr.getSeconds();
				String dataFile = "PYH_FILE_" + sData + " " + hh + mm + ss;
				dataDB = "" + sData + " " + hh + ":" + mm + ":" + ss;

				if (txtPathFile.getText().isEmpty() || txtParam1.getText().isEmpty() || txtParam2.getText().isEmpty()
						|| txtParam3.getText().isEmpty() || txtParam4.getText().isEmpty()
						|| txtParam6.getText().isEmpty() || txtParam7.getText().isEmpty()
						|| txtParam8.getText().isEmpty() || txtParam9.getText().isEmpty()
						|| txtParam10.getText().isEmpty()) {

					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.chkdati"));
				} else {

					int input = JOptionPane.showConfirmDialog(null, Messages.getString("simulazione.conferma.avvio"));
					if (input == 0) {
						try {

							UtilFile.copiaFile(txtPathFile.getText(), Home.getPathJar() + "/"
									+ ConfigurationProperties.FOLDER_DATA + "/" + dataFile + ".csv");

							SwingWorker work = createWorker(dataDB);
							work.execute();
							JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avviata"));
							Home.switchPanel(new PanelBodySimulazione());

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} finally {

						}

					} else {

					}

				}
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(0, 110, 171));
		panel_2.add(button);

	}

	public SwingWorker<Boolean, Integer> createWorker(String dataDB) {
		return new SwingWorker<Boolean, Integer>() {
			@Override
			protected Boolean doInBackground() throws Exception {
				try {

					UtilKettle.executeJobPYH(txtParam1.getText(), txtParam2.getText(), txtParam3.getText(),
							txtParam4.getText(), dataDB, txtParam6.getText(), txtParam7.getText(), txtParam8.getText(),
							txtParam9.getText(), txtParam10.getText());

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avvio.ko"));
				}
				return true;
			}

			@Override
			protected void process(List<Integer> chunks) {

			}

			@Override
			protected void done() {
				boolean bStatus = false;
				try {
					UtilPrint print = new UtilPrint();
					print.Stampa(dataDB);
					if (Home.getPanelActive().equalsIgnoreCase(PanelBodySimulazione.class.getName())) {
						// PanelBodySimulazione.getElencoSimulazioni();
						Home.switchPanel(new PanelBodySimulazione());
						// JOptionPane.showMessageDialog(null, "aggiorno
						// tabella","",JOptionPane.ERROR_MESSAGE);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println("Finished with status " + bStatus);
			}
		};
	}

	private String esitoJob(String data) {
		String result = null;
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			PreparedStatement st = connection
					.prepareStatement("select * from sys_status_job  where dat_exec_job ='" + data + "'");
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				result = rs.getString("job_status_type") + ":" + rs.getString("job_status");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

}
