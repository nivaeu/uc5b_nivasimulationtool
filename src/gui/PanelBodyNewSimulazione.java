package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import bl.ConnectionDB;
import bl.Messages;

public class PanelBodyNewSimulazione  extends JPanel {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3340999272165814753L;
    private int modelSelect=0; 
    Vector<String> vModel;
	/**
	 * Create the panel.
	 */
	public PanelBodyNewSimulazione(){
		setBackground(Color.WHITE);
		setName(PanelBodyNewSimulazione.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.2, 0.6, 0.2};
		gridBagLayout.rowWeights = new double[]{0.2, 0.6, 0.2};
		setLayout(gridBagLayout);
		ImageIcon iconHome =new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage() ;  
		imageHome = imageHome.getScaledInstance( 30, 30,  java.awt.Image.SCALE_SMOOTH ) ;  
		iconHome = new ImageIcon( imageHome );
		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		JPanel menuLeft = new JPanel(); 
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);
		
		ImageIcon iconBreadcrumbSetting =new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage() ;  
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance( 150, 30,  java.awt.Image.SCALE_SMOOTH ) ;  
		iconBreadcrumbSetting = new ImageIcon( imageBreadcrumbSetting );
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setText(Messages.getString("simulazione.simulazione"));
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		labelBreadcrumbSetting.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodySimulazione());
			}
		});
		
		menuLeft.add(labelBreadcrumbSetting);
		
		ImageIcon iconBreadcrumbNewSimulazione=new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbNewSimulazione = iconBreadcrumbNewSimulazione.getImage() ;  
		imageBreadcrumbNewSimulazione = imageBreadcrumbNewSimulazione.getScaledInstance( 180, 30,  java.awt.Image.SCALE_SMOOTH ) ;  
		iconBreadcrumbNewSimulazione = new ImageIcon( imageBreadcrumbNewSimulazione );
		JLabel labelBreadcrumbNewSimulazione = new JLabel(iconBreadcrumbNewSimulazione);
		labelBreadcrumbNewSimulazione.setForeground(Color.WHITE);
		labelBreadcrumbNewSimulazione.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbNewSimulazione.setText(Messages.getString("simulazione.nuova.simulazione"));
		
		menuLeft.add(labelBreadcrumbNewSimulazione);
		
		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);
		
		JPanel menuRight = new JPanel() ;
        FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);
		
		
		JPanel bodyCenter = new JPanel();
		bodyCenter.setBackground(Color.WHITE);
		GridBagConstraints gbc_bodyCenter = new GridBagConstraints();
		gbc_bodyCenter.insets = new Insets(10, 10, 10, 10);
		gbc_bodyCenter.fill = GridBagConstraints.BOTH;
		gbc_bodyCenter.gridx = 1;
		gbc_bodyCenter.gridy = 1;
		add(bodyCenter, gbc_bodyCenter);
		GridBagLayout gbl_bodyCenter = new GridBagLayout();
		gbl_bodyCenter.columnWidths = new int[]{0, 0, 0};
		gbl_bodyCenter.rowHeights = new int[]{0, 0, 0};
		gbl_bodyCenter.columnWeights = new double[]{0.3, 0.4, 0.3};
		gbl_bodyCenter.rowWeights = new double[]{0.1 , 0.8, 0.1};
		bodyCenter.setLayout(gbl_bodyCenter);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 0;
		bodyCenter.add(panel, gbc_panel);
		
		JLabel lblNewLabel = new JLabel(Messages.getString("simulazione.scelta.modello"));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		panel.add(lblNewLabel);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(20, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 1;
		bodyCenter.add(panel_1, gbc_panel_1);
		
		
		vModel=setModel();
		JRadioButton rdbtnModel1=null;
		ButtonGroup bg=new ButtonGroup();
		for (String  nameModel : vModel) {
			rdbtnModel1 = new JRadioButton(nameModel);
			rdbtnModel1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			rdbtnModel1.setAlignmentX(0.5f);
			rdbtnModel1.setAlignmentX(Component.LEFT_ALIGNMENT);
			rdbtnModel1.setActionCommand(nameModel.substring(0,nameModel.indexOf(" ")));
			panel_1.add(rdbtnModel1);	
			bg.add(rdbtnModel1);
			rdbtnModel1.addActionListener(new ActionListener() {
		        @Override
		        public void actionPerformed(ActionEvent e) {
		        	setModelSelect(Integer.parseInt(e.getActionCommand())); 
		        	
		        }
		    });
		}
		
		JButton btnNewButton = new JButton(Messages.getString("button.prosegui")); //$NON-NLS-1$
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			  
				//System.out.println("rdbtnModel1"+rdbtnModel1.getText());
				if (getModelSelect()<=0) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.scelta.modello.empty"));
					
				}else {
					switch (getModelSelect()) {
					case 1:
						Home.switchPanel(new PanelBodySimulazioneModel1(""+vModel.get(getModelSelect()-1)));
						break;

					case 2:
						Home.switchPanel(new PanelBodySimulazioneModel2(""+vModel.get(getModelSelect()-1)));
						break;
						
					case 3:
						Home.switchPanel(new PanelBodySimulazioneModel3(""+vModel.get(getModelSelect()-1)));
	
					break;
					
					}
				
				}
			}
		});
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 110, 171) );
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 2;
		bodyCenter.add(btnNewButton, gbc_btnNewButton);
		
	}

	private Vector<String> setModel() {
        Vector<String> result= new Vector<String>(3); 
    	Connection connection =null;
    	ConnectionDB c= new ConnectionDB();
		try {
			connection=c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			Statement stm = connection.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * from sys_model");
			while (rs.next()) {
				result.add(rs.getString("ID_MODEL")+" "+ rs.getString("NAM_MODEL")+" - "+ rs.getString("DES_MODEL"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}
    	

		return result;
	}

	public int getModelSelect() {
		return modelSelect;
	}

	public void setModelSelect(int modelSelect) {
		this.modelSelect = modelSelect;
	}


}
