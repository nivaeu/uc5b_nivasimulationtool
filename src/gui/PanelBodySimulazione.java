package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import bl.ButtonEditor;
import bl.ButtonRenderer;
import bl.ConnectionDB;
import bl.Messages;
import bl.ObjectTable;
import bl.UtilPrint;

public class PanelBodySimulazione extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1099611050517806305L;
	private JTable simulazioni_1;
	private JScrollPane jspTable;
	private JPanel panel_1;

	/**
	 * Create the panel.
	 */
	public PanelBodySimulazione() {
		setBackground(Color.WHITE);
		setName(PanelBodySimulazione.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 0.6, 0.2 };
		gridBagLayout.rowWeights = new double[] { 0.2, 1.0, 0.1 };
		setLayout(gridBagLayout);

		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);

		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);

		ImageIcon iconBreadcrumbSetting = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage();
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance(150, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbSetting = new ImageIcon(imageBreadcrumbSetting);
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setText(Messages.getString("simulazione.simulazione"));

		menuLeft.add(labelBreadcrumbSetting);

		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenter = new JPanel();
		bodyCenter.setBackground(Color.WHITE);
		GridBagConstraints gbc_bodyCenter = new GridBagConstraints();
		gbc_bodyCenter.insets = new Insets(0, 0, 5, 5);
		gbc_bodyCenter.fill = GridBagConstraints.BOTH;
		gbc_bodyCenter.gridx = 0;
		gbc_bodyCenter.gridy = 1;
		gbc_bodyCenter.gridwidth = 3;
		add(bodyCenter, gbc_bodyCenter);
		GridBagLayout gbl_bodyCenter = new GridBagLayout();
		gbl_bodyCenter.columnWidths = new int[] { 0, 0 };
		gbl_bodyCenter.rowHeights = new int[] { 0, 0, 0 };
		gbl_bodyCenter.columnWeights = new double[] { 0.1, 0.9 };
		gbl_bodyCenter.rowWeights = new double[] { 0.1, 0.8, 0.1 };
		bodyCenter.setLayout(gbl_bodyCenter);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 5, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		bodyCenter.add(panel, gbc_panel);

		JButton btnNewButton = new JButton(Messages.getString("button.nuova.simulazione"));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 110, 171));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Home.switchPanel(new PanelBodyNewSimulazione());
			}
		});
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panel.add(btnNewButton);

		JPanel panelRefresh = new JPanel();
		panelRefresh.setBackground(Color.WHITE);
		GridBagConstraints gbc_panelRefresh = new GridBagConstraints();
		gbc_panelRefresh.anchor = GridBagConstraints.EAST;
		gbc_panelRefresh.insets = new Insets(0, 5, 5, 5);
		gbc_panelRefresh.gridx = 1;
		gbc_panelRefresh.gridy = 0;
		bodyCenter.add(panelRefresh, gbc_panelRefresh);

		ImageIcon iconInfo = new ImageIcon(Home.class.getResource("/images/info.png"));
		Image imageInfo = iconInfo.getImage();
		imageInfo = imageInfo.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconInfo = new ImageIcon(imageInfo);

		ImageIcon iconRefresh = new ImageIcon(Home.class.getResource("/images/refresh.png"));
		Image imgRefresh = iconRefresh.getImage();
		imgRefresh = imgRefresh.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconRefresh = new ImageIcon(imgRefresh);

		JButton btnRefresh = new JButton(iconRefresh);
		btnRefresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				refreshTableSimulazioni();
			}
		});
		btnRefresh.setContentAreaFilled(false);
		btnRefresh.setSize(30, 30);
		btnRefresh.setToolTipText(Messages.getString("page.refresh"));
		btnRefresh.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panelRefresh.setLayout(new GridLayout(0, 1, 0, 0));
		panelRefresh.add(btnRefresh);

		panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(5, 5, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		gbc_panel_1.gridwidth = 2;
		bodyCenter.add(panel_1, gbc_panel_1);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));

		simulazioni_1 = getElencoSimulazioni();
		jspTable = new JScrollPane(simulazioni_1);
		panel_1.add(jspTable);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.WEST;
		gbc_panel_2.gridwidth = 2;
		gbc_panel_2.insets = new Insets(0, 5, 0, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 2;
		bodyCenter.add(panel_2, gbc_panel_2);

		JLabel lblLegend = new JLabel(Messages.getString("legend") + ":");
		panel_2.add(lblLegend);

		ImageIcon iconL1 = new ImageIcon(Home.class.getResource("/images/wait1.png"));
		Image imageL1 = iconL1.getImage();
		imageL1 = imageL1.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iconL1 = new ImageIcon(imageL1);
		JLabel labelL1 = new JLabel(iconL1);
		labelL1.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(labelL1);

		ImageIcon iconL2 = new ImageIcon(Home.class.getResource("/images/completed.png"));
		Image imageL2 = iconL2.getImage();
		imageL2 = imageL2.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iconL2 = new ImageIcon(imageL2);

		JLabel lblLegendWait = new JLabel(Messages.getString("simulazione.in.elaborazione"));
		panel_2.add(lblLegendWait);
		JLabel labelL2 = new JLabel(iconL2);
		labelL2.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(labelL2);

		ImageIcon iconL3 = new ImageIcon(Home.class.getResource("/images/error.png"));
		Image imageL3 = iconL3.getImage();
		imageL3 = imageL3.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iconL3 = new ImageIcon(imageL3);

		JLabel lblLegendOK = new JLabel(Messages.getString("simulazione.completata.senza.errori"));
		panel_2.add(lblLegendOK);
		JLabel labelL3 = new JLabel(iconL3);
		labelL3.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(labelL3);

		ImageIcon iconL4 = new ImageIcon(Home.class.getResource("/images/warning.png"));
		Image imageL4 = iconL4.getImage();
		imageL4 = imageL4.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iconL4 = new ImageIcon(imageL4);

		JLabel lblLegendError = new JLabel(Messages.getString("simulazione.completata.con.errori"));
		panel_2.add(lblLegendError);
		JLabel labelL4 = new JLabel(iconL4);
		labelL4.setHorizontalAlignment(SwingConstants.LEFT);
		panel_2.add(labelL4);

		JLabel lblLegendWarn = new JLabel(Messages.getString("simulazione.completata.con.warning"));
		panel_2.add(lblLegendWarn);

	}

	public JTable getElencoSimulazioni() {
		// headers for the table
		String[] columns = new String[] { Messages.getString("simulazione.table.id"),
				Messages.getString("simulazione.table.simulazione"), Messages.getString("simulazione.table.inizio"),
				Messages.getString("simulazione.table.fine"), Messages.getString("simulazione.table.data"),
				Messages.getString("simulazione.table.stato"), Messages.getString("simulazione.table.stato.message"),
				Messages.getString("simulazione.table.statistics"),

		};

		List<ObjectTable> listData = readSimulazioni();
		Object[][] data = new Object[listData.size()][11];
		for (int i = 0; i < listData.size(); i++) {
			data[i][0] = listData.get(i).getsColumn1();
			data[i][1] = listData.get(i).getsColumn2();
			data[i][2] = listData.get(i).getsColumn3();
			data[i][3] = listData.get(i).getsColumn4();
			data[i][4] = listData.get(i).getsColumn5();

			String s = "" + listData.get(i).getsColumn6();
			ImageIcon img = new ImageIcon();
			img.setDescription("empty");
			if (listData.get(i).getiColumn1() == 0) {
				data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/wait.gif")), 20, 20);
			} else {
				switch (s) {
				case "-1":
					data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/error.png")), 20, 20);
//					data[i][7]= new ButtonRenderer("error.png");  // listData.get(i).getsColumn8();
					break;
				case "0":
					data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/warning.png")), 20, 20);
//					data[i][7]= new ButtonRenderer("chart.png");//scaleImage(new ImageIcon(Home.class.getResource("/images/chart.png")), 20 ,20 );    // listData.get(i).getsColumn8();
					break;

				case "1":
					data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/completed.png")), 20, 20);
//					data[i][7]= new ButtonRenderer("chart.png");//scaleImage(new ImageIcon(Home.class.getResource("/images/chart.png")), 20 ,20 );  
					break;
				case "null":
					data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/wait.gif")), 20, 20);
//					data[i][7]= new ButtonRenderer("error.png");  
					break;

				default:
					data[i][5] = scaleImage(new ImageIcon(Home.class.getResource("/images/wait.png")), 20, 20);
//					data[i][7]= new ButtonRenderer("error.png");  
					break;
				}
			}
			// data[i][5]= scaleImage(new
			// ImageIcon(Home.class.getResource("/images/wait.png")), 20 ,20 );
			data[i][7] = "";
			data[i][6] = listData.get(i).getsColumn7();
			data[i][8] = listData.get(i).getsColumn9();
			data[i][9] = s;
			data[i][10] = listData.get(i).getiColumn1();

		}

		DefaultTableModel model = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 3026474122596090425L;

			public int getColumnCount() {
				return columns.length;
			}

			public int getRowCount() {
				return data.length;
			}

			public String getColumnName(int col) {
				return (String) columns[col];
			}

			public Object getValueAt(int row, int col) {
				return data[row][col];
			}

			public Class<?> getColumnClass(int col) {
				switch (col) {
				case 5:
					return ImageIcon.class;

				case 7:
					return ButtonRenderer.class;

				default:
					return String.class;

				}

			}
		};

		// simulazioni_1= new JTable(data, columns);
		simulazioni_1 = new JTable(model) {
			private static final long serialVersionUID = 1L;

			@Override
			public Class getColumnClass(int column) {
				try {
					return getValueAt(0, column).getClass();
				} catch (Exception e) {
					return null;
				}

			}

		};
		simulazioni_1.setRowHeight(30);
		setImageObserver(simulazioni_1);
		simulazioni_1.setBackground(Color.WHITE);
		// simulazioni.setTableHeader(null);

		simulazioni_1.getColumnModel().getColumn(0).setPreferredWidth(100);
		simulazioni_1.getColumnModel().getColumn(0).setMaxWidth(100);

		simulazioni_1.getColumnModel().getColumn(1).setPreferredWidth(150);
		simulazioni_1.getColumnModel().getColumn(1).setMaxWidth(150);

		simulazioni_1.getColumnModel().getColumn(2).setMaxWidth(150);
		simulazioni_1.getColumnModel().getColumn(2).setPreferredWidth(150);
		simulazioni_1.getColumnModel().getColumn(3).setMaxWidth(150);
		simulazioni_1.getColumnModel().getColumn(3).setPreferredWidth(150);
		simulazioni_1.getColumnModel().getColumn(5).setPreferredWidth(100);
		simulazioni_1.getColumnModel().getColumn(5).setMaxWidth(100);

//		simulazioni_1.getColumnModel().getColumn(5).setCellRenderer(new IconRenderer("wait.png"));
		simulazioni_1.getColumnModel().getColumn(5).setMaxWidth(90);

		// SET CUSTOM EDITOR TO TEAMS COLUMN
		simulazioni_1.getColumnModel().getColumn(7).setCellRenderer(new ButtonRenderer("chart.png"));
		simulazioni_1.getColumnModel().getColumn(7).setCellEditor(new ButtonEditor(new JTextField()));
		simulazioni_1.getColumnModel().getColumn(7).setMaxWidth(50);

		simulazioni_1.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		int gapWidth = 5;
		int gapHeight = 3;
		simulazioni_1.setIntercellSpacing(new Dimension(gapWidth, gapHeight));

		return simulazioni_1;
	}

	private void setImageObserver(JTable table) {
		TableModel model = table.getModel();
		int colCount = model.getColumnCount();
		int rowCount = model.getRowCount();
		for (int col = 0; col < colCount; col++) {
			if (ImageIcon.class == model.getColumnClass(col)) {
				for (int row = 0; row < rowCount; row++) {
					ImageIcon icon = (ImageIcon) model.getValueAt(row, col);
					if (icon != null) {
						icon.setImageObserver(new CellImageObserver(table, row, col));
					}
				}
			}
		}
	}

	class CellImageObserver implements ImageObserver {
		JTable table;
		int row;
		int col;

		CellImageObserver(JTable table, int row, int col) {
			this.table = table;
			this.row = row;
			this.col = col;
		}

		public boolean imageUpdate(Image img, int flags, int x, int y, int w, int h) {
			if ((flags & (FRAMEBITS | ALLBITS)) != 0) {
				Rectangle rect = table.getCellRect(row, col, false);
				table.repaint(rect);
			}
			return (flags & (ALLBITS | ABORT)) == 0;
		}
	}

	public ImageIcon scaleImage(ImageIcon icon, int w, int h) {
		int nw = icon.getIconWidth();
		int nh = icon.getIconHeight();

		if (icon.getIconWidth() > w) {
			nw = w;
			nh = (nw * icon.getIconHeight()) / icon.getIconWidth();
		}

		if (nh > h) {
			nh = h;
			nw = (icon.getIconWidth() * nh) / icon.getIconHeight();
		}

		return new ImageIcon(icon.getImage().getScaledInstance(nw, nh, Image.SCALE_DEFAULT));
	}

	private static List<ObjectTable> readSimulazioni() {
		List<ObjectTable> data = new ArrayList<ObjectTable>();

		Connection connection = null;
		Statement stm = null;
		ResultSet rs = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			if (connection != null) {
				stm = connection.createStatement();
				rs = stm.executeQuery("SELECT " + " SME.id_model_execution, " + " SME.id_model,  "
						+ " SME.dat_model_execution, " + " SME.nam_model_execution,  " + " SME.des_model_execution,  "
						+ " SME.dat_start,  " + " SME.dat_end,  " + " SME.flg_model_execution_status, "
						+ " CASE WHEN SSJ.job_status_type IN (0 , 1) THEN "
						+ " CASE WHEN SME.id_model=1 THEN \'PYH_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYH_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
						+ " WHEN SME.id_model=2 THEN \'PYR_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYR_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
						+ " WHEN SME.id_model=3 THEN \'PYE_FD_\'||SME.id_model_execution::VARCHAR(10)||\'; PYE_MD_\'||SME.id_model_execution::VARCHAR(10)||\';\' "
						+ " END " + "  ELSE NULL END DATA_TABLES," + " SSJ.id_status_job,  " + " SSJ.id_job,  "
						+ " SSJ.nam_job,  " + " SSJ.dat_exec_job, " + " SSJ.job_status_type, " + " SSJ.job_status, "
						+ " SJL.log_field " + " FROM sys_model_execution SME " + " LEFT JOIN sys_status_job SSJ "
						+ " ON SME.dat_model_execution=SSJ.dat_exec_job " + " LEFT JOIN sys_job_log SJL "
						+ " ON SSJ.id_job=SJL.id_job " + "ORDER BY id_model_execution DESC");
				ObjectTable ot;
				while (rs.next()) {
					ot = new ObjectTable();
					ot.setsColumn1(rs.getString("id_model_execution"));
					ot.setsColumn2(rs.getString("nam_model_execution"));
					ot.setsColumn3(rs.getString("dat_start"));
					ot.setsColumn4(rs.getString("dat_end") == null ? "" : rs.getString("dat_end"));
					ot.setsColumn5(rs.getString("DATA_TABLES") == null ? "" : rs.getString("DATA_TABLES"));
					ot.setsColumn6(rs.getString("job_status_type") == null ? "" : rs.getString("job_status_type"));
					ot.setsColumn7(rs.getString("job_status") == null ? "" : rs.getString("job_status"));
					ot.setsColumn8("");
					ot.setsColumn9(rs.getString("id_model"));
					ot.setiColumn1(rs.getInt("flg_model_execution_status"));
					data.add(ot);
				}

			} else {
				System.out.println("connessione null");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				stm.close();
				rs.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return data;
	}

	private void refreshTableSimulazioni() {
		Home.switchPanel(new PanelBodySimulazione());
	}

}
