package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import bl.ConnectionDB;
import bl.Messages;

public class PanelBodyHome extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2116246985117309104L;
	private static JLabel lblNewLabel_1;
	private static JLabel lblNewLabel;
	private static JLabel labelHome;
	private static JLabel labelSetting;
	private static JButton btnEntra;

	/**
	 * Create the panel.
	 */
	public PanelBodyHome() {
		setBackground(Color.WHITE);
		setName(PanelBodyHome.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.2, 0.6, 0.2 };
		gridBagLayout.rowWeights = new double[] { 0.2, 0.7, 0.1 };
		setLayout(gridBagLayout);

		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);
		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);
		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		add(menuLeft, gbc_menuLeft);

		JPanel menuCenter = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) menuCenter.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		menuCenter.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuCenter = new GridBagConstraints();
		gbc_menuCenter.insets = new Insets(0, 0, 5, 5);
		gbc_menuCenter.fill = GridBagConstraints.BOTH;
		gbc_menuCenter.gridx = 1;
		gbc_menuCenter.gridy = 0;
		add(menuCenter, gbc_menuCenter);

		ImageIcon iconSetting = new ImageIcon(Home.class.getResource("/images/setting.png"));
		Image imgSetting = iconSetting.getImage();
		imgSetting = imgSetting.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconSetting = new ImageIcon(imgSetting);

		JButton btnSetting = new JButton(iconSetting);
		btnSetting.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodySetting());
			}
		});
		btnSetting.setContentAreaFilled(false);
		btnSetting.setSize(30, 30);
		btnSetting.setToolTipText(Messages.getString("page.setting"));
		btnSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setHgap(15);
		flowLayout_2.setAlignment(FlowLayout.RIGHT);
		menuRight.setBackground(Color.WHITE);
		menuRight.add(btnSetting);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenter = new JPanel();
		bodyCenter.setLayout(new BoxLayout(bodyCenter, BoxLayout.Y_AXIS));
		bodyCenter.setBackground(Color.WHITE);
		bodyCenter.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(76, 144, 33)));
		GridBagConstraints gbc_bodyCenter = new GridBagConstraints();
		gbc_bodyCenter.insets = new Insets(5, 5, 5, 5);
		gbc_bodyCenter.fill = GridBagConstraints.BOTH;
		gbc_bodyCenter.gridx = 1;
		gbc_bodyCenter.gridy = 1;
		add(bodyCenter, gbc_bodyCenter);

		lblNewLabel = new JLabel(Messages.getString("niva.smt.info"));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBorder(new EmptyBorder(5, 5, 0, 0));
		bodyCenter.add(lblNewLabel);

		JPanel bodyFooter = new JPanel();
		FlowLayout flowLayout_4 = (FlowLayout) bodyFooter.getLayout();
		flowLayout_4.setVgap(0);
		bodyFooter.setBackground(Color.WHITE);
		GridBagConstraints gbc_bodyFooter = new GridBagConstraints();
		gbc_bodyFooter.insets = new Insets(0, 0, 0, 5);
		gbc_bodyFooter.fill = GridBagConstraints.BOTH;
		gbc_bodyFooter.gridx = 1;
		gbc_bodyFooter.gridy = 2;
		add(bodyFooter, gbc_bodyFooter);

		ImageIcon iconEntra = new ImageIcon(Home.class.getResource("/images/entra.png"));
		Image img = iconEntra.getImage();
		Image imgEntra = img.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		iconEntra = new ImageIcon(imgEntra);
		btnEntra = new JButton(Messages.getString("button.entra")); //$NON-NLS-1$
		btnEntra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				entraAction();
			}
		});
		btnEntra.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnEntra.setForeground(Color.WHITE);
		btnEntra.setBackground(new Color(0, 110, 171));
		btnEntra.setIcon(iconEntra);
		btnEntra.setToolTipText(Messages.getString("button.entra"));
		btnEntra.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bodyFooter.add(btnEntra);

	}

	private void entraAction() {

		boolean result = true;
		ConnectionDB c = new ConnectionDB();

		if (!c.verificaDatabaseETabelle()) {
			JOptionPane.showMessageDialog(null, Messages.getString("setting.error.connessione"));
			Home.switchPanel(new PanelBodySetting());
		} else {
			Home.switchPanel(new PanelBodySimulazione());
		}

	}

	public static void updateLabel() {
		lblNewLabel_1.setText(Messages.getString("niva.smt"));
		lblNewLabel.setText(Messages.getString("niva.smt.info"));
		btnEntra.setText(Messages.getString("button.entra"));
		btnEntra.setToolTipText(Messages.getString("button.entra"));
	}

}
