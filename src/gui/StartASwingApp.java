package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class StartASwingApp extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1067942665233851446L;
	JButton close = new JButton("Close");

	public StartASwingApp() {
		getContentPane().add(close, BorderLayout.CENTER);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0); // closes the application
			}
		});

		setVisible(true);
	}
}