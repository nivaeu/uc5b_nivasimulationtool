package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import bl.ConfigurationProperties;
import bl.ConnectionDB;
import bl.Messages;
import bl.UtilFile;
import bl.UtilKettle;
import bl.UtilPrint;

public class PanelBodySimulazioneModel2 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7647813675306805072L;
	private JTextField txtPathFile;
	private JFormattedTextField txtAMTHA23;
	private JFormattedTextField txtAMTHA24;
	private JFormattedTextField txtAMTHA25;
	private JFormattedTextField txtAMTHA26;
	private JFormattedTextField txtNunMinSupLEvel;
	private JFormattedTextField txtPCTCRISS23;
	private JFormattedTextField txtPCTCRISS24;
	private JFormattedTextField txtPCTCRISS25;
	private JFormattedTextField txtPCTCRISS26;
	private String dataDB;
	private JFormattedTextField txtRang23_1DA;
	private JFormattedTextField txtRang23_1A;
	private JFormattedTextField txtRang23_1BISS;

	private JFormattedTextField txtRang23_2DA;
	private JFormattedTextField txtRang23_2A;
	private JFormattedTextField txtRang23_2BISS;

	private JFormattedTextField txtRang23_3DA;
	private JFormattedTextField txtRang23_3A;
	private JFormattedTextField txtRang23_3BISS;

	private JFormattedTextField txtRang24_1DA;
	private JFormattedTextField txtRang24_1A;
	private JFormattedTextField txtRang24_1BISS;

	private JFormattedTextField txtRang24_2DA;
	private JFormattedTextField txtRang24_2A;
	private JFormattedTextField txtRang24_2BISS;

	private JFormattedTextField txtRang24_3DA;
	private JFormattedTextField txtRang24_3A;
	private JFormattedTextField txtRang24_3BISS;

	private JFormattedTextField txtRang25_1DA;
	private JFormattedTextField txtRang25_1A;
	private JFormattedTextField txtRang25_1BISS;

	private JFormattedTextField txtRang25_2DA;
	private JFormattedTextField txtRang25_2A;
	private JFormattedTextField txtRang25_2BISS;

	private JFormattedTextField txtRang25_3DA;
	private JFormattedTextField txtRang25_3A;
	private JFormattedTextField txtRang25_3BISS;

	private JFormattedTextField txtRang26_1DA;
	private JFormattedTextField txtRang26_1A;
	private JFormattedTextField txtRang26_1BISS;

	private JFormattedTextField txtRang26_2DA;
	private JFormattedTextField txtRang26_2A;
	private JFormattedTextField txtRang26_2BISS;

	private JFormattedTextField txtRang26_3DA;
	private JFormattedTextField txtRang26_3A;
	private JFormattedTextField txtRang26_3BISS;
	private JComboBox jcbFlgOnlyPure;

	/**
	 * Create the panel.
	 */
	public PanelBodySimulazioneModel2(String modelTitle) {
		setBackground(Color.WHITE);
		setName(PanelBodySimulazioneModel1.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.2, 0.6, 0.2 };
		gridBagLayout.rowWeights = new double[] { 0.2, 0.7, 0.1 };
		setLayout(gridBagLayout);

		NumberFormat numberFormatRange = NumberFormat.getNumberInstance();
		numberFormatRange.setMaximumFractionDigits(0);
		numberFormatRange.setMaximumFractionDigits(4);
		numberFormatRange.setRoundingMode(RoundingMode.HALF_UP);

		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);

		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);

		ImageIcon iconBreadcrumbSetting = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage();
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance(150, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbSetting = new ImageIcon(imageBreadcrumbSetting);
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setText(Messages.getString("simulazione.simulazione"));
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodySimulazione());
			}
		});
		labelBreadcrumbSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbSetting);

		ImageIcon iconBreadcrumbNewSimulazione = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbNewSimulazione = iconBreadcrumbNewSimulazione.getImage();
		imageBreadcrumbNewSimulazione = imageBreadcrumbNewSimulazione.getScaledInstance(180, 30,
				java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbNewSimulazione = new ImageIcon(imageBreadcrumbNewSimulazione);
		JLabel labelBreadcrumbNewSimulazione = new JLabel(iconBreadcrumbNewSimulazione);
		labelBreadcrumbNewSimulazione.setForeground(Color.WHITE);
		labelBreadcrumbNewSimulazione.setText(Messages.getString("simulazione.nuova.simulazione"));
		labelBreadcrumbNewSimulazione.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbNewSimulazione.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyNewSimulazione());
			}

		});
		labelBreadcrumbNewSimulazione.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbNewSimulazione);

		ImageIcon iconBreadcrumbModello1 = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbModello1 = iconBreadcrumbModello1.getImage();
		imageBreadcrumbModello1 = imageBreadcrumbModello1.getScaledInstance(380, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbModello1 = new ImageIcon(imageBreadcrumbModello1);
		JLabel labelBreadcrumbModello1 = new JLabel(iconBreadcrumbModello1);
		labelBreadcrumbModello1.setForeground(Color.WHITE);
		labelBreadcrumbModello1.setText(modelTitle.substring(0, modelTitle.indexOf("-")));
		labelBreadcrumbModello1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		menuLeft.add(labelBreadcrumbModello1);

		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenterLeft = new JPanel();
		bodyCenterLeft.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_1_1 = new GridBagConstraints();
		gbc_panel_1_1.insets = new Insets(10, 10, 10, 10);
		gbc_panel_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1_1.gridx = 0;
		gbc_panel_1_1.gridy = 1;
		gbc_panel_1_1.gridwidth = 3;
		// bodyCenter.add(bodyCenterLeft, gbc_panel_1_1);
		add(bodyCenterLeft, gbc_panel_1_1);
		GridBagLayout gbl_bodyCenterLeft = new GridBagLayout();
		gbl_bodyCenterLeft.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.columnWeights = new double[] { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1 };
		gbl_bodyCenterLeft.rowWeights = new double[] { 0.1, 0.3, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1,
				0.1 };
		bodyCenterLeft.setLayout(gbl_bodyCenterLeft);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		gbc_panel.gridwidth = 12;
		bodyCenterLeft.add(panel, gbc_panel);

		JLabel lblNewLabel = new JLabel(Messages.getString("simulazione.seleziona.file"));
		panel.add(lblNewLabel);

		txtPathFile = new JTextField();
		txtPathFile.setEditable(false);
		txtPathFile.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_txtPathFile = new GridBagConstraints();
		gbc_txtPathFile.insets = new Insets(0, 0, 5, 5);
		gbc_txtPathFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPathFile.gridx = 0;
		gbc_txtPathFile.gridy = 1;
		gbc_txtPathFile.gridwidth = 8;
		bodyCenterLeft.add(txtPathFile, gbc_txtPathFile);
		txtPathFile.setColumns(10);

		JButton btnNewButton = new JButton(Messages.getString("button.sfoglia"));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 110, 171));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV FILES", "csv", "csv");
				fileChooser.setFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog((Component) e.getSource());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						txtPathFile.setText(file.toString());
					} catch (Exception ex) {
						System.out.println("problem accessing file" + file.getAbsolutePath());
					}
				} else {
					// simulazione.seleziona.file.empty
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.seleziona.file.empty"));
				}

			}

		});
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 8;
		gbc_btnNewButton.gridy = 1;
		gbc_btnNewButton.gridwidth = 4;
		bodyCenterLeft.add(btnNewButton, gbc_btnNewButton);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_3 = (FlowLayout) panel_1.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		gbc_panel_1.gridwidth = 12;
		bodyCenterLeft.add(panel_1, gbc_panel_1);

		JLabel lblNewLabel_1 = new JLabel(Messages.getString("simulazione.scelta.parametri"));
		panel_1.add(lblNewLabel_1);

		JLabel lblNunMinSupLEvel = new JLabel(Messages.getString("param.m2.num_min_sup_level") + "*"); //$NON-NLS-1$
		lblNunMinSupLEvel.setToolTipText(Messages.getString("param.m2.num_min_sup_level.title"));
		GridBagConstraints gbc_lblNunMinSupLEvel = new GridBagConstraints();
		gbc_lblNunMinSupLEvel.anchor = GridBagConstraints.EAST;
		gbc_lblNunMinSupLEvel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNunMinSupLEvel.gridx = 0;
		gbc_lblNunMinSupLEvel.gridy = 4;
//		  gbc_lblNunMinSupLEvel.gridwidth=2;
		bodyCenterLeft.add(lblNunMinSupLEvel, gbc_lblNunMinSupLEvel);

		txtNunMinSupLEvel = new JFormattedTextField(numberFormatRange);
		txtNunMinSupLEvel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtNunMinSupLEvel.getText().isEmpty()) {
					txtNunMinSupLEvel.setValue(null);

				}
			}
		});
		txtNunMinSupLEvel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtNunMinSupLEvel.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtNunMinSupLEvel.getText().isEmpty() && (txtNunMinSupLEvel.getText().length() >= 18
						|| txtNunMinSupLEvel.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtNunMinSupLEvel = new GridBagConstraints();
		gbc_txtNunMinSupLEvel.insets = new Insets(0, 0, 5, 5);
		gbc_txtNunMinSupLEvel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNunMinSupLEvel.gridx = 1;
		gbc_txtNunMinSupLEvel.gridy = 4;
		gbc_txtNunMinSupLEvel.gridwidth = 2;
		bodyCenterLeft.add(txtNunMinSupLEvel, gbc_txtNunMinSupLEvel);

		JLabel lblFlgOnlyPure = new JLabel(Messages.getString("param.m2.param.m2.flg_only_pure") + "*");
		lblFlgOnlyPure.setToolTipText(Messages.getString("param.m2.param.m2.flg_only_pure.title"));
		GridBagConstraints gbc_lblFlgOnlyPure = new GridBagConstraints();
		gbc_lblFlgOnlyPure.anchor = GridBagConstraints.EAST;
		gbc_lblFlgOnlyPure.insets = new Insets(0, 0, 5, 5);
		gbc_lblFlgOnlyPure.gridx = 6;
		gbc_lblFlgOnlyPure.gridy = 4;
		gbc_lblFlgOnlyPure.gridwidth = 1;
		bodyCenterLeft.add(lblFlgOnlyPure, gbc_lblFlgOnlyPure);

		jcbFlgOnlyPure = new JComboBox();
		jcbFlgOnlyPure.addItem(Messages.getString("select.si"));
		jcbFlgOnlyPure.addItem(Messages.getString("select.no"));
		GridBagConstraints gbc_jcbFlgOnlyPure = new GridBagConstraints();
		gbc_jcbFlgOnlyPure.gridwidth = 2;
		gbc_jcbFlgOnlyPure.insets = new Insets(0, 0, 5, 5);
		gbc_jcbFlgOnlyPure.fill = GridBagConstraints.HORIZONTAL;
		gbc_jcbFlgOnlyPure.gridx = 7;
		gbc_jcbFlgOnlyPure.gridy = 4;
		bodyCenterLeft.add(jcbFlgOnlyPure, gbc_jcbFlgOnlyPure);

		JLabel lblAMTHA23 = new JLabel(Messages.getString("param.m2.amt_ha_23") + "*");
		lblAMTHA23.setToolTipText(Messages.getString("param.m2.amt_ha_23.title"));
		GridBagConstraints gbc_lblAMTHA23 = new GridBagConstraints();
		gbc_lblAMTHA23.anchor = GridBagConstraints.EAST;
		gbc_lblAMTHA23.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTHA23.gridx = 0;
		gbc_lblAMTHA23.gridy = 5;
		bodyCenterLeft.add(lblAMTHA23, gbc_lblAMTHA23);

		txtAMTHA23 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMTHA23.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMTHA23.getText().isEmpty()) {
					txtAMTHA23.setValue(null);
				}
			}
		});
		txtAMTHA23.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMTHA23.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMTHA23.getText().isEmpty()
						&& (txtAMTHA23.getText().length() >= 18 || txtAMTHA23.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
					txtAMTHA23.requestFocus();
				}
			}
		});

		GridBagConstraints gbc_txtAMTHA23 = new GridBagConstraints();
		gbc_txtAMTHA23.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMTHA23.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMTHA23.gridx = 1;
		gbc_txtAMTHA23.gridy = 5;
		gbc_txtAMTHA23.gridwidth = 2;
		bodyCenterLeft.add(txtAMTHA23, gbc_txtAMTHA23);
		
		/*TaG****************/
		JLabel lblPCTCriss23 = new JLabel(Messages.getString("param.m2.pct_criss_23") + "*");
		lblPCTCriss23.setToolTipText(Messages.getString("param.m2.pct_criss_23.title"));
		GridBagConstraints gbc_lblPCTCriss23  = new GridBagConstraints();
		gbc_lblPCTCriss23.anchor = GridBagConstraints.EAST;
		gbc_lblPCTCriss23.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTCriss23.gridx = 3;
		gbc_lblPCTCriss23.gridy = 5;
		bodyCenterLeft.add(lblPCTCriss23, gbc_lblPCTCriss23);

		txtPCTCRISS23 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTCRISS23.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTCRISS23.getText().isEmpty()) {
					txtPCTCRISS23.setValue(null);
				}
			}
		});
		txtPCTCRISS23.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCTCRISS23.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCTCRISS23.getText().isEmpty()
						&& (txtPCTCRISS23.getText().length() >= 18 || txtPCTCRISS23.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
					txtPCTCRISS23.requestFocus();
				}
			}
		});

		GridBagConstraints gbc_txtPCTCRISS23 = new GridBagConstraints();
		gbc_txtPCTCRISS23.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTCRISS23.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTCRISS23.gridx = 4;
		gbc_txtPCTCRISS23.gridy = 5;
		bodyCenterLeft.add(txtPCTCRISS23, gbc_txtPCTCRISS23);
		
		JLabel lblNewLabelPCTCRISS23 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabelPCTCRISS23 = new GridBagConstraints();
		gbc_lblNewLabelPCTCRISS23.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabelPCTCRISS23.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabelPCTCRISS23.gridx = 5;
		gbc_lblNewLabelPCTCRISS23.gridy = 5;
		bodyCenterLeft.add(lblNewLabelPCTCRISS23, gbc_lblNewLabelPCTCRISS23);
		
		/******************/
		

		JLabel lblAMTHA24 = new JLabel(Messages.getString("param.m2.amt_ha_24") + "*");
		lblAMTHA24.setToolTipText(Messages.getString("param.m2.amt_ha_24.title"));
		GridBagConstraints gbc_lblAMTHA24 = new GridBagConstraints();
		gbc_lblAMTHA24.anchor = GridBagConstraints.EAST;
		gbc_lblAMTHA24.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTHA24.gridx = 6;
		gbc_lblAMTHA24.gridy = 5;
		bodyCenterLeft.add(lblAMTHA24, gbc_lblAMTHA24);

		txtAMTHA24 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMTHA24.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMTHA24.getText().isEmpty()) {
					txtAMTHA24.setValue(null);
				}
			}
		});
		txtAMTHA24.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMTHA24.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMTHA24.getText().isEmpty()
						&& (txtAMTHA24.getText().length() >= 18 || txtAMTHA24.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtAMTHA24 = new GridBagConstraints();
		gbc_txtAMTHA24.gridwidth = 2;
		gbc_txtAMTHA24.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMTHA24.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMTHA24.gridx = 7;
		gbc_txtAMTHA24.gridy = 5;
		bodyCenterLeft.add(txtAMTHA24, gbc_txtAMTHA24);
		
		
		JLabel lblPCTCriss24 = new JLabel(Messages.getString("param.m2.pct_criss_24") + "*");
		lblPCTCriss24.setToolTipText(Messages.getString("param.m2.pct_criss_24.title"));
		GridBagConstraints gbc_lblPCTCriss24  = new GridBagConstraints();
		gbc_lblPCTCriss24.anchor = GridBagConstraints.EAST;
		gbc_lblPCTCriss24.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTCriss24.gridx = 9;
		gbc_lblPCTCriss24.gridy = 5;
		bodyCenterLeft.add(lblPCTCriss24, gbc_lblPCTCriss24);

		txtPCTCRISS24 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTCRISS24.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTCRISS24.getText().isEmpty()) {
					txtPCTCRISS24.setValue(null);
				}
			}
		});
		txtPCTCRISS24.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCTCRISS24.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCTCRISS24.getText().isEmpty()
						&& (txtPCTCRISS24.getText().length() >= 18 || txtPCTCRISS24.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
					txtPCTCRISS24.requestFocus();
				}
			}
		});

		GridBagConstraints gbc_txtPCTCRISS24 = new GridBagConstraints();
		gbc_txtPCTCRISS24.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTCRISS24.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTCRISS24.gridx = 10;
		gbc_txtPCTCRISS24.gridy = 5;
		bodyCenterLeft.add(txtPCTCRISS24, gbc_txtPCTCRISS24);
		
		JLabel lblNewLabelPCTCRISS24 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabelPCTCRISS24 = new GridBagConstraints();
		gbc_lblNewLabelPCTCRISS24.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabelPCTCRISS24.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabelPCTCRISS24.gridx = 11;
		gbc_lblNewLabelPCTCRISS24.gridy = 5;
		bodyCenterLeft.add(lblNewLabelPCTCRISS24, gbc_lblNewLabelPCTCRISS24);
		

		JLabel lblRang23_1 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_23_x1"));
		lblRang23_1.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_23_x1.title"));
		GridBagConstraints gbc_lblRang23_1 = new GridBagConstraints();
		gbc_lblRang23_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_1.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_1.gridx = 0;
		gbc_lblRang23_1.gridy = 6;
		bodyCenterLeft.add(lblRang23_1, gbc_lblRang23_1);

		txtRang23_1DA = new JFormattedTextField(numberFormatRange);
		txtRang23_1DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_1DA.getText().isEmpty()) {
					txtRang23_1DA.setValue(null);
					txtRang23_1A.setValue(null);
					txtRang23_1BISS.setValue(null);
				}
			}
		});
		txtRang23_1DA.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (!txtRang23_1DA.getText().isEmpty()
						&& (txtRang23_1DA.getText().length() >= 18 || txtRang23_1DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});
		txtRang23_1DA.setFont(new Font("Tahoma", Font.PLAIN, 18));

		GridBagConstraints gbc_txtRang23_1DA = new GridBagConstraints();
		gbc_txtRang23_1DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_1DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_1DA.gridx = 1;
		gbc_txtRang23_1DA.gridy = 6;
		bodyCenterLeft.add(txtRang23_1DA, gbc_txtRang23_1DA);

		txtRang23_1A = new JFormattedTextField(numberFormatRange);
		txtRang23_1A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_1A.getText().isEmpty()) {
					txtRang23_1A.setValue(null);
					txtRang23_1BISS.setValue(null);
				} else {
					if (txtRang23_1DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang23_1DA.requestFocusInWindow();
						return;
					}

					if (!txtRang23_1DA.getText().isEmpty()
							&& Double.parseDouble(txtRang23_1A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang23_1DA.getText().replace(".", "").replace(",", "."))) {
						txtRang23_1A.setValue(null);
						txtRang23_1BISS.setValue(null);
						txtRang23_1A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}
				}
			}
		});
		txtRang23_1A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_1A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_1A.getText().isEmpty()
						&& (txtRang23_1A.getText().length() >= 18 || txtRang23_1A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_1A = new GridBagConstraints();
		gbc_txtRang23_1A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_1A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_1A.gridx = 2;
		gbc_txtRang23_1A.gridy = 6;
		bodyCenterLeft.add(txtRang23_1A, gbc_txtRang23_1A);

		JLabel lblRang23_1BISS = new JLabel(Messages.getString("param.m2.pct_biss_23_x1"));
		lblRang23_1BISS.setToolTipText(Messages.getString("param.m2.pct_biss_23_x1.title"));
		GridBagConstraints gbc_lblRang23_1BISS = new GridBagConstraints();
		gbc_lblRang23_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_1BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_1BISS.gridx = 3;
		gbc_lblRang23_1BISS.gridy = 6;
		bodyCenterLeft.add(lblRang23_1BISS, gbc_lblRang23_1BISS);

		txtRang23_1BISS = new JFormattedTextField(numberFormatRange);
		txtRang23_1BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_1BISS.getText().isEmpty()) {
					txtRang23_1BISS.setValue(null);

				}

				if (!txtRang23_1BISS.getText().isEmpty()
						&& (txtRang23_1DA.getText().isEmpty() || txtRang23_1A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_23_x1"));
					txtRang23_1BISS.setValue(null);

				}

				if (!txtRang23_1DA.getText().isEmpty() && !txtRang23_1A.getText().isEmpty()
						&& txtRang23_1BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang23_1BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang23_1BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_1BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_1BISS.getText().isEmpty() && (txtRang23_1BISS.getText().length() >= 18
						|| txtRang23_1BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_1BISS = new GridBagConstraints();
		gbc_txtRang23_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_1BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_1BISS.gridx = 4;
		gbc_txtRang23_1BISS.gridy = 6;
		bodyCenterLeft.add(txtRang23_1BISS, gbc_txtRang23_1BISS);

		JLabel lblNewLabel_2 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 5;
		gbc_lblNewLabel_2.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel lblRang24_1 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_24_x1"));
		lblRang24_1.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_24_x1.title"));
		GridBagConstraints gbc_lblRang24_1 = new GridBagConstraints();
		gbc_lblRang24_1.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_1.gridx = 6;
		gbc_lblRang24_1.gridy = 6;
		bodyCenterLeft.add(lblRang24_1, gbc_lblRang24_1);

		txtRang24_1DA = new JFormattedTextField(numberFormatRange);
		txtRang24_1DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_1DA.getText().isEmpty()) {
					txtRang24_1DA.setValue(null);
					txtRang24_1A.setValue(null);
					txtRang24_1BISS.setValue(null);
				}
			}
		});
		txtRang24_1DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_1DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_1DA.getText().isEmpty()
						&& (txtRang24_1DA.getText().length() >= 18 || txtRang24_1DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_1DA = new GridBagConstraints();
		gbc_txtRang24_1DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_1DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_1DA.gridx = 7;
		gbc_txtRang24_1DA.gridy = 6;
		bodyCenterLeft.add(txtRang24_1DA, gbc_txtRang24_1DA);

		txtRang24_1A = new JFormattedTextField(numberFormatRange);
		txtRang24_1A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_1A.getText().isEmpty()) {
					txtRang24_1A.setValue(null);
					txtRang24_1BISS.setValue(null);
				} else {
					if (txtRang24_1DA.getText().isEmpty()) {
						txtRang24_1DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));

						return;
					}

					if (!txtRang24_1DA.getText().isEmpty()
							&& Double.parseDouble(txtRang24_1A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang24_1DA.getText().replace(".", "").replace(",", "."))) {
						txtRang24_1A.setValue(null);
						txtRang24_1BISS.setValue(null);
						txtRang24_1A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}
				}
			}
		});
		txtRang24_1A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_1A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_1A.getText().isEmpty()
						&& (txtRang24_1A.getText().length() >= 18 || txtRang24_1A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_1A = new GridBagConstraints();
		gbc_txtRang24_1A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_1A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_1A.gridx = 8;
		gbc_txtRang24_1A.gridy = 6;
		bodyCenterLeft.add(txtRang24_1A, gbc_txtRang24_1A);

		JLabel lblRang24_1BISS = new JLabel(Messages.getString("param.m2.pct_biss_24_x1"));
		lblRang24_1BISS.setToolTipText(Messages.getString("param.m2.pct_biss_24_x1.title"));
		GridBagConstraints gbc_lblRang24_1BISS = new GridBagConstraints();
		gbc_lblRang24_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_1BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_1BISS.gridx = 9;
		gbc_lblRang24_1BISS.gridy = 6;
		bodyCenterLeft.add(lblRang24_1BISS, gbc_lblRang24_1BISS);

		txtRang24_1BISS = new JFormattedTextField(numberFormatRange);
		txtRang24_1BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_1BISS.getText().isEmpty()) {
					txtRang24_1BISS.setValue(null);

				}
				if (!txtRang24_1BISS.getText().isEmpty()
						&& (txtRang24_1DA.getText().isEmpty() || txtRang24_1A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_24_x1"));
					txtRang24_1BISS.setValue(null);
				}

				if (!txtRang24_1DA.getText().isEmpty() && !txtRang24_1A.getText().isEmpty()
						&& txtRang24_1BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang24_1BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang24_1BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_1BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_1BISS.getText().isEmpty() && (txtRang24_1BISS.getText().length() >= 18
						|| txtRang24_1BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_1BISS = new GridBagConstraints();
		gbc_txtRang24_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_1BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_1BISS.gridx = 10;
		gbc_txtRang24_1BISS.gridy = 6;
		bodyCenterLeft.add(txtRang24_1BISS, gbc_txtRang24_1BISS);

		JLabel lblNewLabel_2_1 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1 = new GridBagConstraints();
		gbc_lblNewLabel_2_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1.gridx = 11;
		gbc_lblNewLabel_2_1.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_2_1, gbc_lblNewLabel_2_1);

		JLabel lblRang23_2 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_23_x2"));
		lblRang23_2.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_23_x2.title"));
		GridBagConstraints gbc_lblRang23_2 = new GridBagConstraints();
		gbc_lblRang23_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_2.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_2.gridx = 0;
		gbc_lblRang23_2.gridy = 7;
		bodyCenterLeft.add(lblRang23_2, gbc_lblRang23_2);

		txtRang23_2DA = new JFormattedTextField(numberFormatRange);
		txtRang23_2DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_2DA.getText().isEmpty()) {
					txtRang23_2DA.setValue(null);
					txtRang23_2A.setValue(null);
					txtRang23_2BISS.setValue(null);

				} else {

					if (!txtRang23_1A.getText().isEmpty()
							&& Double.parseDouble(txtRang23_2DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang23_1A.getText().replace(".", "").replace(",", "."))) {
						txtRang23_2DA.setValue(null);
						txtRang23_2A.setValue(null);
						txtRang23_2BISS.setValue(null);
						txtRang23_2DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang23_1DA.getText().isEmpty() || txtRang23_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang23_2DA.setValue(null);
						txtRang23_2A.setValue(null);
						txtRang23_2BISS.setValue(null);
						txtRang23_1A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_23_x1"));

					}

				}

			}
		});

		txtRang23_2DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_2DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_2DA.getText().isEmpty()
						&& (txtRang23_2DA.getText().length() >= 18 || txtRang23_2DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_2DA = new GridBagConstraints();
		gbc_txtRang23_2DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_2DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_2DA.gridx = 1;
		gbc_txtRang23_2DA.gridy = 7;
		bodyCenterLeft.add(txtRang23_2DA, gbc_txtRang23_2DA);

		txtRang23_2A = new JFormattedTextField(numberFormatRange);
		txtRang23_2A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_2A.getText().isEmpty()) {
					txtRang23_2A.setValue(null);
					txtRang23_2BISS.setValue(null);

				} else {
					if (txtRang23_2DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang23_2DA.requestFocusInWindow();
						return;
					}

					if (!txtRang23_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang23_2A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang23_2DA.getText().replace(".", "").replace(",", "."))) {
						txtRang23_2A.setValue(null);
						txtRang23_2BISS.setValue(null);
						txtRang23_2A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang23_1DA.getText().isEmpty() || txtRang23_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang23_2A.setValue(null);
						txtRang23_2BISS.setValue(null);

						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_23_x1"));
					}
				}

			}
		});

		txtRang23_2A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_2A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_2A.getText().isEmpty()
						&& (txtRang23_2A.getText().length() >= 18 || txtRang23_2A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_2A = new GridBagConstraints();
		gbc_txtRang23_2A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_2A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_2A.gridx = 2;
		gbc_txtRang23_2A.gridy = 7;
		bodyCenterLeft.add(txtRang23_2A, gbc_txtRang23_2A);

		JLabel lblRang23_2BISS = new JLabel(Messages.getString("param.m2.pct_biss_23_x2"));
		lblRang23_2BISS.setToolTipText(Messages.getString("param.m2.pct_biss_23_x2.title"));
		GridBagConstraints gbc_lblRang23_2BISS = new GridBagConstraints();
		gbc_lblRang23_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_2BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_2BISS.gridx = 3;
		gbc_lblRang23_2BISS.gridy = 7;
		bodyCenterLeft.add(lblRang23_2BISS, gbc_lblRang23_2BISS);

		txtRang23_2BISS = new JFormattedTextField(numberFormatRange);
		txtRang23_2BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_2BISS.getText().isEmpty()) {
					txtRang23_2BISS.setValue(null);

				}
				if (!txtRang23_2BISS.getText().isEmpty()
						&& (txtRang23_2DA.getText().isEmpty() || txtRang23_2A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_23_x2"));
					txtRang23_2BISS.setValue(null);

				}

				if (!txtRang23_2DA.getText().isEmpty() && !txtRang23_2A.getText().isEmpty()
						&& txtRang23_2BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang23_2BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang23_2BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_2BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_2BISS.getText().isEmpty() && (txtRang23_2BISS.getText().length() >= 18
						|| txtRang23_2BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_2BISS = new GridBagConstraints();
		gbc_txtRang23_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_2BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_2BISS.gridx = 4;
		gbc_txtRang23_2BISS.gridy = 7;
		bodyCenterLeft.add(txtRang23_2BISS, gbc_txtRang23_2BISS);

		JLabel lblNewLabel_2_2 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_2 = new GridBagConstraints();
		gbc_lblNewLabel_2_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_2.gridx = 5;
		gbc_lblNewLabel_2_2.gridy = 7;
		bodyCenterLeft.add(lblNewLabel_2_2, gbc_lblNewLabel_2_2);

		JLabel lblRang24_2 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_24_x2"));
		lblRang24_2.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_24_x2.title"));
		GridBagConstraints gbc_lblRang24_2 = new GridBagConstraints();
		gbc_lblRang24_2.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_2.gridx = 6;
		gbc_lblRang24_2.gridy = 7;
		bodyCenterLeft.add(lblRang24_2, gbc_lblRang24_2);

		txtRang24_2DA = new JFormattedTextField(numberFormatRange);
		txtRang24_2DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_2DA.getText().isEmpty()) {
					txtRang24_2DA.setValue(null);

				} else {
					if (!txtRang24_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang24_2DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang24_1A.getText().replace(".", "").replace(",", "."))) {
						txtRang24_2DA.setValue(null);
						txtRang24_2DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}
					if (txtRang24_1DA.getText().isEmpty() || txtRang24_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang24_2DA.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_24_x1"));
					}
				}
			}
		});
		txtRang24_2DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_2DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_2DA.getText().isEmpty()
						&& (txtRang24_2DA.getText().length() >= 18 || txtRang24_2DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_2DA = new GridBagConstraints();
		gbc_txtRang24_2DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_2DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_2DA.gridx = 7;
		gbc_txtRang24_2DA.gridy = 7;
		bodyCenterLeft.add(txtRang24_2DA, gbc_txtRang24_2DA);

		txtRang24_2A = new JFormattedTextField(numberFormatRange);
		txtRang24_2A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_2A.getText().isEmpty()) {
					txtRang24_2A.setValue(null);
					txtRang24_2BISS.setValue(null);

				} else {
					if (txtRang24_2DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang24_2DA.requestFocusInWindow();
						return;
					}

					if (!txtRang24_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang24_2A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang24_2DA.getText().replace(".", "").replace(",", "."))) {
						txtRang24_2A.setValue(null);
						txtRang24_2BISS.setValue(null);
						txtRang24_2A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang24_1DA.getText().isEmpty() || txtRang24_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang24_2A.setValue(null);
						txtRang24_2BISS.setValue(null);
						txtRang24_2A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_24_x1"));
					}
				}
			}
		});
		txtRang24_2A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_2A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_2A.getText().isEmpty()
						&& (txtRang24_2A.getText().length() >= 18 || txtRang24_2A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_2A = new GridBagConstraints();
		gbc_txtRang24_2A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_2A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_2A.gridx = 8;
		gbc_txtRang24_2A.gridy = 7;
		bodyCenterLeft.add(txtRang24_2A, gbc_txtRang24_2A);

		JLabel lblRang24_2BISS = new JLabel(Messages.getString("param.m2.pct_biss_24_x2"));
		lblRang24_2BISS.setToolTipText(Messages.getString("param.m2.pct_biss_24_x2.title"));
		GridBagConstraints gbc_lblRang24_2BISS = new GridBagConstraints();
		gbc_lblRang24_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_2BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_2BISS.gridx = 9;
		gbc_lblRang24_2BISS.gridy = 7;
		bodyCenterLeft.add(lblRang24_2BISS, gbc_lblRang24_2BISS);

		txtRang24_2BISS = new JFormattedTextField(numberFormatRange);
		txtRang24_2BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_2BISS.getText().isEmpty()) {
					txtRang24_2BISS.setValue(null);

				}
				if (!txtRang24_2BISS.getText().isEmpty()
						&& (txtRang24_2DA.getText().isEmpty() || txtRang24_2A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_24_x2"));
					txtRang24_2BISS.setValue(null);
				}

				if (!txtRang24_2DA.getText().isEmpty() && !txtRang24_2A.getText().isEmpty()
						&& txtRang24_2BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang24_2BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang24_2BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_2BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_2BISS.getText().isEmpty() && (txtRang24_2BISS.getText().length() >= 18
						|| txtRang24_2BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_2BISS = new GridBagConstraints();
		gbc_txtRang24_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_2BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_2BISS.gridx = 10;
		gbc_txtRang24_2BISS.gridy = 7;
		bodyCenterLeft.add(txtRang24_2BISS, gbc_txtRang24_2BISS);

		JLabel lblNewLabel_2_1_1 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1_1 = new GridBagConstraints();
		gbc_lblNewLabel_2_1_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1_1.gridx = 11;
		gbc_lblNewLabel_2_1_1.gridy = 7;
		bodyCenterLeft.add(lblNewLabel_2_1_1, gbc_lblNewLabel_2_1_1);

		JLabel lblRang23_3 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_23_x3"));
		lblRang23_3.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_23_x3.title"));
		GridBagConstraints gbc_lblRang23_3 = new GridBagConstraints();
		gbc_lblRang23_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_3.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_3.gridx = 0;
		gbc_lblRang23_3.gridy = 8;
		bodyCenterLeft.add(lblRang23_3, gbc_lblRang23_3);

		txtRang23_3DA = new JFormattedTextField(numberFormatRange);
		txtRang23_3DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_3DA.getText().isEmpty()) {
					txtRang23_3DA.setValue(null);
					txtRang23_3A.setValue(null);
					txtRang23_3BISS.setValue(null);

				} else {
					if (!txtRang23_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang23_3DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang23_2A.getText().replace(".", "").replace(",", "."))) {
						txtRang23_3DA.setValue(null);
						txtRang23_3A.setValue(null);
						txtRang23_3BISS.setValue(null);
						txtRang23_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang23_2DA.getText().isEmpty() || txtRang23_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang23_3DA.setValue(null);
						txtRang23_3A.setValue(null);
						txtRang23_3BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_23_x2"));
					}
				}
			}
		});
		txtRang23_3DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_3DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_3DA.getText().isEmpty()
						&& (txtRang23_3DA.getText().length() >= 18 || txtRang23_3DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_3DA = new GridBagConstraints();
		gbc_txtRang23_3DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_3DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_3DA.gridx = 1;
		gbc_txtRang23_3DA.gridy = 8;
		bodyCenterLeft.add(txtRang23_3DA, gbc_txtRang23_3DA);

		txtRang23_3A = new JFormattedTextField(numberFormatRange);
		txtRang23_3A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_3A.getText().isEmpty()) {
					txtRang23_3A.setValue(null);
					txtRang23_3BISS.setValue(null);
				} else {
					if (txtRang23_3DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang23_3DA.requestFocusInWindow();
						return;
					}

					if (!txtRang23_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang23_3A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang23_3DA.getText().replace(".", "").replace(",", "."))) {
						txtRang23_3A.setValue(null);
						txtRang23_3BISS.setValue(null);
						txtRang23_3A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}
					if (txtRang23_2DA.getText().isEmpty() || txtRang23_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang23_3A.setValue(null);
						txtRang23_3BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_23_x2"));
					}
				}

			}
		});
		txtRang23_3A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_3A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_3A.getText().isEmpty()
						&& (txtRang23_3A.getText().length() >= 18 || txtRang23_3A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_3A = new GridBagConstraints();
		gbc_txtRang23_3A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_3A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_3A.gridx = 2;
		gbc_txtRang23_3A.gridy = 8;
		bodyCenterLeft.add(txtRang23_3A, gbc_txtRang23_3A);

		JLabel lblRang23_3BISS = new JLabel(Messages.getString("param.m2.pct_biss_23_x3"));
		lblRang23_3BISS.setToolTipText(Messages.getString("param.m2.pct_biss_23_x3.title"));
		GridBagConstraints gbc_lblRang23_3BISS = new GridBagConstraints();
		gbc_lblRang23_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang23_3BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang23_3BISS.gridx = 3;
		gbc_lblRang23_3BISS.gridy = 8;
		bodyCenterLeft.add(lblRang23_3BISS, gbc_lblRang23_3BISS);

		txtRang23_3BISS = new JFormattedTextField(numberFormatRange);
		txtRang23_3BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang23_3BISS.getText().isEmpty()) {
					txtRang23_3BISS.setValue(null);
				}
				if (!txtRang23_3BISS.getText().isEmpty()
						&& (txtRang23_3DA.getText().isEmpty() || txtRang23_3A.getText().isEmpty())) {
					txtRang23_3BISS.setValue(null);

					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_23_x3"));

				}

				if (!txtRang23_3DA.getText().isEmpty() && !txtRang23_3A.getText().isEmpty()
						&& txtRang23_3BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang23_3BISS.requestFocusInWindow();
					return;
				}
			}
		});
		txtRang23_3BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang23_3BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang23_3BISS.getText().isEmpty() && (txtRang23_3BISS.getText().length() >= 18
						|| txtRang23_3BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang23_3BISS = new GridBagConstraints();
		gbc_txtRang23_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang23_3BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang23_3BISS.gridx = 4;
		gbc_txtRang23_3BISS.gridy = 8;
		bodyCenterLeft.add(txtRang23_3BISS, gbc_txtRang23_3BISS);

		JLabel lblNewLabel_2_3 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_3 = new GridBagConstraints();
		gbc_lblNewLabel_2_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_3.gridx = 5;
		gbc_lblNewLabel_2_3.gridy = 8;
		bodyCenterLeft.add(lblNewLabel_2_3, gbc_lblNewLabel_2_3);

		JLabel lblRang24_3 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_24_x3"));
		lblRang24_3.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_24_x3.title"));
		GridBagConstraints gbc_lblRang24_3 = new GridBagConstraints();
		gbc_lblRang24_3.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_3.gridx = 6;
		gbc_lblRang24_3.gridy = 8;
		bodyCenterLeft.add(lblRang24_3, gbc_lblRang24_3);

		txtRang24_3DA = new JFormattedTextField(numberFormatRange);
		txtRang24_3DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_3DA.getText().isEmpty()) {
					txtRang24_3DA.setValue(null);
				} else {
					if (!txtRang24_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang24_3DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang24_2A.getText().replace(".", "").replace(",", "."))) {
						txtRang24_3DA.setValue(null);
						txtRang24_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang24_2DA.getText().isEmpty() || txtRang24_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang24_3DA.setValue(null);
						txtRang24_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_24_x2"));
					}
				}
			}
		});
		txtRang24_3DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_3DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_3DA.getText().isEmpty()
						&& (txtRang24_3DA.getText().length() >= 18 || txtRang24_3DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_3DA = new GridBagConstraints();
		gbc_txtRang24_3DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_3DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_3DA.gridx = 7;
		gbc_txtRang24_3DA.gridy = 8;
		bodyCenterLeft.add(txtRang24_3DA, gbc_txtRang24_3DA);

		txtRang24_3A = new JFormattedTextField(numberFormatRange);
		txtRang24_3A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_3A.getText().isEmpty()) {
					txtRang24_3A.setValue(null);
					txtRang24_3BISS.setValue(null);
				} else {
					if (txtRang24_3DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang24_3DA.requestFocusInWindow();
						return;
					}

					if (!txtRang24_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang24_3A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang24_3DA.getText().replace(".", "").replace(",", "."))) {
						txtRang24_3A.setValue(null);
						txtRang24_3BISS.setValue(null);
						txtRang24_3A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang24_2DA.getText().isEmpty() || txtRang24_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang24_3A.setValue(null);
						txtRang24_3BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_24_x2"));
					}
				}
			}
		});
		txtRang24_3A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_3A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_3A.getText().isEmpty()
						&& (txtRang24_3A.getText().length() >= 18 || txtRang24_3A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_3A = new GridBagConstraints();
		gbc_txtRang24_3A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_3A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_3A.gridx = 8;
		gbc_txtRang24_3A.gridy = 8;
		bodyCenterLeft.add(txtRang24_3A, gbc_txtRang24_3A);

		JLabel lblRang24_3BISS = new JLabel(Messages.getString("param.m2.pct_biss_24_x3"));
		lblRang24_3BISS.setToolTipText(Messages.getString("param.m2.pct_biss_24_x3.title"));
		GridBagConstraints gbc_lblRang24_3BISS = new GridBagConstraints();
		gbc_lblRang24_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang24_3BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang24_3BISS.gridx = 9;
		gbc_lblRang24_3BISS.gridy = 8;
		bodyCenterLeft.add(lblRang24_3BISS, gbc_lblRang24_3BISS);

		txtRang24_3BISS = new JFormattedTextField(numberFormatRange);
		txtRang24_3BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang24_3BISS.getText().isEmpty()) {
					txtRang24_3BISS.setValue(null);
				}
				if (!txtRang24_3BISS.getText().isEmpty()
						&& (txtRang24_3DA.getText().isEmpty() || txtRang24_3A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_24_x3"));
					txtRang24_3BISS.setValue(null);
				}

				if (!txtRang24_3DA.getText().isEmpty() && !txtRang24_3A.getText().isEmpty()
						&& txtRang24_3BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang24_3BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang24_3BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang24_3BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang24_3BISS.getText().isEmpty() && (txtRang24_3BISS.getText().length() >= 18
						|| txtRang24_3BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang24_3BISS = new GridBagConstraints();
		gbc_txtRang24_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang24_3BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang24_3BISS.gridx = 10;
		gbc_txtRang24_3BISS.gridy = 8;
		bodyCenterLeft.add(txtRang24_3BISS, gbc_txtRang24_3BISS);

		JLabel lblNewLabel_2_1_2 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1_2 = new GridBagConstraints();
		gbc_lblNewLabel_2_1_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1_2.gridx = 11;
		gbc_lblNewLabel_2_1_2.gridy = 8;
		bodyCenterLeft.add(lblNewLabel_2_1_2, gbc_lblNewLabel_2_1_2);

		JLabel lblAMTHA25 = new JLabel(Messages.getString("param.m2.amt_ha_25") + "*");
		lblAMTHA25.setToolTipText(Messages.getString("param.m2.amt_ha_25.title"));
		GridBagConstraints gbc_lblAMTHA25 = new GridBagConstraints();
		gbc_lblAMTHA25.anchor = GridBagConstraints.EAST;
		gbc_lblAMTHA25.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTHA25.gridx = 0;
		gbc_lblAMTHA25.gridy = 9;
		bodyCenterLeft.add(lblAMTHA25, gbc_lblAMTHA25);

		txtAMTHA25 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMTHA25.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMTHA25.getText().isEmpty()) {
					txtAMTHA25.setValue(null);
				}
			}
		});
		txtAMTHA25.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMTHA25.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMTHA25.getText().isEmpty()
						&& (txtAMTHA25.getText().length() >= 18 || txtAMTHA25.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtAMTHA25 = new GridBagConstraints();
		gbc_txtAMTHA25.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMTHA25.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMTHA25.gridx = 1;
		gbc_txtAMTHA25.gridy = 9;
		gbc_txtAMTHA25.gridwidth = 2;
		bodyCenterLeft.add(txtAMTHA25, gbc_txtAMTHA25);

		JLabel lblPCTCriss25 = new JLabel(Messages.getString("param.m2.pct_criss_25") + "*");
		lblPCTCriss25.setToolTipText(Messages.getString("param.m2.pct_criss_25.title"));
		GridBagConstraints gbc_lblPCTCriss25  = new GridBagConstraints();
		gbc_lblPCTCriss25.anchor = GridBagConstraints.EAST;
		gbc_lblPCTCriss25.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTCriss25.gridx = 3;
		gbc_lblPCTCriss25.gridy = 9;
		bodyCenterLeft.add(lblPCTCriss25, gbc_lblPCTCriss25);

		txtPCTCRISS25 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTCRISS25.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTCRISS25.getText().isEmpty()) {
					txtPCTCRISS25.setValue(null);
				}
			}
		});
		txtPCTCRISS25.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCTCRISS25.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCTCRISS25.getText().isEmpty()
						&& (txtPCTCRISS25.getText().length() >= 18 || txtPCTCRISS25.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
					txtPCTCRISS25.requestFocus();
				}
			}
		});

		GridBagConstraints gbc_txtPCTCRISS25 = new GridBagConstraints();
		gbc_txtPCTCRISS25.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTCRISS25.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTCRISS25.gridx = 4;
		gbc_txtPCTCRISS25.gridy = 9;
		bodyCenterLeft.add(txtPCTCRISS25, gbc_txtPCTCRISS25);
		
		JLabel lblNewLabelPCTCRISS25 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabelPCTCRISS25 = new GridBagConstraints();
		gbc_lblNewLabelPCTCRISS25.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabelPCTCRISS25.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabelPCTCRISS25.gridx = 5;
		gbc_lblNewLabelPCTCRISS25.gridy = 9;
		bodyCenterLeft.add(lblNewLabelPCTCRISS25, gbc_lblNewLabelPCTCRISS25);
		
		
		JLabel lblAMTHA26 = new JLabel(Messages.getString("param.m2.amt_ha_26") + "*");
		lblAMTHA26.setToolTipText(Messages.getString("param.m2.amt_ha_26.title"));
		GridBagConstraints gbc_lblAMTHA26 = new GridBagConstraints();
		gbc_lblAMTHA26.anchor = GridBagConstraints.EAST;
		gbc_lblAMTHA26.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTHA26.gridx = 6;
		gbc_lblAMTHA26.gridy = 9;
		bodyCenterLeft.add(lblAMTHA26, gbc_lblAMTHA26);

		txtAMTHA26 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMTHA26.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMTHA26.getText().isEmpty()) {
					txtAMTHA26.setValue(null);
				}
			}
		});
		txtAMTHA26.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMTHA26.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMTHA26.getText().isEmpty()
						&& (txtAMTHA26.getText().length() >= 18 || txtAMTHA26.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtAMTHA26 = new GridBagConstraints();
		gbc_txtAMTHA26.gridwidth = 2;
		gbc_txtAMTHA26.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMTHA26.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMTHA26.gridx = 7;
		gbc_txtAMTHA26.gridy = 9;
		bodyCenterLeft.add(txtAMTHA26, gbc_txtAMTHA26);

		

JLabel lblPCTCriss26 = new JLabel(Messages.getString("param.m2.pct_criss_26") + "*");
		lblPCTCriss26.setToolTipText(Messages.getString("param.m2.pct_criss_26.title"));
		GridBagConstraints gbc_lblPCTCriss26  = new GridBagConstraints();
		gbc_lblPCTCriss26.anchor = GridBagConstraints.EAST;
		gbc_lblPCTCriss26.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTCriss26.gridx = 9;
		gbc_lblPCTCriss26.gridy = 9;
		bodyCenterLeft.add(lblPCTCriss26, gbc_lblPCTCriss26);

		txtPCTCRISS26 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTCRISS26.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTCRISS26.getText().isEmpty()) {
					txtPCTCRISS26.setValue(null);
				}
			}
		});
		txtPCTCRISS26.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCTCRISS26.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCTCRISS26.getText().isEmpty()
						&& (txtPCTCRISS26.getText().length() >= 18 || txtPCTCRISS26.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
					txtPCTCRISS26.requestFocus();
				}
			}
		});

		GridBagConstraints gbc_txtPCTCRISS26 = new GridBagConstraints();
		gbc_txtPCTCRISS26.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTCRISS26.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTCRISS26.gridx = 10;
		gbc_txtPCTCRISS26.gridy = 9;
		bodyCenterLeft.add(txtPCTCRISS26, gbc_txtPCTCRISS26);
		
		JLabel lblNewLabelPCTCRISS26 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabelPCTCRISS26 = new GridBagConstraints();
		gbc_lblNewLabelPCTCRISS26.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabelPCTCRISS26.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabelPCTCRISS26.gridx = 11;
		gbc_lblNewLabelPCTCRISS26.gridy = 9;
		bodyCenterLeft.add(lblNewLabelPCTCRISS26, gbc_lblNewLabelPCTCRISS25);
		
		
		JLabel lblRang25_1 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_25_x1"));
		lblRang25_1.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_25_x1.title"));
		GridBagConstraints gbc_lblRang25_1 = new GridBagConstraints();
		gbc_lblRang25_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_1.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_1.gridx = 0;
		gbc_lblRang25_1.gridy = 10;
		bodyCenterLeft.add(lblRang25_1, gbc_lblRang25_1);

		txtRang25_1DA = new JFormattedTextField(numberFormatRange);
		txtRang25_1DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_1DA.getText().isEmpty()) {
					txtRang25_1DA.setValue(null);
					txtRang25_1A.setValue(null);
					txtRang25_1BISS.setValue(null);

				}
			}
		});
		txtRang25_1DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_1DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_1DA.getText().isEmpty()
						&& (txtRang25_1DA.getText().length() >= 18 || txtRang25_1DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_1DA = new GridBagConstraints();
		gbc_txtRang25_1DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_1DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_1DA.gridx = 1;
		gbc_txtRang25_1DA.gridy = 10;
		bodyCenterLeft.add(txtRang25_1DA, gbc_txtRang25_1DA);

		txtRang25_1A = new JFormattedTextField(numberFormatRange);
		txtRang25_1A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_1A.getText().isEmpty()) {
					txtRang25_1A.setValue(null);
					txtRang25_1BISS.setValue(null);
				} else {

					if (txtRang25_1DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang25_1DA.requestFocusInWindow();
						return;
					}

					if (!txtRang25_1DA.getText().isEmpty()
							&& Double.parseDouble(txtRang25_1A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang25_1DA.getText().replace(".", "").replace(",", "."))) {
						txtRang25_1A.setValue(null);
						txtRang25_1BISS.setValue(null);
						txtRang25_1A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}
				}
			}
		});
		txtRang25_1A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_1A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_1A.getText().isEmpty()
						&& (txtRang25_1A.getText().length() >= 18 || txtRang25_1A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_1A = new GridBagConstraints();
		gbc_txtRang25_1A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_1A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_1A.gridx = 2;
		gbc_txtRang25_1A.gridy = 10;
		bodyCenterLeft.add(txtRang25_1A, gbc_txtRang25_1A);

		JLabel lblRang25_1BISS = new JLabel(Messages.getString("param.m2.pct_biss_25_x1"));
		lblRang25_1BISS.setToolTipText(Messages.getString("param.m2.pct_biss_25_x1.title"));
		GridBagConstraints gbc_lblRang25_1BISS = new GridBagConstraints();
		gbc_lblRang25_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_1BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_1BISS.gridx = 3;
		gbc_lblRang25_1BISS.gridy = 10;
		bodyCenterLeft.add(lblRang25_1BISS, gbc_lblRang25_1BISS);

		txtRang25_1BISS = new JFormattedTextField(numberFormatRange);
		txtRang25_1BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_1BISS.getText().isEmpty()) {
					txtRang25_1BISS.setValue(null);
				}
				if (!txtRang25_1BISS.getText().isEmpty()
						&& (txtRang25_1DA.getText().isEmpty() || txtRang25_1A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_25_x1"));
					txtRang25_1BISS.setValue(null);
				}

				if (!txtRang25_1DA.getText().isEmpty() && !txtRang25_1A.getText().isEmpty()
						&& txtRang25_1BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang25_1BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang25_1BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_1BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_1BISS.getText().isEmpty() && (txtRang25_1BISS.getText().length() >= 18
						|| txtRang25_1BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_1BISS = new GridBagConstraints();
		gbc_txtRang25_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_1BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_1BISS.gridx = 4;
		gbc_txtRang25_1BISS.gridy = 10;
		bodyCenterLeft.add(txtRang25_1BISS, gbc_txtRang25_1BISS);

		JLabel lblNewLabel_2_4 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_4 = new GridBagConstraints();
		gbc_lblNewLabel_2_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_4.gridx = 5;
		gbc_lblNewLabel_2_4.gridy = 10;
		bodyCenterLeft.add(lblNewLabel_2_4, gbc_lblNewLabel_2_4);

		JLabel lblRang26_1 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_26_x1"));
		lblRang26_1.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_26_x1.title"));
		GridBagConstraints gbc_lblRang26_1 = new GridBagConstraints();
		gbc_lblRang26_1.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_1.gridx = 6;
		gbc_lblRang26_1.gridy = 10;
		bodyCenterLeft.add(lblRang26_1, gbc_lblRang26_1);

		txtRang26_1DA = new JFormattedTextField(numberFormatRange);
		txtRang26_1DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_1DA.getText().isEmpty()) {
					txtRang26_1DA.setValue(null);
					txtRang26_1A.setValue(null);
					txtRang26_1BISS.setValue(null);

				}
			}
		});
		txtRang26_1DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_1DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_1DA.getText().isEmpty()
						&& (txtRang26_1DA.getText().length() >= 18 || txtRang26_1DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_1DA = new GridBagConstraints();
		gbc_txtRang26_1DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_1DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_1DA.gridx = 7;
		gbc_txtRang26_1DA.gridy = 10;
		bodyCenterLeft.add(txtRang26_1DA, gbc_txtRang26_1DA);

		txtRang26_1A = new JFormattedTextField(numberFormatRange);
		txtRang26_1A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_1A.getText().isEmpty()) {
					txtRang26_1A.setValue(null);
					txtRang26_1BISS.setValue(null);
				} else {
					if (txtRang26_1DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang26_1DA.requestFocusInWindow();
						return;
					}

					if (!txtRang26_1DA.getText().isEmpty()
							&& Double.parseDouble(txtRang26_1A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang26_1DA.getText().replace(".", "").replace(",", "."))) {
						txtRang26_1A.setValue(null);
						txtRang26_1BISS.setValue(null);
						txtRang26_1A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}
				}
			}
		});
		txtRang26_1A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_1A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_1A.getText().isEmpty()
						&& (txtRang26_1A.getText().length() >= 18 || txtRang26_1A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_1A = new GridBagConstraints();
		gbc_txtRang26_1A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_1A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_1A.gridx = 8;
		gbc_txtRang26_1A.gridy = 10;
		bodyCenterLeft.add(txtRang26_1A, gbc_txtRang26_1A);

		JLabel lblRang26_1BISS = new JLabel(Messages.getString("param.m2.pct_biss_26_x1"));
		lblRang26_1BISS.setToolTipText(Messages.getString("param.m2.pct_biss_26_x1.title"));
		GridBagConstraints gbc_lblRang26_1BISS = new GridBagConstraints();
		gbc_lblRang26_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_1BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_1BISS.gridx = 9;
		gbc_lblRang26_1BISS.gridy = 10;
		bodyCenterLeft.add(lblRang26_1BISS, gbc_lblRang26_1BISS);

		txtRang26_1BISS = new JFormattedTextField(numberFormatRange);
		txtRang26_1BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_1BISS.getText().isEmpty()) {
					txtRang26_1BISS.setValue(null);
				}
				if (!txtRang26_1BISS.getText().isEmpty()
						&& (txtRang26_1DA.getText().isEmpty() || txtRang26_1A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_26_x1"));
					txtRang26_1BISS.setValue(null);
				}

				if (!txtRang26_1DA.getText().isEmpty() && !txtRang26_1A.getText().isEmpty()
						&& txtRang26_1BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang26_1BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang26_1BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_1BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_1BISS.getText().isEmpty() && (txtRang26_1BISS.getText().length() >= 18
						|| txtRang26_1BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_1BISS = new GridBagConstraints();
		gbc_txtRang26_1BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_1BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_1BISS.gridx = 10;
		gbc_txtRang26_1BISS.gridy = 10;
		bodyCenterLeft.add(txtRang26_1BISS, gbc_txtRang26_1BISS);

		JLabel lblNewLabel_2_1_3 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1_3 = new GridBagConstraints();
		gbc_lblNewLabel_2_1_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1_3.gridx = 11;
		gbc_lblNewLabel_2_1_3.gridy = 10;
		bodyCenterLeft.add(lblNewLabel_2_1_3, gbc_lblNewLabel_2_1_3);

		JLabel lblRang25_2 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_25_x2"));
		lblRang25_2.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_25_x2.title"));
		GridBagConstraints gbc_lblRang25_2 = new GridBagConstraints();
		gbc_lblRang25_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_2.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_2.gridx = 0;
		gbc_lblRang25_2.gridy = 11;
		bodyCenterLeft.add(lblRang25_2, gbc_lblRang25_2);

		txtRang25_2DA = new JFormattedTextField(numberFormatRange);
		txtRang25_2DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_2DA.getText().isEmpty()) {
					txtRang25_2DA.setValue(null);
					txtRang25_2A.setValue(null);
					txtRang25_2BISS.setValue(null);
				} else {
					if (!txtRang25_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang25_2DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang25_1A.getText().replace(".", "").replace(",", "."))) {
						txtRang25_2DA.setValue(null);
						txtRang25_2A.setValue(null);
						txtRang25_2BISS.setValue(null);
						txtRang25_2DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang25_1DA.getText().isEmpty() || txtRang25_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang25_2DA.setValue(null);
						txtRang25_2A.setValue(null);
						txtRang25_2BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_25_x1"));
					}
				}
			}
		});
		txtRang25_2DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_2DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_2DA.getText().isEmpty()
						&& (txtRang25_2DA.getText().length() >= 18 || txtRang25_2DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_2DA = new GridBagConstraints();
		gbc_txtRang25_2DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_2DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_2DA.gridx = 1;
		gbc_txtRang25_2DA.gridy = 11;
		bodyCenterLeft.add(txtRang25_2DA, gbc_txtRang25_2DA);

		txtRang25_2A = new JFormattedTextField(numberFormatRange);
		txtRang25_2A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_2A.getText().isEmpty()) {
					txtRang25_2A.setValue(null);
					txtRang25_2BISS.setValue(null);
				} else {

					if (txtRang25_2DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang25_2DA.requestFocusInWindow();
						return;
					}

					if (!txtRang25_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang25_2A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang25_2DA.getText().replace(".", "").replace(",", "."))) {
						txtRang25_2A.setValue(null);
						txtRang25_2BISS.setValue(null);
						txtRang25_2A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang25_1DA.getText().isEmpty() || txtRang25_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang25_2A.setValue(null);
						txtRang25_2BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_25_x1"));
					}
				}
			}
		});
		txtRang25_2A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_2A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_2A.getText().isEmpty()
						&& (txtRang25_2A.getText().length() >= 18 || txtRang25_2A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_2A = new GridBagConstraints();
		gbc_txtRang25_2A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_2A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_2A.gridx = 2;
		gbc_txtRang25_2A.gridy = 11;
		bodyCenterLeft.add(txtRang25_2A, gbc_txtRang25_2A);

		JLabel lblRang25_2BISS = new JLabel(Messages.getString("param.m2.pct_biss_25_x2"));
		lblRang25_2BISS.setToolTipText(Messages.getString("param.m2.pct_biss_25_x2.title"));
		GridBagConstraints gbc_lblRang25_2BISS = new GridBagConstraints();
		gbc_lblRang25_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_2BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_2BISS.gridx = 3;
		gbc_lblRang25_2BISS.gridy = 11;
		bodyCenterLeft.add(lblRang25_2BISS, gbc_lblRang25_2BISS);

		txtRang25_2BISS = new JFormattedTextField(numberFormatRange);
		txtRang25_2BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_2BISS.getText().isEmpty()) {
					txtRang25_2BISS.setValue(null);
				}

				if (!txtRang25_2BISS.getText().isEmpty()
						&& (txtRang25_2DA.getText().isEmpty() || txtRang25_2A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_25_x2"));
					txtRang25_2BISS.setValue(null);
				}

				if (!txtRang25_2DA.getText().isEmpty() && !txtRang25_2A.getText().isEmpty()
						&& txtRang25_2BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang25_2BISS.requestFocusInWindow();
					return;
				}

			}

		});
		txtRang25_2BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_2BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_2BISS.getText().isEmpty() && (txtRang25_2BISS.getText().length() >= 18
						|| txtRang25_2BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_2BISS = new GridBagConstraints();
		gbc_txtRang25_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_2BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_2BISS.gridx = 4;
		gbc_txtRang25_2BISS.gridy = 11;
		bodyCenterLeft.add(txtRang25_2BISS, gbc_txtRang25_2BISS);

		JLabel lblNewLabel_2_5 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_5 = new GridBagConstraints();
		gbc_lblNewLabel_2_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_5.gridx = 5;
		gbc_lblNewLabel_2_5.gridy = 11;
		bodyCenterLeft.add(lblNewLabel_2_5, gbc_lblNewLabel_2_5);

		JLabel lblRang26_2 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_26_x2"));
		lblRang26_2.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_26_x2.title"));
		GridBagConstraints gbc_lblRang26_2 = new GridBagConstraints();
		gbc_lblRang26_2.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_2.gridx = 6;
		gbc_lblRang26_2.gridy = 11;
		bodyCenterLeft.add(lblRang26_2, gbc_lblRang26_2);

		txtRang26_2DA = new JFormattedTextField(numberFormatRange);
		txtRang26_2DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_2DA.getText().isEmpty()) {
					txtRang26_2DA.setValue(null);
				} else {
					if (!txtRang26_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang26_2DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang26_1A.getText().replace(".", "").replace(",", "."))) {
						txtRang26_2DA.setValue(null);
						txtRang26_2DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}
					if (txtRang26_1DA.getText().isEmpty() || txtRang26_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang26_2DA.setValue(null);
						txtRang26_2DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_26_x1"));
					}
				}
			}
		});
		txtRang26_2DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_2DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_2DA.getText().isEmpty()
						&& (txtRang26_2DA.getText().length() >= 18 || txtRang26_2DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_2DA = new GridBagConstraints();
		gbc_txtRang26_2DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_2DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_2DA.gridx = 7;
		gbc_txtRang26_2DA.gridy = 11;
		bodyCenterLeft.add(txtRang26_2DA, gbc_txtRang26_2DA);

		txtRang26_2A = new JFormattedTextField(numberFormatRange);
		txtRang26_2A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_2A.getText().isEmpty()) {
					txtRang26_2A.setValue(null);
					txtRang26_2BISS.setValue(null);
				} else {
					if (txtRang26_2DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang26_2DA.requestFocusInWindow();
						return;
					}

					if (!txtRang26_2DA.getText().isEmpty()
							&& Double.parseDouble(txtRang26_2A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang26_2DA.getText().replace(".", "").replace(",", "."))) {
						txtRang26_2A.setValue(null);
						txtRang26_2BISS.setValue(null);
						txtRang26_2A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang26_1DA.getText().isEmpty() || txtRang26_1A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang26_2A.setValue(null);
						txtRang26_2BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_26_x1"));
					}
				}
			}
		});
		txtRang26_2A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_2A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_2A.getText().isEmpty()
						&& (txtRang26_2A.getText().length() >= 18 || txtRang26_2A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_2A = new GridBagConstraints();
		gbc_txtRang26_2A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_2A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_2A.gridx = 8;
		gbc_txtRang26_2A.gridy = 11;
		bodyCenterLeft.add(txtRang26_2A, gbc_txtRang26_2A);

		JLabel lblRang26_2BISS = new JLabel(Messages.getString("param.m2.pct_biss_26_x2"));
		lblRang26_2BISS.setToolTipText(Messages.getString("param.m2.pct_biss_26_x2.title"));
		GridBagConstraints gbc_lblRang26_2BISS = new GridBagConstraints();
		gbc_lblRang26_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_2BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_2BISS.gridx = 9;
		gbc_lblRang26_2BISS.gridy = 11;
		bodyCenterLeft.add(lblRang26_2BISS, gbc_lblRang26_2BISS);

		txtRang26_2BISS = new JFormattedTextField(numberFormatRange);
		txtRang26_2BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_2BISS.getText().isEmpty()) {
					txtRang26_2BISS.setValue(null);
				}

				if (!txtRang26_2BISS.getText().isEmpty()
						&& (txtRang26_2DA.getText().isEmpty() || txtRang26_2A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_26_x2"));
					txtRang26_2BISS.setValue(null);
				}

				if (!txtRang26_2DA.getText().isEmpty() && !txtRang26_2A.getText().isEmpty()
						&& txtRang26_2BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang26_2BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang26_2BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_2BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_2BISS.getText().isEmpty() && (txtRang26_2BISS.getText().length() >= 18
						|| txtRang26_2BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_2BISS = new GridBagConstraints();
		gbc_txtRang26_2BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_2BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_2BISS.gridx = 10;
		gbc_txtRang26_2BISS.gridy = 11;
		bodyCenterLeft.add(txtRang26_2BISS, gbc_txtRang26_2BISS);

		JLabel lblNewLabel_2_1_4 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1_4 = new GridBagConstraints();
		gbc_lblNewLabel_2_1_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1_4.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1_4.gridx = 11;
		gbc_lblNewLabel_2_1_4.gridy = 11;
		bodyCenterLeft.add(lblNewLabel_2_1_4, gbc_lblNewLabel_2_1_4);

		JLabel lblRang25_3 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_25_x3"));
		lblRang25_3.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_25_x3.title"));
		GridBagConstraints gbc_lblRang25_3 = new GridBagConstraints();
		gbc_lblRang25_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_3.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_3.gridx = 0;
		gbc_lblRang25_3.gridy = 12;
		bodyCenterLeft.add(lblRang25_3, gbc_lblRang25_3);

		txtRang25_3DA = new JFormattedTextField(numberFormatRange);
		txtRang25_3DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {

				if (txtRang25_3DA.getText().isEmpty()) {
					txtRang25_3DA.setValue(null);
					txtRang25_3A.setValue(null);
					txtRang25_3BISS.setValue(null);
				} else {

					if (!txtRang25_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang25_3DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang25_2A.getText().replace(".", "").replace(",", "."))) {
						txtRang25_3DA.setValue(null);
						txtRang25_3A.setValue(null);
						txtRang25_3BISS.setValue(null);
						txtRang25_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang25_2DA.getText().isEmpty() || txtRang25_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang25_3DA.setValue(null);
						txtRang25_3A.setValue(null);
						txtRang25_3BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_25_x2"));
					}
				}
			}
		});
		txtRang25_3DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_3DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_3DA.getText().isEmpty()
						&& (txtRang25_3DA.getText().length() >= 18 || txtRang25_3DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_3DA = new GridBagConstraints();
		gbc_txtRang25_3DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_3DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_3DA.gridx = 1;
		gbc_txtRang25_3DA.gridy = 12;
		bodyCenterLeft.add(txtRang25_3DA, gbc_txtRang25_3DA);

		txtRang25_3A = new JFormattedTextField(numberFormatRange);
		txtRang25_3A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_3A.getText().isEmpty()) {
					txtRang25_3A.setValue(null);
					txtRang25_3BISS.setValue(null);
				} else {
					if (txtRang25_3DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang25_3DA.requestFocusInWindow();
						return;
					}

					if (!txtRang25_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang25_3A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang25_3DA.getText().replace(".", "").replace(",", "."))) {
						txtRang25_3A.setValue(null);
						txtRang25_3BISS.setValue(null);
						txtRang25_3A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang25_2DA.getText().isEmpty() || txtRang25_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang25_3A.setValue(null);
						txtRang25_3BISS.setValue(null);
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_25_x2"));
					}
				}

			}
		});
		txtRang25_3A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_3A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_3A.getText().isEmpty()
						&& (txtRang25_3A.getText().length() >= 18 || txtRang25_3A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_3A = new GridBagConstraints();
		gbc_txtRang25_3A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_3A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_3A.gridx = 2;
		gbc_txtRang25_3A.gridy = 12;
		bodyCenterLeft.add(txtRang25_3A, gbc_txtRang25_3A);

		JLabel lblRang25_3BISS = new JLabel(Messages.getString("param.m2.pct_biss_25_x3"));
		lblRang25_3BISS.setToolTipText(Messages.getString("param.m2.pct_biss_25_x3.title"));
		GridBagConstraints gbc_lblRang25_3BISS = new GridBagConstraints();
		gbc_lblRang25_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang25_3BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang25_3BISS.gridx = 3;
		gbc_lblRang25_3BISS.gridy = 12;
		bodyCenterLeft.add(lblRang25_3BISS, gbc_lblRang25_3BISS);

		txtRang25_3BISS = new JFormattedTextField(numberFormatRange);
		txtRang25_3BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang25_3BISS.getText().isEmpty()) {
					txtRang25_3BISS.setValue(null);
				}

				if (!txtRang25_3BISS.getText().isEmpty()
						&& (txtRang25_3DA.getText().isEmpty() || txtRang25_3A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_25_x3"));
					txtRang25_3BISS.setValue(null);
				}

				if (!txtRang25_3DA.getText().isEmpty() && !txtRang25_3A.getText().isEmpty()
						&& txtRang25_3BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang25_3BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang25_3BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang25_3BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang25_3BISS.getText().isEmpty() && (txtRang25_3BISS.getText().length() >= 18
						|| txtRang25_3BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang25_3BISS = new GridBagConstraints();
		gbc_txtRang25_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang25_3BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang25_3BISS.gridx = 4;
		gbc_txtRang25_3BISS.gridy = 12;
		bodyCenterLeft.add(txtRang25_3BISS, gbc_txtRang25_3BISS);

		JLabel lblNewLabel_2_6 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_6 = new GridBagConstraints();
		gbc_lblNewLabel_2_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_6.gridx = 5;
		gbc_lblNewLabel_2_6.gridy = 12;
		bodyCenterLeft.add(lblNewLabel_2_6, gbc_lblNewLabel_2_6);

		JLabel lblRang26_3 = new JLabel(Messages.getString("param.m2.num_sup_rng_fm_26_x3"));
		lblRang26_3.setToolTipText(Messages.getString("param.m2.num_sup_rng_fm_26_x3.title"));
		GridBagConstraints gbc_lblRang26_3 = new GridBagConstraints();
		gbc_lblRang26_3.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_3.gridx = 6;
		gbc_lblRang26_3.gridy = 12;
		bodyCenterLeft.add(lblRang26_3, gbc_lblRang26_3);

		txtRang26_3DA = new JFormattedTextField(numberFormatRange);
		txtRang26_3DA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {

				if (txtRang26_3DA.getText().isEmpty()) {
					txtRang26_3DA.setValue(null);
				} else {

					if (!txtRang26_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang26_3DA.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang26_2A.getText().replace(".", "").replace(",", "."))) {
						txtRang26_3DA.setValue(null);
						txtRang26_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.maggiore.a.prec"));
					}

					if (txtRang26_2DA.getText().isEmpty() || txtRang26_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang26_3DA.setValue(null);
						txtRang26_3DA.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_26_x2"));
					}
				}
			}
		});
		txtRang26_3DA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_3DA.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_3DA.getText().isEmpty()
						&& (txtRang26_3DA.getText().length() >= 18 || txtRang26_3DA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_3DA = new GridBagConstraints();
		gbc_txtRang26_3DA.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_3DA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_3DA.gridx = 7;
		gbc_txtRang26_3DA.gridy = 12;
		bodyCenterLeft.add(txtRang26_3DA, gbc_txtRang26_3DA);

		txtRang26_3A = new JFormattedTextField(numberFormatRange);
		txtRang26_3A.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_3A.getText().isEmpty()) {
					txtRang26_3A.setValue(null);
					txtRang26_3BISS.setValue(null);
				} else {

					if (txtRang26_3DA.getText().isEmpty()) {
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.da.empty"));
						txtRang26_3DA.requestFocusInWindow();
						return;
					}

					if (!txtRang26_3DA.getText().isEmpty()
							&& Double.parseDouble(txtRang26_3A.getText().replace(".", "").replace(",", ".")) <= Double
									.parseDouble(txtRang26_3DA.getText().replace(".", "").replace(",", "."))) {
						txtRang26_3A.setValue(null);
						txtRang26_3BISS.setValue(null);
						txtRang26_3A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.a.maggiore.da"));
					}

					if (txtRang26_2DA.getText().isEmpty() || txtRang26_2A.getText().isEmpty()) {
						getToolkit().beep();
						txtRang26_3A.setValue(null);
						txtRang26_3BISS.setValue(null);
						txtRang26_3A.requestFocusInWindow();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.before.year.empty")
								+ ":" + Messages.getString("param.m2.num_sup_rng_fm_26_x2"));
					}
				}
			}
		});
		txtRang26_3A.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_3A.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_3A.getText().isEmpty()
						&& (txtRang26_3A.getText().length() >= 18 || txtRang26_3A.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_3A = new GridBagConstraints();
		gbc_txtRang26_3A.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_3A.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_3A.gridx = 8;
		gbc_txtRang26_3A.gridy = 12;
		bodyCenterLeft.add(txtRang26_3A, gbc_txtRang26_3A);

		JLabel lblRang26_3BISS = new JLabel(Messages.getString("param.m2.pct_biss_26_x3"));
		lblRang26_3BISS.setToolTipText(Messages.getString("param.m2.pct_biss_26_x3.title"));
		GridBagConstraints gbc_lblRang26_3BISS = new GridBagConstraints();
		gbc_lblRang26_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_lblRang26_3BISS.anchor = GridBagConstraints.EAST;
		gbc_lblRang26_3BISS.gridx = 9;
		gbc_lblRang26_3BISS.gridy = 12;
		bodyCenterLeft.add(lblRang26_3BISS, gbc_lblRang26_3BISS);

		txtRang26_3BISS = new JFormattedTextField(numberFormatRange);
		txtRang26_3BISS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtRang26_3BISS.getText().isEmpty()) {
					txtRang26_3BISS.setValue(null);
				}

				if (!txtRang26_3BISS.getText().isEmpty()
						&& (txtRang26_3DA.getText().isEmpty() || txtRang26_3A.getText().isEmpty())) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.field.da.a.empty") + ":"
							+ Messages.getString("param.m2.num_sup_rng_fm_26_x3"));
					txtRang26_3BISS.setValue(null);
				}

				if (!txtRang26_3DA.getText().isEmpty() && !txtRang26_3A.getText().isEmpty()
						&& txtRang26_3BISS.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.biss.empty"));
					txtRang26_3BISS.requestFocusInWindow();
					return;
				}

			}
		});
		txtRang26_3BISS.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtRang26_3BISS.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtRang26_3BISS.getText().isEmpty() && (txtRang26_3BISS.getText().length() >= 18
						|| txtRang26_3BISS.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtRang26_3BISS = new GridBagConstraints();
		gbc_txtRang26_3BISS.insets = new Insets(0, 0, 5, 5);
		gbc_txtRang26_3BISS.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtRang26_3BISS.gridx = 10;
		gbc_txtRang26_3BISS.gridy = 12;
		bodyCenterLeft.add(txtRang26_3BISS, gbc_txtRang26_3BISS);

		JLabel lblNewLabel_2_1_5 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1_5 = new GridBagConstraints();
		gbc_lblNewLabel_2_1_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1_5.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1_5.gridx = 11;
		gbc_lblNewLabel_2_1_5.gridy = 12;
		bodyCenterLeft.add(lblNewLabel_2_1_5, gbc_lblNewLabel_2_1_5);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 0, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 5;
		gbc_panel_2.gridy = 13;
		gbc_panel_2.gridwidth = 2;
		bodyCenterLeft.add(panel_2, gbc_panel_2);

		JButton button = new JButton(Messages.getString("button.esegui.simulazione"));
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		// set valori di default da eliminare
		txtNunMinSupLEvel.setText("1,0000");

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Date curr = new Date();
				String anno = "" + (curr.getYear() + 1900);
				String mese = (curr.getMonth() + 1) > 9 ? "" + (curr.getMonth() + 1) : "0" + (curr.getMonth() + 1);
				String giorno = curr.getDate() > 9 ? "" + curr.getDate() : "0" + curr.getDate();
				String sData = anno + "-" + mese + "-" + giorno;
				String hh = curr.getHours() > 9 ? "" + curr.getHours() : "0" + curr.getHours();
				String mm = curr.getMinutes() > 9 ? "" + curr.getMinutes() : "0" + curr.getMinutes();
				String ss = curr.getSeconds() > 9 ? "" + curr.getSeconds() : "0" + curr.getSeconds();
				String dataFile = "PYR_FILE_" + sData + " " + hh + mm + ss;
				dataDB = "" + sData + " " + hh + ":" + mm + ":" + ss;

				if (txtPathFile.getText().isEmpty() || txtNunMinSupLEvel.getText().isEmpty()
						|| txtAMTHA23.getText().isEmpty() || txtAMTHA24.getText().isEmpty()
						|| txtAMTHA25.getText().isEmpty() || txtAMTHA26.getText().isEmpty()
						|| txtPCTCRISS23.getText().isEmpty() || txtPCTCRISS24.getText().isEmpty()
						|| txtPCTCRISS25.getText().isEmpty() || txtPCTCRISS26.getText().isEmpty()) {

					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.chkdati"));
				} else {

					int input = JOptionPane.showConfirmDialog(null, Messages.getString("simulazione.conferma.avvio"));
					if (input == 0) {
						try {

							UtilFile.copiaFile(txtPathFile.getText(), Home.getPathJar() + "/"
									+ ConfigurationProperties.FOLDER_DATA + "/" + dataFile + ".csv");
							SwingWorker work = createWorker(dataDB);
							work.execute();
							JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avviata"));
							Home.switchPanel(new PanelBodySimulazione());

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} finally {

						}

					} else {

					}

				}
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(0, 110, 171));
		panel_2.add(button);

	}

	public SwingWorker<Boolean, Integer> createWorker(String dataDB) {
		return new SwingWorker<Boolean, Integer>() {
			@Override
			protected Boolean doInBackground() throws Exception {

				try {
					UtilKettle.executeJobPYR(txtAMTHA23.getText(), txtAMTHA24.getText(), txtAMTHA25.getText(),
							txtAMTHA26.getText(), txtPCTCRISS23.getText(), txtPCTCRISS24.getText(), txtPCTCRISS25.getText(),txtPCTCRISS26.getText(), 
							dataDB,
							jcbFlgOnlyPure.getSelectedItem().toString()
									.equalsIgnoreCase(Messages.getString("select.no")) ? "0" : "1",
							txtNunMinSupLEvel.getText(),
							txtRang23_1DA.getText().isEmpty() ? "-1" : txtRang23_1DA.getText(),
							txtRang23_2DA.getText().isEmpty() ? "-1" : txtRang23_2DA.getText(),
							txtRang23_3DA.getText().isEmpty() ? "-1" : txtRang23_3DA.getText(),
							txtRang24_1DA.getText().isEmpty() ? "-1" : txtRang24_1DA.getText(),
							txtRang24_2DA.getText().isEmpty() ? "-1" : txtRang24_2DA.getText(),
							txtRang24_3DA.getText().isEmpty() ? "-1" : txtRang24_3DA.getText(),
							txtRang25_1DA.getText().isEmpty() ? "-1" : txtRang25_1DA.getText(),
							txtRang25_2DA.getText().isEmpty() ? "-1" : txtRang25_2DA.getText(),
							txtRang25_3DA.getText().isEmpty() ? "-1" : txtRang25_3DA.getText(),
							txtRang26_1DA.getText().isEmpty() ? "-1" : txtRang26_1DA.getText(),
							txtRang26_2DA.getText().isEmpty() ? "-1" : txtRang26_2DA.getText(),
							txtRang26_3DA.getText().isEmpty() ? "-1" : txtRang26_3DA.getText(),
							txtRang23_1A.getText().isEmpty() ? "-1" : txtRang23_1A.getText(),
							txtRang23_2A.getText().isEmpty() ? "-1" : txtRang23_2A.getText(),
							txtRang23_3A.getText().isEmpty() ? "-1" : txtRang23_3A.getText(),
							txtRang24_1A.getText().isEmpty() ? "-1" : txtRang24_1A.getText(),
							txtRang24_2A.getText().isEmpty() ? "-1" : txtRang24_2A.getText(),
							txtRang24_3A.getText().isEmpty() ? "-1" : txtRang24_3A.getText(),
							txtRang25_1A.getText().isEmpty() ? "-1" : txtRang25_1A.getText(),
							txtRang25_2A.getText().isEmpty() ? "-1" : txtRang25_2A.getText(),
							txtRang25_3A.getText().isEmpty() ? "-1" : txtRang25_3A.getText(),
							txtRang26_1A.getText().isEmpty() ? "-1" : txtRang26_1A.getText(),
							txtRang26_2A.getText().isEmpty() ? "-1" : txtRang26_2A.getText(),
							txtRang26_3A.getText().isEmpty() ? "-1" : txtRang26_3A.getText(),
							txtRang23_1BISS.getText().isEmpty() ? "-1" : txtRang23_1BISS.getText(),
							txtRang23_2BISS.getText().isEmpty() ? "-1" : txtRang23_2BISS.getText(),
							txtRang23_3BISS.getText().isEmpty() ? "-1" : txtRang23_3BISS.getText(),
							txtRang24_1BISS.getText().isEmpty() ? "-1" : txtRang24_1BISS.getText(),
							txtRang24_2BISS.getText().isEmpty() ? "-1" : txtRang24_2BISS.getText(),
							txtRang24_3BISS.getText().isEmpty() ? "-1" : txtRang24_3BISS.getText(),
							txtRang25_1BISS.getText().isEmpty() ? "-1" : txtRang25_1BISS.getText(),
							txtRang25_2BISS.getText().isEmpty() ? "-1" : txtRang25_2BISS.getText(),
							txtRang25_3BISS.getText().isEmpty() ? "-1" : txtRang25_3BISS.getText(),
							txtRang26_1BISS.getText().isEmpty() ? "-1" : txtRang26_1BISS.getText(),
							txtRang26_2BISS.getText().isEmpty() ? "-1" : txtRang26_2BISS.getText(),
							txtRang26_3BISS.getText().isEmpty() ? "-1" : txtRang26_3BISS.getText());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avvio.ko"));
				}

				return true;
			}

			@Override
			protected void process(List<Integer> chunks) {
			}

			@Override
			protected void done() {
				boolean bStatus = false;
				try {
					UtilPrint print = new UtilPrint();
					print.Stampa(dataDB);
					if (Home.getPanelActive().equalsIgnoreCase(PanelBodySimulazione.class.getName())) {
						// PanelBodySimulazione.getElencoSimulazioni();
						Home.switchPanel(new PanelBodySimulazione());
//                		JOptionPane.showMessageDialog(null, "aggiorno tabella","",JOptionPane.ERROR_MESSAGE);
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println("Finished with status " + bStatus);
			}
		};
	}

//	 private void waitFor (int iMillis) {
//	        try {
//	            Thread.sleep(iMillis);
//	        }
//	        catch (Exception ex) {
//	            System.err.println(ex);
//	        }
//	    } // End of M

	private String esitoJob(String data) {
		String result = null;
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			PreparedStatement st = connection
					.prepareStatement("select * from sys_status_job  where dat_exec_job ='" + data + "'");
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				result = rs.getString("job_status_type") + ":" + rs.getString("job_status");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

}
