package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

import bl.ConfigurationProperties;
import bl.ConnectionDB;
import bl.Messages;
import bl.UtilFile;
import bl.UtilKettle;
import bl.UtilPrint;

public class PanelBodySimulazioneModel3 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7647813675306805072L;
	private JTextField txtPathFile;
	private JComboBox jcbNumYearMaxLevel;
	private JFormattedTextField txtAMTMaxLevel;
	private JFormattedTextField txtPCT_BISS_26;
	private JFormattedTextField txtNunMaxSupLevel;
	private String dataDB;
	private JFormattedTextField txtPCTSL;
	// private JFormattedTextField txtAMTMAXBPS22;
	private JFormattedTextField txtNUMCYSL;

	private JFormattedTextField txtAMT_HA_GRE;
	private JFormattedTextField txtAMT_MAX_23;

	private JFormattedTextField txtPCT_BISS_24;
	private JFormattedTextField txtAMT_MAX_25;

	private JFormattedTextField txtPCT_BISS_23;
	private JFormattedTextField txtAMT_MAX_24;

	private JFormattedTextField txtPCT_BISS_25;
	private JFormattedTextField txtAMT_MAX_26;
	private JComboBox jcbNumYearsConv;
	private JFormattedTextField txtPCTAUA;
	private JRadioButton jrbGREE;
	private JRadioButton jrbHAGREE;

	/**
	 * Create the panel.
	 */
	public PanelBodySimulazioneModel3(String modelTitle) {
		setBackground(Color.WHITE);
		setName(PanelBodySimulazioneModel1.class.getName());
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.2, 0.6, 0.2 };
		gridBagLayout.rowWeights = new double[] { 0.2, 0.7, 0.1 };
		setLayout(gridBagLayout);

		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/home.png"));
		Image imageHome = iconHome.getImage();
		imageHome = imageHome.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
		iconHome = new ImageIcon(imageHome);

		JButton btnHome = new JButton(iconHome);
		btnHome.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyHome());
			}
		});
		btnHome.setContentAreaFilled(false);
		btnHome.setSize(30, 30);
		btnHome.setToolTipText(Messages.getString("page.home"));
		btnHome.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		JPanel menuLeft = new JPanel();
		FlowLayout flowLayout = (FlowLayout) menuLeft.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		menuLeft.setBackground(Color.WHITE);
		menuLeft.add(btnHome);

		ImageIcon iconBreadcrumbSetting = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbSetting = iconBreadcrumbSetting.getImage();
		imageBreadcrumbSetting = imageBreadcrumbSetting.getScaledInstance(150, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbSetting = new ImageIcon(imageBreadcrumbSetting);
		JLabel labelBreadcrumbSetting = new JLabel(iconBreadcrumbSetting);
		labelBreadcrumbSetting.setForeground(Color.WHITE);
		labelBreadcrumbSetting.setText(Messages.getString("simulazione.simulazione"));
		labelBreadcrumbSetting.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbSetting.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbSetting.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodySimulazione());
			}
		});
		labelBreadcrumbSetting.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbSetting);

		ImageIcon iconBreadcrumbNewSimulazione = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbNewSimulazione = iconBreadcrumbNewSimulazione.getImage();
		imageBreadcrumbNewSimulazione = imageBreadcrumbNewSimulazione.getScaledInstance(190, 30,
				java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbNewSimulazione = new ImageIcon(imageBreadcrumbNewSimulazione);
		JLabel labelBreadcrumbNewSimulazione = new JLabel(iconBreadcrumbNewSimulazione);
		labelBreadcrumbNewSimulazione.setForeground(Color.WHITE);
		labelBreadcrumbNewSimulazione.setText(Messages.getString("simulazione.nuova.simulazione"));
		labelBreadcrumbNewSimulazione.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbNewSimulazione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		labelBreadcrumbNewSimulazione.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Home.switchPanel(new PanelBodyNewSimulazione());
			}

		});
		labelBreadcrumbNewSimulazione.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		menuLeft.add(labelBreadcrumbNewSimulazione);

		ImageIcon iconBreadcrumbModello1 = new ImageIcon(Home.class.getResource("/images/breadcrumb_empty.png"));
		Image imageBreadcrumbModello1 = iconBreadcrumbModello1.getImage();
		imageBreadcrumbModello1 = imageBreadcrumbModello1.getScaledInstance(300, 30, java.awt.Image.SCALE_SMOOTH);
		iconBreadcrumbModello1 = new ImageIcon(imageBreadcrumbModello1);
		JLabel labelBreadcrumbModello1 = new JLabel(iconBreadcrumbModello1);
		labelBreadcrumbModello1.setForeground(Color.WHITE);
		labelBreadcrumbModello1.setText(modelTitle.substring(0, modelTitle.indexOf("-")));
		labelBreadcrumbModello1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		labelBreadcrumbModello1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		menuLeft.add(labelBreadcrumbModello1);

		GridBagConstraints gbc_menuLeft = new GridBagConstraints();
		gbc_menuLeft.insets = new Insets(0, 0, 5, 5);
		gbc_menuLeft.fill = GridBagConstraints.BOTH;
		gbc_menuLeft.gridx = 0;
		gbc_menuLeft.gridy = 0;
		gbc_menuLeft.gridwidth = 2;
		add(menuLeft, gbc_menuLeft);

		JPanel menuRight = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) menuRight.getLayout();
		flowLayout_2.setVgap(0);
		menuRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_menuRight = new GridBagConstraints();
		gbc_menuRight.insets = new Insets(0, 0, 5, 0);
		gbc_menuRight.fill = GridBagConstraints.BOTH;
		gbc_menuRight.gridx = 2;
		gbc_menuRight.gridy = 0;
		add(menuRight, gbc_menuRight);

		JPanel bodyCenterLeft = new JPanel();
		bodyCenterLeft.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_1_1 = new GridBagConstraints();
		gbc_panel_1_1.insets = new Insets(10, 10, 10, 10);
		gbc_panel_1_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1_1.gridx = 0;
		gbc_panel_1_1.gridy = 1;
		gbc_panel_1_1.gridwidth = 3;
		// bodyCenter.add(bodyCenterLeft, gbc_panel_1_1);
		add(bodyCenterLeft, gbc_panel_1_1);
		GridBagLayout gbl_bodyCenterLeft = new GridBagLayout();
		gbl_bodyCenterLeft.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_bodyCenterLeft.columnWeights = new double[] { 0.1, 0.3, 0.1, 0.3, 0.1 };
		gbl_bodyCenterLeft.rowWeights = new double[] { 0.1, 0.3, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1,
				0.1 };
		bodyCenterLeft.setLayout(gbl_bodyCenterLeft);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_1 = (FlowLayout) panel.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		gbc_panel.gridwidth = 5;
		bodyCenterLeft.add(panel, gbc_panel);

		JLabel lblNewLabel = new JLabel(Messages.getString("simulazione.seleziona.file"));
		panel.add(lblNewLabel);

		txtPathFile = new JTextField();
		txtPathFile.setEditable(false);
		txtPathFile.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_txtPathFile = new GridBagConstraints();
		gbc_txtPathFile.insets = new Insets(0, 0, 5, 5);
		gbc_txtPathFile.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPathFile.gridx = 0;
		gbc_txtPathFile.gridy = 1;
		gbc_txtPathFile.gridwidth = 3;
		bodyCenterLeft.add(txtPathFile, gbc_txtPathFile);
		txtPathFile.setColumns(10);

		JButton btnNewButton = new JButton(Messages.getString("button.sfoglia"));
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 110, 171));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV FILES", "csv", "csv");
				fileChooser.setFileFilter(filter);
				int returnVal = fileChooser.showOpenDialog((Component) e.getSource());
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						txtPathFile.setText(file.toString());
					} catch (Exception ex) {
						System.out.println("problem accessing file" + file.getAbsolutePath());
					}
				} else {
					// simulazione.seleziona.file.empty
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.seleziona.file.empty"));
				}

			}

		});
		btnNewButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 1;
		gbc_btnNewButton.gridwidth = 2;
		bodyCenterLeft.add(btnNewButton, gbc_btnNewButton);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(76, 144, 33)));
		FlowLayout flowLayout_3 = (FlowLayout) panel_1.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.anchor = GridBagConstraints.WEST;
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 2;
		gbc_panel_1.gridwidth = 5;
		bodyCenterLeft.add(panel_1, gbc_panel_1);

		JLabel lblNewLabel_1 = new JLabel(Messages.getString("simulazione.scelta.parametri"));
		panel_1.add(lblNewLabel_1);

		JLabel lblNunMaxSupLevel = new JLabel(Messages.getString("param.m3.num_max_sup_level") + "*"); //$NON-NLS-1$
		lblNunMaxSupLevel.setToolTipText(Messages.getString("param.m3.num_max_sup_level.title"));
		GridBagConstraints gbc_lblNunMaxSupLevel = new GridBagConstraints();
		gbc_lblNunMaxSupLevel.anchor = GridBagConstraints.EAST;
		gbc_lblNunMaxSupLevel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNunMaxSupLevel.gridx = 0;
		gbc_lblNunMaxSupLevel.gridy = 4;
//		  gbc_lblNunMinSupLEvel.gridwidth=2;
		bodyCenterLeft.add(lblNunMaxSupLevel, gbc_lblNunMaxSupLevel);

		txtNunMaxSupLevel = new JFormattedTextField(NumberFormat.getInstance());
		txtNunMaxSupLevel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtNunMaxSupLevel.getText().isEmpty()) {
					txtNunMaxSupLevel.setValue(null);
				}
			}
		});
		txtNunMaxSupLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtNunMaxSupLevel.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtNunMaxSupLevel.getText().isEmpty() && (txtNunMaxSupLevel.getText().length() >= 18
						|| txtNunMaxSupLevel.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtNunMaxSupLevel = new GridBagConstraints();
		gbc_txtNunMaxSupLevel.insets = new Insets(0, 0, 5, 5);
		gbc_txtNunMaxSupLevel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNunMaxSupLevel.gridx = 1;
		gbc_txtNunMaxSupLevel.gridy = 4;
		// gbc_txtNunMinSupLEvel.gridwidth=3;
		bodyCenterLeft.add(txtNunMaxSupLevel, gbc_txtNunMaxSupLevel);

		JLabel lblNumYearsConv = new JLabel(Messages.getString("param.m3.num_years_conv") + "*");
		lblNumYearsConv.setToolTipText(Messages.getString("param.m3.num_years_conv.title"));
		GridBagConstraints gbc_lblNumYearsConv = new GridBagConstraints();
		gbc_lblNumYearsConv.anchor = GridBagConstraints.EAST;
		gbc_lblNumYearsConv.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumYearsConv.gridx = 2;
		gbc_lblNumYearsConv.gridy = 4;
		gbc_lblNumYearsConv.gridwidth = 1;
		bodyCenterLeft.add(lblNumYearsConv, gbc_lblNumYearsConv);

		jcbNumYearsConv = new JComboBox();
		jcbNumYearsConv.addItem("1");
		jcbNumYearsConv.addItem("2");
		jcbNumYearsConv.addItem("3");
		jcbNumYearsConv.addItem("4");
		GridBagConstraints gbc_jcbNumYearsConv = new GridBagConstraints();
		gbc_jcbNumYearsConv.insets = new Insets(0, 0, 5, 5);
		gbc_jcbNumYearsConv.fill = GridBagConstraints.HORIZONTAL;
		gbc_jcbNumYearsConv.gridx = 3;
		gbc_jcbNumYearsConv.gridy = 4;
		bodyCenterLeft.add(jcbNumYearsConv, gbc_jcbNumYearsConv);

		JLabel lblNumYearMaxLevel = new JLabel(Messages.getString("param.m3.num_year_max_level") + "*");
		lblNumYearMaxLevel.setToolTipText(Messages.getString("param.m3.num_year_max_level.title"));
		GridBagConstraints gbc_lblNumYearMaxLevel = new GridBagConstraints();
		gbc_lblNumYearMaxLevel.anchor = GridBagConstraints.EAST;
		gbc_lblNumYearMaxLevel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumYearMaxLevel.gridx = 0;
		gbc_lblNumYearMaxLevel.gridy = 5;
		bodyCenterLeft.add(lblNumYearMaxLevel, gbc_lblNumYearMaxLevel);

		jcbNumYearMaxLevel = new JComboBox();
		jcbNumYearMaxLevel.addItem("1");
		jcbNumYearMaxLevel.addItem("2");
		jcbNumYearMaxLevel.addItem("3");
		jcbNumYearMaxLevel.addItem("4");

		GridBagConstraints gbc_jcbNumYearMaxLevel = new GridBagConstraints();
		gbc_jcbNumYearMaxLevel.insets = new Insets(0, 0, 5, 5);
		gbc_jcbNumYearMaxLevel.fill = GridBagConstraints.HORIZONTAL;
		gbc_jcbNumYearMaxLevel.gridx = 1;
		gbc_jcbNumYearMaxLevel.gridy = 5;
//		  gbc_txtNumYearMaxLevel.gridwidth=2;
		bodyCenterLeft.add(jcbNumYearMaxLevel, gbc_jcbNumYearMaxLevel);

		JLabel lblAMTMaxLevel = new JLabel(Messages.getString("param.m3.amt_max_level") + "*");
		lblAMTMaxLevel.setToolTipText(Messages.getString("param.m3.amt_max_level.title"));
		GridBagConstraints gbc_lblAMTMaxLevel = new GridBagConstraints();
		gbc_lblAMTMaxLevel.anchor = GridBagConstraints.EAST;
		gbc_lblAMTMaxLevel.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTMaxLevel.gridx = 2;
		gbc_lblAMTMaxLevel.gridy = 5;
		bodyCenterLeft.add(lblAMTMaxLevel, gbc_lblAMTMaxLevel);

		txtAMTMaxLevel = new JFormattedTextField(NumberFormat.getInstance());
		txtAMTMaxLevel.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMTMaxLevel.getText().isEmpty()) {
					txtAMTMaxLevel.setValue(null);
				}
			}
		});
		txtAMTMaxLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMTMaxLevel.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMTMaxLevel.getText().isEmpty() && (txtAMTMaxLevel.getText().length() >= 18
						|| txtAMTMaxLevel.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtAMTMaxLevel = new GridBagConstraints();
		gbc_txtAMTMaxLevel.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMTMaxLevel.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMTMaxLevel.gridx = 3;
		gbc_txtAMTMaxLevel.gridy = 5;
//		  gbc_txtAMTMaxLevel.gridwidth=2;
		bodyCenterLeft.add(txtAMTMaxLevel, gbc_txtAMTMaxLevel);

		JLabel lblPCTSL = new JLabel(Messages.getString("param.m3.pct_sl") + "*");
		lblPCTSL.setToolTipText(Messages.getString("param.m3.pct_sl.title"));
		GridBagConstraints gbc_lblPCTSL = new GridBagConstraints();
		gbc_lblPCTSL.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTSL.anchor = GridBagConstraints.EAST;
		gbc_lblPCTSL.gridx = 0;
		gbc_lblPCTSL.gridy = 6;
		bodyCenterLeft.add(lblPCTSL, gbc_lblPCTSL);

		txtPCTSL = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTSL.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTSL.getText().isEmpty()) {
					txtPCTSL.setValue(null);
				} else {
					if (Double.parseDouble(txtPCTSL.getText().replace(".", "").replace(",", ".")) < 30) {
						getToolkit().beep();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.perc.maggiore.di.30"));
						txtPCTSL.setValue(null);
					}
				}

			}
		});
		txtPCTSL.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCTSL.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCTSL.getText().isEmpty()
						&& (txtPCTSL.getText().length() >= 18 || txtPCTSL.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtPCTSL = new GridBagConstraints();
		gbc_txtPCTSL.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTSL.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTSL.gridx = 1;
		gbc_txtPCTSL.gridy = 6;
		bodyCenterLeft.add(txtPCTSL, gbc_txtPCTSL);

		/*********************************************************/
		JLabel lblNUMCYSL = new JLabel(Messages.getString("param.m3.num_cy_sl") + "*");
		lblNUMCYSL.setToolTipText(Messages.getString("param.m3.num_cy_sl.title"));
		GridBagConstraints gbc_lblNUMCYSL = new GridBagConstraints();
		gbc_lblNUMCYSL.anchor = GridBagConstraints.EAST;
		gbc_lblNUMCYSL.insets = new Insets(0, 0, 5, 5);
		gbc_lblNUMCYSL.gridx = 2;
		gbc_lblNUMCYSL.gridy = 6;
		bodyCenterLeft.add(lblNUMCYSL, gbc_lblNUMCYSL);

		txtNUMCYSL = new JFormattedTextField(NumberFormat.getInstance());
		txtNUMCYSL.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtNUMCYSL.getText().isEmpty()) {
					txtNUMCYSL.setValue(null);
				}
			}
		});
		txtNUMCYSL.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtNUMCYSL.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtNUMCYSL.getText().isEmpty()
						&& (txtNUMCYSL.getText().length() >= 18 || txtNUMCYSL.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtNUMCYSL = new GridBagConstraints();
		gbc_txtNUMCYSL.insets = new Insets(0, 0, 5, 5);
		gbc_txtNUMCYSL.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNUMCYSL.gridx = 3;
		gbc_txtNUMCYSL.gridy = 6;
//		  gbc_txtNUMCYSL.gridwidth=2;
		bodyCenterLeft.add(txtNUMCYSL, gbc_txtNUMCYSL);
		/********************************************************/

		JLabel lblNewLabel_2_5 = new JLabel("%");
		lblNewLabel_2_5.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2_5 = new GridBagConstraints();
		gbc_lblNewLabel_2_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_5.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_5.gridx = 2;
		gbc_lblNewLabel_2_5.gridy = 6;
		bodyCenterLeft.add(lblNewLabel_2_5, gbc_lblNewLabel_2_5);

		JLabel lblAMTMAXBPS22_1 = new JLabel(
				Messages.getString("param.m3.pct_gre") + " / " + Messages.getString("param.m3.amt_ha_gre") + "*");
		lblAMTMAXBPS22_1.setToolTipText(Messages.getString("param.m3.pct_gre.title") + "    "
				+ Messages.getString("param.m3.amt_ha_gre.title"));
		GridBagConstraints gbc_lblAMTMAXBPS22_1 = new GridBagConstraints();
		gbc_lblAMTMAXBPS22_1.anchor = GridBagConstraints.EAST;
		gbc_lblAMTMAXBPS22_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMTMAXBPS22_1.gridx = 0;
		gbc_lblAMTMAXBPS22_1.gridy = 8;
		bodyCenterLeft.add(lblAMTMAXBPS22_1, gbc_lblAMTMAXBPS22_1);

		JPanel pnlRadioButtonGree = new JPanel();
		pnlRadioButtonGree.setBackground(Color.WHITE);
		GridBagConstraints gbc_pnlRadioButtonGree = new GridBagConstraints();
		gbc_pnlRadioButtonGree.insets = new Insets(0, 0, 5, 5);
		gbc_pnlRadioButtonGree.fill = GridBagConstraints.BOTH;
		gbc_pnlRadioButtonGree.gridx = 1;
		gbc_pnlRadioButtonGree.gridy = 8;
		bodyCenterLeft.add(pnlRadioButtonGree, gbc_pnlRadioButtonGree);
		GridBagLayout gbl_pnlRadioButtonGree = new GridBagLayout();
		gbl_pnlRadioButtonGree.columnWidths = new int[] { 0, 0, 0 };
		gbl_pnlRadioButtonGree.rowHeights = new int[] { 0 };
		gbl_pnlRadioButtonGree.columnWeights = new double[] { 0.1, 0.1, 0.8 };
		gbl_pnlRadioButtonGree.rowWeights = new double[] { 1.0 };
		pnlRadioButtonGree.setLayout(gbl_pnlRadioButtonGree);

		txtAMT_HA_GRE = new JFormattedTextField(NumberFormat.getInstance());
		txtAMT_HA_GRE.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMT_HA_GRE.getText().isEmpty()) {
					txtAMT_HA_GRE.setValue(null);
				}
			}
		});
		txtAMT_HA_GRE.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMT_HA_GRE.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMT_HA_GRE.getText().isEmpty()
						&& (txtAMT_HA_GRE.getText().length() >= 18 || txtAMT_HA_GRE.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		ButtonGroup bg = new ButtonGroup();
		jrbGREE = new JRadioButton(Messages.getString("param.m3.pct_gre"));
		jrbGREE.setSelected(true);
		GridBagConstraints gbc_jrbGREE = new GridBagConstraints();
		gbc_jrbGREE.anchor = GridBagConstraints.WEST;
		gbc_jrbGREE.insets = new Insets(0, 0, 0, 5);
		gbc_jrbGREE.gridx = 0;
		gbc_jrbGREE.gridy = 0;
		pnlRadioButtonGree.add(jrbGREE, gbc_jrbGREE);
		bg.add(jrbGREE);
		jrbHAGREE = new JRadioButton(Messages.getString("param.m3.amt_ha_gre"));
		GridBagConstraints gbc_jrbHAGREE = new GridBagConstraints();
		gbc_jrbHAGREE.anchor = GridBagConstraints.WEST;
		gbc_jrbHAGREE.insets = new Insets(0, 0, 0, 5);
		gbc_jrbHAGREE.gridx = 1;
		gbc_jrbHAGREE.gridy = 0;
		pnlRadioButtonGree.add(jrbHAGREE, gbc_jrbHAGREE);
		bg.add(jrbHAGREE);
		GridBagConstraints gbc_txtAMT_HA_GRE = new GridBagConstraints();
		gbc_txtAMT_HA_GRE.fill = GridBagConstraints.BOTH;
		gbc_txtAMT_HA_GRE.anchor = GridBagConstraints.NORTHWEST;
		gbc_txtAMT_HA_GRE.gridx = 2;
		gbc_txtAMT_HA_GRE.gridy = 0;
		pnlRadioButtonGree.add(txtAMT_HA_GRE, gbc_txtAMT_HA_GRE);

		txtPCTAUA = new JFormattedTextField(NumberFormat.getInstance());
		txtPCTAUA.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCTAUA.getText().isEmpty()) {
					txtPCTAUA.setValue(null);
				} else {
					if (Double.parseDouble(txtPCTAUA.getText().replace(".", "").replace(",", ".")) < 75) {
						getToolkit().beep();
						JOptionPane.showMessageDialog(null, Messages.getString("simulazione.perc.maggiore.di.75"));
						txtPCTAUA.setValue(null);
					}
				}
			}
		});
		txtPCTAUA.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (!txtPCTAUA.getText().isEmpty()
						&& (txtPCTAUA.getText().length() >= 18 || txtPCTAUA.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					e.consume();
				}
			}
		});
		txtPCTAUA.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_txtPCTAUA = new GridBagConstraints();
		gbc_txtPCTAUA.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCTAUA.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCTAUA.gridx = 1;
		gbc_txtPCTAUA.gridy = 9;
		bodyCenterLeft.add(txtPCTAUA, gbc_txtPCTAUA);

		JLabel lblPCTAUA = new JLabel(Messages.getString("param.m3.pct_aua") + "*");
		lblPCTAUA.setToolTipText(Messages.getString("param.m3.pct_aua.title"));
		GridBagConstraints gbc_lblPCTAUA = new GridBagConstraints();
		gbc_lblPCTAUA.anchor = GridBagConstraints.EAST;
		gbc_lblPCTAUA.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCTAUA.gridx = 0;
		gbc_lblPCTAUA.gridy = 9;
		bodyCenterLeft.add(lblPCTAUA, gbc_lblPCTAUA);

		JLabel lblNewLabel_2_4 = new JLabel("%");
		lblNewLabel_2_4.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2_4 = new GridBagConstraints();
		gbc_lblNewLabel_2_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2_4.gridx = 2;
		gbc_lblNewLabel_2_4.gridy = 9;
		bodyCenterLeft.add(lblNewLabel_2_4, gbc_lblNewLabel_2_4);

		JLabel lblAMT_MAX_23 = new JLabel(Messages.getString("param.m3.amt_max_23") + "*");
		lblAMT_MAX_23.setToolTipText(Messages.getString("param.m3.amt_max_23.title"));
		GridBagConstraints gbc_lblAMT_MAX_23 = new GridBagConstraints();
		gbc_lblAMT_MAX_23.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMT_MAX_23.anchor = GridBagConstraints.EAST;
		gbc_lblAMT_MAX_23.gridx = 0;
		gbc_lblAMT_MAX_23.gridy = 10;
		bodyCenterLeft.add(lblAMT_MAX_23, gbc_lblAMT_MAX_23);

		txtAMT_MAX_23 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMT_MAX_23.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMT_MAX_23.getText().isEmpty()) {
					txtAMT_MAX_23.setValue(null);
				}
			}
		});
		txtAMT_MAX_23.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMT_MAX_23.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMT_MAX_23.getText().isEmpty()
						&& (txtAMT_MAX_23.getText().length() >= 18 || txtAMT_MAX_23.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtAMT_MAX_23 = new GridBagConstraints();
		gbc_txtAMT_MAX_23.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMT_MAX_23.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMT_MAX_23.gridx = 1;
		gbc_txtAMT_MAX_23.gridy = 10;
		bodyCenterLeft.add(txtAMT_MAX_23, gbc_txtAMT_MAX_23);

		JLabel lblPCT_BISS_23 = new JLabel(Messages.getString("param.m3.pct_biss_23") + "*");
		lblPCT_BISS_23.setToolTipText(Messages.getString("param.m3.pct_biss_23.title"));
		GridBagConstraints gbc_lblPCT_BISS_23 = new GridBagConstraints();
		gbc_lblPCT_BISS_23.anchor = GridBagConstraints.EAST;
		gbc_lblPCT_BISS_23.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCT_BISS_23.gridx = 2;
		gbc_lblPCT_BISS_23.gridy = 10;
		bodyCenterLeft.add(lblPCT_BISS_23, gbc_lblPCT_BISS_23);

		txtPCT_BISS_23 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCT_BISS_23.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCT_BISS_23.getText().isEmpty()) {
					txtPCT_BISS_23.setValue(null);
				}
			}
		});
		txtPCT_BISS_23.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCT_BISS_23.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCT_BISS_23.getText().isEmpty() && (txtPCT_BISS_23.getText().length() >= 18
						|| txtPCT_BISS_23.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtPCT_BISS_23 = new GridBagConstraints();
		gbc_txtPCT_BISS_23.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCT_BISS_23.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCT_BISS_23.gridx = 3;
		gbc_txtPCT_BISS_23.gridy = 10;
		bodyCenterLeft.add(txtPCT_BISS_23, gbc_txtPCT_BISS_23);

		JLabel lblNewLabel_2 = new JLabel("%");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridx = 4;
		gbc_lblNewLabel_2.gridy = 10;
		bodyCenterLeft.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel lblAMT_MAX_24 = new JLabel(Messages.getString("param.m3.amt_max_24") + "*");
		lblAMT_MAX_24.setToolTipText(Messages.getString("param.m3.amt_max_24.title"));
		GridBagConstraints gbc_lblAMT_MAX_24 = new GridBagConstraints();
		gbc_lblAMT_MAX_24.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMT_MAX_24.anchor = GridBagConstraints.EAST;
		gbc_lblAMT_MAX_24.gridx = 0;
		gbc_lblAMT_MAX_24.gridy = 11;
		bodyCenterLeft.add(lblAMT_MAX_24, gbc_lblAMT_MAX_24);

		txtAMT_MAX_24 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMT_MAX_24.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMT_MAX_24.getText().isEmpty()) {
					txtAMT_MAX_24.setValue(null);
				}
			}
		});
		txtAMT_MAX_24.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMT_MAX_24.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMT_MAX_24.getText().isEmpty()
						&& (txtAMT_MAX_24.getText().length() >= 18 || txtAMT_MAX_24.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtAMT_MAX_24 = new GridBagConstraints();
		gbc_txtAMT_MAX_24.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMT_MAX_24.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMT_MAX_24.gridx = 1;
		gbc_txtAMT_MAX_24.gridy = 11;
		bodyCenterLeft.add(txtAMT_MAX_24, gbc_txtAMT_MAX_24);

		JLabel lblPCT_BISS_24 = new JLabel(Messages.getString("param.m3.pct_biss_24") + "*");
		lblPCT_BISS_24.setToolTipText(Messages.getString("param.m3.pct_biss_24.title"));
		GridBagConstraints gbc_lblPCT_BISS_24 = new GridBagConstraints();
		gbc_lblPCT_BISS_24.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCT_BISS_24.anchor = GridBagConstraints.EAST;
		gbc_lblPCT_BISS_24.gridx = 2;
		gbc_lblPCT_BISS_24.gridy = 11;
		bodyCenterLeft.add(lblPCT_BISS_24, gbc_lblPCT_BISS_24);

		txtPCT_BISS_24 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCT_BISS_24.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCT_BISS_24.getText().isEmpty()) {
					txtPCT_BISS_24.setValue(null);
				}
			}
		});
		txtPCT_BISS_24.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCT_BISS_24.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCT_BISS_24.getText().isEmpty() && (txtPCT_BISS_24.getText().length() >= 18
						|| txtPCT_BISS_24.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtPCT_BISS_24 = new GridBagConstraints();
		gbc_txtPCT_BISS_24.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCT_BISS_24.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCT_BISS_24.gridx = 3;
		gbc_txtPCT_BISS_24.gridy = 11;
		bodyCenterLeft.add(txtPCT_BISS_24, gbc_txtPCT_BISS_24);

		JLabel lblNewLabel_2_1 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_1 = new GridBagConstraints();
		gbc_lblNewLabel_2_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_1.gridx = 4;
		gbc_lblNewLabel_2_1.gridy = 11;
		bodyCenterLeft.add(lblNewLabel_2_1, gbc_lblNewLabel_2_1);

		JLabel lblAMT_MAX_25 = new JLabel(Messages.getString("param.m3.amt_max_25") + "*");
		lblAMT_MAX_25.setToolTipText(Messages.getString("param.m3.amt_max_25.title"));
		GridBagConstraints gbc_lblAMT_MAX_25 = new GridBagConstraints();
		gbc_lblAMT_MAX_25.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMT_MAX_25.anchor = GridBagConstraints.EAST;
		gbc_lblAMT_MAX_25.gridx = 0;
		gbc_lblAMT_MAX_25.gridy = 12;
		bodyCenterLeft.add(lblAMT_MAX_25, gbc_lblAMT_MAX_25);

		txtAMT_MAX_25 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMT_MAX_25.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMT_MAX_25.getText().isEmpty()) {
					txtAMT_MAX_25.setValue(null);
				}
			}
		});
		txtAMT_MAX_25.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMT_MAX_25.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMT_MAX_25.getText().isEmpty()
						&& (txtAMT_MAX_25.getText().length() >= 18 || txtAMT_MAX_25.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtAMT_MAX_25 = new GridBagConstraints();
		gbc_txtAMT_MAX_25.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMT_MAX_25.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMT_MAX_25.gridx = 1;
		gbc_txtAMT_MAX_25.gridy = 12;
		bodyCenterLeft.add(txtAMT_MAX_25, gbc_txtAMT_MAX_25);

		JLabel lblPCT_BISS_25 = new JLabel(Messages.getString("param.m3.pct_biss_25") + "*");
		lblPCT_BISS_25.setToolTipText(Messages.getString("param.m3.pct_biss_25.title"));
		GridBagConstraints gbc_lblPCT_BISS_25 = new GridBagConstraints();
		gbc_lblPCT_BISS_25.anchor = GridBagConstraints.EAST;
		gbc_lblPCT_BISS_25.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCT_BISS_25.gridx = 2;
		gbc_lblPCT_BISS_25.gridy = 12;
		bodyCenterLeft.add(lblPCT_BISS_25, gbc_lblPCT_BISS_25);

		txtPCT_BISS_25 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCT_BISS_25.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCT_BISS_25.getText().isEmpty()) {
					txtPCT_BISS_25.setValue(null);
				}
			}
		});
		txtPCT_BISS_25.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCT_BISS_25.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCT_BISS_25.getText().isEmpty() && (txtPCT_BISS_25.getText().length() >= 18
						|| txtPCT_BISS_25.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtPCT_BISS_25 = new GridBagConstraints();
		gbc_txtPCT_BISS_25.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCT_BISS_25.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCT_BISS_25.gridx = 3;
		gbc_txtPCT_BISS_25.gridy = 12;
		bodyCenterLeft.add(txtPCT_BISS_25, gbc_txtPCT_BISS_25);

		JLabel lblNewLabel_2_2 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_2 = new GridBagConstraints();
		gbc_lblNewLabel_2_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_2.gridx = 4;
		gbc_lblNewLabel_2_2.gridy = 12;
		bodyCenterLeft.add(lblNewLabel_2_2, gbc_lblNewLabel_2_2);

		JLabel lblAMT_MAX_26 = new JLabel(Messages.getString("param.m3.amt_max_26") + "*");
		lblAMT_MAX_26.setToolTipText(Messages.getString("param.m3.amt_max_26.title"));
		GridBagConstraints gbc_lblAMT_MAX_26 = new GridBagConstraints();
		gbc_lblAMT_MAX_26.insets = new Insets(0, 0, 5, 5);
		gbc_lblAMT_MAX_26.anchor = GridBagConstraints.EAST;
		gbc_lblAMT_MAX_26.gridx = 0;
		gbc_lblAMT_MAX_26.gridy = 13;
		bodyCenterLeft.add(lblAMT_MAX_26, gbc_lblAMT_MAX_26);

		txtAMT_MAX_26 = new JFormattedTextField(NumberFormat.getInstance());
		txtAMT_MAX_26.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtAMT_MAX_26.getText().isEmpty()) {
					txtAMT_MAX_26.setValue(null);
				}
			}
		});
		txtAMT_MAX_26.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtAMT_MAX_26.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtAMT_MAX_26.getText().isEmpty()
						&& (txtAMT_MAX_26.getText().length() >= 18 || txtAMT_MAX_26.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});
		GridBagConstraints gbc_txtAMT_MAX_26 = new GridBagConstraints();
		gbc_txtAMT_MAX_26.insets = new Insets(0, 0, 5, 5);
		gbc_txtAMT_MAX_26.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAMT_MAX_26.gridx = 1;
		gbc_txtAMT_MAX_26.gridy = 13;
		bodyCenterLeft.add(txtAMT_MAX_26, gbc_txtAMT_MAX_26);

		JLabel lblPCT_BISS_26 = new JLabel(Messages.getString("param.m3.pct_biss_26") + "*");
		lblPCT_BISS_26.setToolTipText(Messages.getString("param.m3.pct_biss_26.title"));
		GridBagConstraints gbc_lblPCT_BISS_26 = new GridBagConstraints();
		gbc_lblPCT_BISS_26.anchor = GridBagConstraints.EAST;
		gbc_lblPCT_BISS_26.insets = new Insets(0, 0, 5, 5);
		gbc_lblPCT_BISS_26.gridx = 2;
		gbc_lblPCT_BISS_26.gridy = 13;
		bodyCenterLeft.add(lblPCT_BISS_26, gbc_lblPCT_BISS_26);

		txtPCT_BISS_26 = new JFormattedTextField(NumberFormat.getInstance());
		txtPCT_BISS_26.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (txtPCT_BISS_26.getText().isEmpty()) {
					txtPCT_BISS_26.setValue(null);
				}
			}
		});
		txtPCT_BISS_26.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtPCT_BISS_26.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				if (!txtPCT_BISS_26.getText().isEmpty() && (txtPCT_BISS_26.getText().length() >= 18
						|| txtPCT_BISS_26.getText().equalsIgnoreCase("-"))) {
					getToolkit().beep();
					evt.consume();
				}
			}
		});

		GridBagConstraints gbc_txtPCT_BISS_26 = new GridBagConstraints();
		gbc_txtPCT_BISS_26.insets = new Insets(0, 0, 5, 5);
		gbc_txtPCT_BISS_26.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPCT_BISS_26.gridx = 3;
		gbc_txtPCT_BISS_26.gridy = 13;
		// gbc_txtPCT_BISS_26.gridwidth=2;
		bodyCenterLeft.add(txtPCT_BISS_26, gbc_txtPCT_BISS_26);

		JLabel lblNewLabel_2_3 = new JLabel("%");
		GridBagConstraints gbc_lblNewLabel_2_3 = new GridBagConstraints();
		gbc_lblNewLabel_2_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2_3.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2_3.gridx = 4;
		gbc_lblNewLabel_2_3.gridy = 13;
		bodyCenterLeft.add(lblNewLabel_2_3, gbc_lblNewLabel_2_3);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 14;
		gbc_panel_2.gridwidth = 2;
		bodyCenterLeft.add(panel_2, gbc_panel_2);

		JButton button = new JButton(Messages.getString("button.esegui.simulazione"));

		// set valori di default da eliminare
		txtNunMaxSupLevel.setText("1,0000");
		txtNUMCYSL.setText("10");
		button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Date curr = new Date();
				String anno = "" + (curr.getYear() + 1900);
				String mese = (curr.getMonth() + 1) > 9 ? "" + (curr.getMonth() + 1) : "0" + (curr.getMonth() + 1);
				String giorno = curr.getDate() > 9 ? "" + curr.getDate() : "0" + curr.getDate();
				String sData = anno + "-" + mese + "-" + giorno;
				String hh = curr.getHours() > 9 ? "" + curr.getHours() : "0" + curr.getHours();
				String mm = curr.getMinutes() > 9 ? "" + curr.getMinutes() : "0" + curr.getMinutes();
				String ss = curr.getSeconds() > 9 ? "" + curr.getSeconds() : "0" + curr.getSeconds();
				String dataFile = "PYE_FILE_" + sData + " " + hh + mm + ss;
				dataDB = "" + sData + " " + hh + ":" + mm + ":" + ss;
				String valGree;
				String valHAGree;
				if (txtNunMaxSupLevel.getText().isEmpty() || jcbNumYearsConv.getSelectedItem().toString().isEmpty()
						|| jcbNumYearMaxLevel.getSelectedItem().toString().isEmpty()
						|| txtAMTMaxLevel.getText().isEmpty() || txtPCTSL.getText().isEmpty() ||
				// txtAMTMAXBPS22.getText().isEmpty() ||
				// txtAMTMAXGRE22.getText().isEmpty() ||
				txtNUMCYSL.getText().isEmpty() || txtAMT_HA_GRE.getText().isEmpty() || txtPCTAUA.getText().isEmpty()
						|| txtAMT_MAX_23.getText().isEmpty() || txtPCT_BISS_23.getText().isEmpty()
						|| txtAMT_MAX_24.getText().isEmpty() || txtPCT_BISS_24.getText().isEmpty()
						|| txtAMT_MAX_25.getText().isEmpty() || txtPCT_BISS_25.getText().isEmpty()
						|| txtAMT_MAX_26.getText().isEmpty() || txtPCT_BISS_26.getText().isEmpty()) {

					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.chkdati"));
				} else {

					int input = JOptionPane.showConfirmDialog(null, Messages.getString("simulazione.conferma.avvio"));
					if (input == 0) {
						try {

							UtilFile.copiaFile(txtPathFile.getText(), Home.getPathJar() + "/"
									+ ConfigurationProperties.FOLDER_DATA + "/" + dataFile + ".csv");
							SwingWorker work = createWorker(dataDB);
							work.execute();
							JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avviata"));
							Home.switchPanel(new PanelBodySimulazione());

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} finally {

						}

					} else {

					}

				}
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(0, 110, 171));
		panel_2.add(button);

	}

	public SwingWorker<Boolean, Integer> createWorker(String dataDB) {
		return new SwingWorker<Boolean, Integer>() {
			@Override
			protected Boolean doInBackground() throws Exception {
				try {
					UtilKettle.executeJobPYE(txtNunMaxSupLevel.getText(), jcbNumYearsConv.getSelectedItem().toString(),
							jcbNumYearMaxLevel.getSelectedItem().toString(), dataDB, txtAMTMaxLevel.getText(),
							txtPCTSL.getText(),
							// txtAMTMAXBPS22.getText(),
							// txtAMTMAXGRE22.getText(),
							txtNUMCYSL.getText(),
//						 txtPCT_GRE.getText(),
							jrbGREE.isSelected() ? txtAMT_HA_GRE.getText() : "-1",
							jrbHAGREE.isSelected() ? txtAMT_HA_GRE.getText() : "-1", txtPCTAUA.getText(),
							txtAMT_MAX_23.getText(), txtPCT_BISS_23.getText(), txtAMT_MAX_24.getText(),
							txtPCT_BISS_24.getText(), txtAMT_MAX_25.getText(), txtPCT_BISS_25.getText(),
							txtAMT_MAX_26.getText(), txtPCT_BISS_26.getText());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, Messages.getString("simulazione.avvio.ko"));
				}
				return true;
			}

			@Override
			protected void process(List<Integer> chunks) {
			}

			@Override
			protected void done() {
				boolean bStatus = false;
				try {
					UtilPrint print = new UtilPrint();
					print.Stampa(dataDB);
					if (Home.getPanelActive().equalsIgnoreCase(PanelBodySimulazione.class.getName())) {
						// PanelBodySimulazione.getElencoSimulazioni();
						Home.switchPanel(new PanelBodySimulazione());
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				System.out.println("Finished with status " + bStatus);
			}
		};
	}

	private String esitoJob(String data) {
		String result = null;
		Connection connection = null;
		ConnectionDB c = new ConnectionDB();
		try {
			connection = c.getConnectionDB(Home.getDB_NAME(), Home.getDB_UTENTE(), Home.getDB_PASSWORD());
			PreparedStatement st = connection
					.prepareStatement("select * from sys_status_job  where dat_exec_job ='" + data + "'");
			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				result = rs.getString("job_status_type") + ":" + rs.getString("job_status");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				// gestione errore in chiusura
			}
		}

		return result;
	}

}
