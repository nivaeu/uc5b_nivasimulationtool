package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import bl.Messages;
import javax.swing.Icon;
import java.awt.Label;
import java.awt.FlowLayout;

public class PanelFooter extends JPanel {
	/**
	* 
	*/
	private static final long serialVersionUID = 2661815361586151904L;

	/**
	 * Create the panel.
	 */
	public PanelFooter() {
		setBackground(Color.WHITE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0 };
		gridBagLayout.columnWeights = new double[] { 0.1, 0.6, 0.3 };
		gridBagLayout.rowWeights = new double[] { 1.0 };
		setLayout(gridBagLayout);

		JPanel panelLeft = new JPanel();
		panelLeft.setBackground(Color.WHITE);

		GridBagConstraints gbc_panelLeft = new GridBagConstraints();
		gbc_panelLeft.insets = new Insets(0, 0, 0, 5);
		gbc_panelLeft.fill = GridBagConstraints.BOTH;
		gbc_panelLeft.gridx = 0;
		gbc_panelLeft.gridy = 0;
		add(panelLeft, gbc_panelLeft);

		ImageIcon iconLogoUE = new ImageIcon(Home.class.getResource("/images/unione_europea.png"));
		Image imageLogoUE = iconLogoUE.getImage();
		imageLogoUE = imageLogoUE.getScaledInstance(57, 38, java.awt.Image.SCALE_SMOOTH);
		iconLogoUE = new ImageIcon(imageLogoUE);
		JLabel labelLogoUE = new JLabel(iconLogoUE);
		labelLogoUE.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoUE.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoUE.setBorder(new EmptyBorder(10, 0, 0, 0));
		panelLeft.add(labelLogoUE);

		JPanel panelCenter = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelCenter.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panelCenter.setBackground(Color.WHITE);
		GridBagConstraints gbc_panelCenter = new GridBagConstraints();
		gbc_panelCenter.insets = new Insets(10, 0, 0, 5);
		gbc_panelCenter.fill = GridBagConstraints.BOTH;
		gbc_panelCenter.gridx = 1;
		gbc_panelCenter.gridy = 0;
		add(panelCenter, gbc_panelCenter);

		JLabel label = new JLabel(Messages.getString("niva.footer.ue"));
		panelCenter.add(label);

		JPanel panelRight = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panelRight.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panelRight.setBackground(Color.WHITE);
		GridBagConstraints gbc_panelRight = new GridBagConstraints();
		gbc_panelRight.fill = GridBagConstraints.BOTH;
		gbc_panelRight.gridx = 2;
		gbc_panelRight.gridy = 0;
		add(panelRight, gbc_panelRight);

		ImageIcon iconLogoTecso = new ImageIcon(Home.class.getResource("/images/fornitore.png"));
		Image imageLogoTecso = iconLogoTecso.getImage();
		imageLogoTecso = imageLogoTecso.getScaledInstance(161, 38, java.awt.Image.SCALE_SMOOTH);
		iconLogoTecso = new ImageIcon(imageLogoTecso);
		JLabel labelLogoTecso = new JLabel(iconLogoTecso);
		labelLogoTecso.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoTecso.setHorizontalAlignment(SwingConstants.LEFT);
		labelLogoTecso.setBorder(new EmptyBorder(10, 0, 0, 20));
		panelRight.add(labelLogoTecso);

	}

	public JPanel createBackgroundPanel(String urlResource, int maxWidth, int maxHeight) {
		JPanel panelRight = new JPanel() {

			/**
			* 
			*/
			private static final long serialVersionUID = -3557741759172228180L;
			BufferedImage image;
			{
				try {
					image = ImageIO.read(getClass().getResource(urlResource));
				} catch (IOException ex) {
					// Logger.getLogger(TestBackgroundResize.class.getName()).log(Level.SEVERE,
					// null, ex);
				}

			}

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				int w = getWidth();
				int h = getHeight();

				if (maxWidth > 0) {
					w = w < maxWidth ? w : maxWidth;

				}
				if (maxHeight > 0) {

					h = h < maxHeight ? h : maxHeight;
				}

				g.drawImage(image, getWidth() - w - 30, getHeight() - h - 10, w, h, this);

			}

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(80, 40);
			}
		};
		panelRight.setBackground(Color.WHITE);

		return panelRight;
	}
}
