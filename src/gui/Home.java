package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bl.ConfigurationProperties;
import bl.Messages;
import bl.UtilKettle;

public class Home extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3177944258522044362L;
	private JPanel contentPane;
	private static JLayeredPane layeredPane;
	private static String pathJar;
	private static String DB_NAME;
	private static String DB_IP;
	private static String DB_PORTA;
	private static String DB_UTENTE;
	private static String DB_PASSWORD;
	private static String DB_CREATO;
	private static String LANGUAGE;
	private static Home frame;
	private static String panelActive;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Home();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void switchPanel(JPanel panel) {
		layeredPane.removeAll();
		layeredPane.add(panel);
		layeredPane.repaint();
		layeredPane.validate();
		panelActive = panel.getName();
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setEnviroment();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setName(Home.class.getName());
		ImageIcon iconHome = new ImageIcon(Home.class.getResource("/images/niva.png"));
		Image imageHome = iconHome.getImage();
		setIconImage(imageHome);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setTitle(Messages.getString("niva.smt")); //$NON-NLS-1$
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int widthJF = (int) ((dim.getWidth()) * 0.8);
		int heightJF = (int) (dim.getHeight() * 0.8);
		setSize(widthJF, heightJF);
		setResizable(false);
		setLocation((dim.width / 2) - (widthJF / 2), (dim.height / 2) - (heightJF / 2));
		setBackground(Color.WHITE);
		// Da coomentare quando � tutto finito
		// setBounds(100, 100, 1000, 600);

		setContentPane(contentPane);

		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0 };
		gbl_contentPane.rowWeights = new double[] { 0.1, 0.8, 0.1 };
		contentPane.setLayout(gbl_contentPane);

		/*************************************
		 * HEADER PANEL *
		 ***************************************/
		JPanel header = new PanelHeader();
		GridBagConstraints gbc_header = new GridBagConstraints();
		gbc_header.fill = GridBagConstraints.BOTH;
		gbc_header.gridx = 0;
		gbc_header.gridy = 0;
		contentPane.add(header, gbc_header);

		/*************************************
		 * BODY PANEL *
		 ***************************************/
		JPanel body = new JPanel();
		body.setBackground(Color.WHITE);
		body.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, new Color(76, 144, 33)));
		GridBagConstraints gbc_body = new GridBagConstraints();
		gbc_body.fill = GridBagConstraints.BOTH;
		gbc_body.gridx = 0;
		gbc_body.gridy = 1;
		contentPane.add(body, gbc_body);
		body.setLayout(new CardLayout(0, 0));

		layeredPane = new JLayeredPane();
		body.add(layeredPane, "layeredPaneBody");
		layeredPane.setLayout(new CardLayout(0, 0));

		switchPanel(new PanelBodyHome());

		/**************************************
		 * FOOTER PANEL *
		 ***************************************/

		JPanel footer = new PanelFooter();
		footer.setBackground(Color.WHITE);
		GridBagConstraints gbc_footer = new GridBagConstraints();
		gbc_footer.fill = GridBagConstraints.BOTH;
		gbc_footer.gridx = 0;
		gbc_footer.gridy = 2;
		contentPane.add(footer, gbc_footer);

	}

	private void setEnviroment() {
		String path = null;
		try {
			File currentJavaJarFile = new File(
					Home.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			String currentJavaJarFilePath = currentJavaJarFile.getAbsolutePath();
			String currentRootDirectoryPath = currentJavaJarFilePath.replace(currentJavaJarFile.getName(), "");
			path = currentRootDirectoryPath;

		} catch (Exception e) {

			e.printStackTrace();
		}

		Home.setPathJar(path);
		ConfigurationProperties.configureEnviroment();
		setParametriDB();
		setLanguage(getLANGUAGE());
		setEnviromentKettle();

	}

	public static void setLanguage(String language) {

		if (language.equalsIgnoreCase(ConfigurationProperties.PROP_LANGUAGE_EN)) {

			Locale.setDefault(Locale.ENGLISH);
			try {
				ConfigurationProperties.setProperties(ConfigurationProperties.PROP_LANGUAGE,
						ConfigurationProperties.PROP_LANGUAGE_EN);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			Locale.setDefault(Locale.ITALIAN);
			try {
				ConfigurationProperties.setProperties(ConfigurationProperties.PROP_LANGUAGE,
						ConfigurationProperties.PROP_LANGUAGE_IT);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		setParametriDB();
		if (frame != null) {
			frame.setTitle(Messages.getString("niva.smt"));
			PanelHeader.updateLabel();
			PanelBodyHome.updateLabel();
		}
	}

	private void setEnviromentKettle() {
		UtilKettle.configureEnviroment();
		UtilKettle.initialized();
	}

	public static void setParametriDB() {
		String[] keys;
		try {
			keys = ConfigurationProperties.getProperties();
			setDB_NAME(keys[0]);
			setDB_IP(keys[1]);
			setDB_PORTA(keys[2]);
			setDB_UTENTE(keys[3]);
			setDB_PASSWORD(keys[4]);
			setDB_CREATO(keys[5]);
			setLANGUAGE(keys[6]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	public static String getPathJar() {
		return pathJar;
	}

	public static void setPathJar(String pathJar) {
		Home.pathJar = pathJar;
	}

	public static String getDB_NAME() {
		return DB_NAME;
	}

	public static void setDB_NAME(String dB_NAME) {
		DB_NAME = dB_NAME;
	}

	public static String getDB_IP() {
		return DB_IP;
	}

	public static void setDB_IP(String dB_IP) {
		DB_IP = dB_IP;
	}

	public static String getDB_PORTA() {
		return DB_PORTA;
	}

	public static void setDB_PORTA(String dB_PORTA) {
		DB_PORTA = dB_PORTA;
	}

	public static String getDB_UTENTE() {
		return DB_UTENTE;
	}

	public static void setDB_UTENTE(String dB_UTENTE) {
		DB_UTENTE = dB_UTENTE;
	}

	public static String getDB_PASSWORD() {
		return DB_PASSWORD;
	}

	public static void setDB_PASSWORD(String dB_PASSWORD) {
		DB_PASSWORD = dB_PASSWORD;
	}

	public static String getDB_CREATO() {
		return DB_CREATO;
	}

	public static void setDB_CREATO(String dB_CREATO) {
		DB_CREATO = dB_CREATO;
	}

	public static String getLANGUAGE() {
		return LANGUAGE;
	}

	public static void setLANGUAGE(String language) {
		LANGUAGE = language;
	}

	public static String getPanelActive() {
		return panelActive;
	}

	public static void setPanelActive(String panelActive) {
		Home.panelActive = panelActive;
	}

}
